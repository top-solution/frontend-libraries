declare module '@mdi/svg/svg/*.svg' {
  import React from 'react';
  const SVG: React.VFC<React.SVGProps<SVGSVGElement>>;
  export const ReactComponent = SVG;
}

declare module '*.svg' {
  const SVG: React.VFC<React.SVGProps<SVGSVGElement>>;
  export const ReactComponent = SVG;
}
