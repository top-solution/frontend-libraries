All SVGs are transformed in React components with SVGR.

Bundlers plugins (for Storybook/webpack, rollup and vite) can't decide wether to export the component as `default` or `ReactComponent`, so this is
the painless way so far to include those few assets.

Please do not edit the files in this directory, update the sources and launch `npm run icons` after.