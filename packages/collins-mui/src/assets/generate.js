const { readFile, writeFile, readdir } = require('fs').promises;
const { join, basename, extname } = require('path');
const svgr = require('@svgr/core').default;

function toPascalCase(text) {
  return text.replace(/(^\w|-\w)/g, clearAndUpper);
}

function clearAndUpper(text) {
  return text.replace(/-/, '').toUpperCase();
}



(async () => {
  const nodeModules = join(__dirname, '../../node_modules');
  const filesInSources = await readdir(join(__dirname, 'sources'))

  const filteredFilesInSources =  filesInSources
    .filter(filename => filename.endsWith('.svg'))
    .map(filename => join(__dirname, 'sources', filename));

  // Add your icons here
  const icons = [
    join(nodeModules, '@mdi/svg/svg/account-outline.svg'),
    join(nodeModules, '@mdi/svg/svg/chevron-down.svg'),
    join(nodeModules, '@mdi/svg/svg/chevron-right.svg'),
    join(nodeModules, '@mdi/svg/svg/content-copy.svg'),
    ...filteredFilesInSources,
  ];

  const toExport = [];

  for (let i = 0; i < icons.length; i++) {
    const iconPath = icons[i];
    const icon = await readFile(iconPath);
    const filename = basename(iconPath.replace(extname(iconPath), ''));
    const jsCode = await svgr(icon, { plugins: ['@svgr/plugin-svgo', '@svgr/plugin-jsx'], typescript: true }, { componentName: toPascalCase(filename) })
    await writeFile(join(__dirname, `${toPascalCase(filename)}.tsx`), jsCode);
    toExport.push(toPascalCase(filename));
  }

  await writeFile(join(__dirname, `index.ts`), toExport.map(exportFile => `export { default as ${exportFile}} from './${exportFile}';`).join('\n'));

})();