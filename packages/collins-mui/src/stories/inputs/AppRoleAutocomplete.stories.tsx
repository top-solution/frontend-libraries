import React, { useState, useEffect } from 'react';
import { Story, Meta } from '@storybook/react';

import { AppRoleAutocomplete, AppRoleAutocompleteProps } from '../../components/inputs/AppRoleAutocomplete';
import { CommonDataProvider } from '../CommonDataProvider';

export default {
  title: 'Collins MUI/Inputs/AppRoleAutocomplete',
  component: AppRoleAutocomplete,
  argTypes: {
    onChange: { action: 'onChange' },
  },
} as Meta;

const Template: Story<AppRoleAutocompleteProps<false, false, false>> = (props) => {
  const [value, setValue] = useState(props.value);

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  const handleChange: AppRoleAutocompleteProps<false, false, false>['onChange'] = (...args) => {
    setValue(args[1]);
    if (props.onChange) {
      props.onChange(...args);
    }
  };

  return (
    <CommonDataProvider>
      <AppRoleAutocomplete {...props} onChange={handleChange as never} value={value} />
    </CommonDataProvider>
  );
};

export const Basic = Template.bind({});
