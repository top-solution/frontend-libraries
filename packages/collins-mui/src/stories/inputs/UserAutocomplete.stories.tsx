import React, { useState, useEffect } from 'react';
import { Story, Meta } from '@storybook/react';

import { UserAutocomplete, UserAutocompleteProps } from '../../components/inputs/UserAutocomplete';
import { CommonDataProvider } from '../CommonDataProvider';

export default {
  title: 'Collins MUI/Inputs/UserAutocomplete',
  component: UserAutocomplete,
  argTypes: {
    onChange: { action: 'onChange' },
  },
} as Meta;

const Template: Story<UserAutocompleteProps<false, false, false>> = (props) => {
  const [value, setValue] = useState(props.value);

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  const handleChange: UserAutocompleteProps<false, false, false>['onChange'] = (...args) => {
    setValue(args[1]);
    if (props.onChange) {
      props.onChange(...args);
    }
  };

  return (
    <CommonDataProvider>
      <UserAutocomplete {...props} onChange={handleChange as never} value={value} />
    </CommonDataProvider>
  );
};

export const Basic = Template.bind({});
