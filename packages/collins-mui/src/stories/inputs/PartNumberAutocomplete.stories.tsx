import React, { useState, useEffect } from 'react';
import { Story, Meta } from '@storybook/react';

import { PartNumberAutocomplete, PartNumberAutocompleteProps } from '../../components/inputs/PartNumberAutocomplete';
import { CommonDataProvider } from '../CommonDataProvider';

export default {
  title: 'Collins MUI/Inputs/PartNumberAutocomplete',
  component: PartNumberAutocomplete,
  argTypes: {
    onChange: { action: 'onChange' },
  },
} as Meta;

const Template: Story<PartNumberAutocompleteProps<false, false, false>> = (props) => {
  const [value, setValue] = useState(props.value);

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  const handleChange: PartNumberAutocompleteProps<false, false, false>['onChange'] = (...args) => {
    setValue(args[1]);
    if (props.onChange) {
      props.onChange(...args);
    }
  };

  return (
    <CommonDataProvider>
      <PartNumberAutocomplete
        //  {...props}
        onChange={handleChange as never}
        value={value}
        sx={{ width: '50%' }}
      />
    </CommonDataProvider>
  );
};

export const Basic = Template.bind({});
