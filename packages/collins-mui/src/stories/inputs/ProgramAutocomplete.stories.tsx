import React, { useState, useEffect } from 'react';
import Box from '@mui/material/Box';
import { Story, Meta } from '@storybook/react';

import { ProgramAutocomplete, ProgramAutocompleteProps } from '../../components/inputs/ProgramAutocomplete';
import { CommonDataProvider } from '../CommonDataProvider';

export default {
  title: 'Collins MUI/Inputs/ProgramAutocomplete',
  component: ProgramAutocomplete,
  argTypes: {
    onChange: { action: 'onChange' },
  },
} as Meta;

const Template: Story<ProgramAutocompleteProps<false, false, true>> = (props) => {
  const [value, setValue] = useState(props.value);

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  const handleChange: ProgramAutocompleteProps<false, false, true>['onChange'] = (...args) => {
    setValue(args[1]);
    if (props.onChange) {
      props.onChange(...args);
    }
  };

  return (
    <Box sx={{ maxWidth: 400 }}>
      <CommonDataProvider>
        <ProgramAutocomplete {...props} onChange={handleChange as never} value={value} />
      </CommonDataProvider>
    </Box>
  );
};

export const Basic = Template.bind({});

export const DefaultValue = Template.bind({});
DefaultValue.args = {
  value: 'c-27j',
};

export const ValueAsStringNotFound = Template.bind({});
ValueAsStringNotFound.args = {
  value: 'th1sd0esn0tex1st',
};
