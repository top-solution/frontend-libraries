import React, { useState, useEffect } from 'react';
import { Story, Meta } from '@storybook/react';

import { CountryAutocomplete, CountryAutocompleteProps } from '../../components/inputs/CountryAutocomplete';
import { CommonDataProvider } from '../CommonDataProvider';

export default {
  title: 'Collins MUI/Inputs/CountryAutocomplete',
  component: CountryAutocomplete,
  argTypes: {
    onChange: { action: 'onChange' },
  },
} as Meta;

const Template: Story<CountryAutocompleteProps<false, false, false>> = (props) => {
  const [value, setValue] = useState(props.value);

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  const handleChange: CountryAutocompleteProps<false, false, false>['onChange'] = (...args) => {
    setValue(args[1]);
    if (props.onChange) {
      props.onChange(...args);
    }
  };

  return (
    <CommonDataProvider>
      <CountryAutocomplete {...props} onChange={handleChange as never} value={value} />
    </CommonDataProvider>
  );
};

export const Basic = Template.bind({});
