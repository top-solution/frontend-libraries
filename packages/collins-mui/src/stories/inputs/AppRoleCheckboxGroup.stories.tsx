import React, { useState, useEffect } from 'react';
import { Story, Meta } from '@storybook/react';

import { AppRoleCheckboxGroup, AppRoleCheckboxGroupProps } from '../../components/inputs/AppRoleCheckboxGroup';
import { CommonDataProvider } from '../CommonDataProvider';

export default {
  title: 'Collins MUI/Inputs/AppRoleCheckboxGroup',
  component: AppRoleCheckboxGroup,
  argTypes: {
    onChange: { action: 'onChange' },
  },
} as Meta;

const Template: Story<AppRoleCheckboxGroupProps> = (props) => {
  const [value, setValue] = useState(props.value);

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  return (
    <CommonDataProvider>
      <AppRoleCheckboxGroup
        {...props}
        onChange={(...args) => {
          if (props.onChange) {
            setValue([...args[1]]);
            props.onChange(...args);
          }
        }}
        value={value}
      />
    </CommonDataProvider>
  );
};

export const Basic = Template.bind({});
