import React, { useState, useEffect } from 'react';
import { Story, Meta } from '@storybook/react';

import { PlantSelect, PlantSelectProps } from '../../components/inputs/PlantSelect';
import { CommonDataProvider } from '../CommonDataProvider';

export default {
  title: 'Collins MUI/Inputs/PlantSelect',
  component: PlantSelect,
  argTypes: {
    onChange: { action: 'onChange' },
  },
} as Meta;

const Template: Story<PlantSelectProps> = (props) => {
  const [value, setValue] = useState(props.value);

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  return (
    <CommonDataProvider>
      <PlantSelect
        {...props}
        onChange={(...args) => {
          setValue(args[1]);
          if (props.onChange) {
            props.onChange(...args);
          }
        }}
        value={value}
      />
    </CommonDataProvider>
  );
};

export const Basic = Template.bind({});
