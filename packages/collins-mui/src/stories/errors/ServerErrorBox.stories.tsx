import React from 'react';
import Box from '@mui/system/Box';
import { FetchBaseQueryError } from '@reduxjs/toolkit/dist/query';
import { Story, Meta } from '@storybook/react';

import { ServerErrorBox, ServerErrorBoxProps } from '../../components/errors/ServerErrorBox';

export default {
  title: 'Collins MUI/Errors/ServerErrorBox',
  component: ServerErrorBox,
  argTypes: {},
} as Meta;

const Template: Story<ServerErrorBoxProps> = (props) => {
  return (
    <Box sx={{ maxWidth: 600, svg: { maxHeight: 320 } }}>
      <ServerErrorBox
        {...props}
        error={
          {
            status: 'PARSING_ERROR',
            data: 'Mr. Utterson’s only answer was to rise and get his hat and greatcoat',
            originalStatus: 418,
            error:
              'But he observed with wonder the greatness of the relief that appeared upon the butler’s face, and perhaps with no less, that the wine was still untasted when he set it down to follow.',
          } as FetchBaseQueryError
        }
      />
    </Box>
  );
};

export const Basic = Template.bind({});
