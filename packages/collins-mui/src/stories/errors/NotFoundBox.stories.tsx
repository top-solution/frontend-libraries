import React from 'react';
import Box from '@mui/system/Box';
import { Story, Meta } from '@storybook/react';

import { NotFoundBox, NotFoundBoxProps } from '../../components/errors/NotFoundBox';

export default {
  title: 'Collins MUI/Errors/NotFoundBox',
  component: NotFoundBox,
  argTypes: {},
} as Meta;

const Template: Story<NotFoundBoxProps> = (props) => {
  return (
    <Box sx={{ maxWidth: 600, svg: { maxHeight: 320 } }}>
      <NotFoundBox {...props} />
    </Box>
  );
};

export const Basic = Template.bind({});
