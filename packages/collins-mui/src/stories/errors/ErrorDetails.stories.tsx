import React from 'react';
import Box from '@mui/system/Box';
import { Story, Meta } from '@storybook/react';

import { ErrorDetails, ErrorDetailsProps } from '../../components/errors/ErrorDetails';
import { CommonDataProvider } from '../CommonDataProvider';

export default {
  title: 'Collins MUI/Errors/ErrorDetails',
  component: ErrorDetails,
  argTypes: {},
} as Meta;

const Template: Story<ErrorDetailsProps> = (props) => {
  return (
    <CommonDataProvider>
      <Box sx={{ maxWidth: 600 }}>
        <ErrorDetails
          {...props}
          title="The lawyer listened gloomily"
          content="The lawyer listened gloomily; he did not like his friend’s feverish manner. “You seem pretty sure of him,” said he; “and for your sake, I hope you may be right. If it came to a trial, your name might appear.”"
        />
      </Box>
    </CommonDataProvider>
  );
};

export const Basic = Template.bind({});
