import React from 'react';
import Box from '@mui/system/Box';
import { FetchBaseQueryError } from '@reduxjs/toolkit/dist/query';
import { Story, Meta } from '@storybook/react';

import { z, ZodError } from 'zod';
import { ErrorAlert, ErrorAlertProps } from '../../components/errors/ErrorAlert';
import { CommonDataProvider } from '../CommonDataProvider';

export default {
  title: 'Collins MUI/Errors/ErrorAlert',
  component: ErrorAlert,
  argTypes: {},
} as Meta;

const Template: Story<ErrorAlertProps> = (props) => {
  return (
    <CommonDataProvider>
      <Box sx={{ maxWidth: 600 }}>
        <ErrorAlert {...props} />
      </Box>
    </CommonDataProvider>
  );
};

export const Basic = Template.bind({});
Basic.args = {
  title: 'Here I proceeded to examine its contents',
  error: new Error(
    'The powders were neatly enough made up, but not with the nicety of the dispensing chemist; so that it was plain they were of Jekyll’s private manufacture: and when I opened one of the wrappers I found what seemed to me a simple crystalline salt of a white colour.'
  ),
};

export const WithFetchError = Template.bind({});
WithFetchError.args = {
  title: 'Here I proceeded to examine its contents',
  error: {
    status: 'FETCH_ERROR',
    data: undefined,
    error:
      'The phial, to which I next turned my attention, might have been about half full of a blood-red liquor, which was highly pungent to the sense of smell and seemed to me to contain phosphorus and some volatile ether. At the other ingredients I could make no guess. ',
  } as FetchBaseQueryError,
};

export const WithParsingError = Template.bind({});
WithParsingError.args = {
  title: 'At the other ingredients I could make no guess.',
  error: {
    status: 'PARSING_ERROR',
    data: 'The book was an ordinary version book and contained little but a series of dates.',
    originalStatus: 418,
    error:
      'These covered a period of many years, but I observed that the entries ceased nearly a year ago and quite abruptly.',
  } as FetchBaseQueryError,
};

const failingValidation = z.object({ stringProperty: z.string() }).safeParse({ numberProperty: 42 });

export const WithZodError = Template.bind({});
WithZodError.args = {
  title: '',
  error: (failingValidation.success ? null : failingValidation.error) as ZodError,
};
