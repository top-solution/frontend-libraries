import React from 'react';
import Box from '@mui/system/Box';
import { Story, Meta } from '@storybook/react';

import { UnauthorizedBox, UnauthorizedBoxProps } from '../../components/errors/UnauthorizedBox';

export default {
  title: 'Collins MUI/Errors/UnauthorizedBox',
  component: UnauthorizedBox,
  argTypes: {},
} as Meta;

const Template: Story<UnauthorizedBoxProps> = (props) => {
  return (
    <Box sx={{ maxWidth: 600, svg: { maxHeight: 320 } }}>
      <UnauthorizedBox {...props} />
    </Box>
  );
};

export const Basic = Template.bind({});
