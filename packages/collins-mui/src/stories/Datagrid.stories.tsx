import React, { useCallback, useMemo, useState } from 'react';
import Button from '@mui/material/Button';
import Box from '@mui/system/Box';
import {
  GridActionsColDef,
  GridCellParams,
  GridColDef,
  GridFilterModel,
  GridLinkOperator,
  GridSortModel,
  useGridApiRef,
  LicenseInfo,
} from '@mui/x-data-grid-pro';
import { Story, Meta } from '@storybook/react';

import { usePagination, useReadPersonListQuery } from '@top-solution/collins-utils';
import { DataGrid, DataGridProps, DataGridWrapper, usePlantColDef } from '../components/DataGrid';
import { muiFiltersToPagedRequestFilters } from '../components/DataGrid/muiFiltersToPagedRequestFilters';
import { CommonDataProvider } from './CommonDataProvider';

export default {
  title: 'Collins MUI/Datagrid',
  component: DataGrid,
  argTypes: {},
} as Meta;

LicenseInfo.setLicenseKey(
  '5db82d853cbecf7e60d7f45586d406ccT1JERVI6MjkzODUsRVhQSVJZPTE2NjMyMzQwMjkwMDAsS0VZVkVSU0lPTj0x'
);

const DataGridExample: Story<DataGridProps> = (props) => {
  const apiRef = useGridApiRef();
  const { page, setPage, pageSize, setPageSize } = usePagination(0);
  const [sortModel, setSortModel] = useState<GridSortModel>([{ field: 'lastname', sort: 'asc' }]);
  const [filterModel, setFilterModel] = useState<GridFilterModel>({ items: [], linkOperator: GridLinkOperator.And });
  const plantColumn = usePlantColDef();

  const readPersonListParams = useMemo(
    () => ({
      limit: pageSize as number,
      offset: (pageSize as number) * page,
      sort: sortModel.map(({ field, sort }) => `${sort === 'desc' ? '-' : ''}${field}`),
      filters: muiFiltersToPagedRequestFilters(filterModel.items),
    }),
    [filterModel.items, page, pageSize, sortModel]
  );

  const columns = useMemo(() => {
    const columns: (GridColDef | GridActionsColDef)[] = [
      { field: 'lastname', headerName: 'Cognome', width: 200, minWidth: 150, flex: 1, editable: true },
      { field: 'firstname', headerName: 'Nome', width: 200, minWidth: 130, flex: 1, editable: true },
      { field: 'company', headerName: 'Società', width: 250, minWidth: 150, flex: 1, editable: true },
      {
        ...plantColumn,
        field: 'plantID',
        headerName: 'Sito',
        width: 150,
        minWidth: 110,
        editable: true,
      },
      {
        field: 'phoneNumber',
        headerName: 'Telefono',
        minWidth: 150,
        sortable: false,
        filterable: false,
        editable: true,
      },
      {
        field: 'phoneNumberShort',
        headerName: 'T. abbr.',
        description: 'Telefono abbreviato',
        minWidth: 110,
        sortable: false,
        filterable: false,
        editable: true,
      },
      {
        field: 'mobileNumber',
        headerName: 'Cellulare',
        minWidth: 150,
        sortable: false,
        filterable: false,
        editable: true,
      },
      {
        field: 'mobileNumberShort',
        headerName: 'C. abbr.',
        description: 'Cellulare abbreviato',
        minWidth: 110,
        sortable: false,
        filterable: false,
        editable: true,
      },
      {
        field: 'email',
        headerName: 'Email',
        width: 300,
        minWidth: 100,
        renderCell: function renderEmailCell({ value }: GridCellParams): JSX.Element {
          return <a href={`mailto:${value}`}>{value}</a>;
        },
        sortable: false,
        filterable: false,
        editable: true,
      },
      { field: 'username', headerName: 'Username', width: 200, minWidth: 150 },
    ];
    return columns;
  }, [plantColumn]);

  const handleSortModelChange = useCallback(
    (sortModel: GridSortModel) => {
      setSortModel(sortModel);
      setPage(0);
    },
    [setPage]
  );

  const handleFilterModelChange = useCallback(
    (filterModel: GridFilterModel) => {
      setFilterModel(filterModel);
      setPage(0);
    },
    [setPage]
  );

  const personList = useReadPersonListQuery(readPersonListParams, { skip: pageSize === undefined });

  return (
    <DataGridWrapper>
      <DataGrid
        {...props}
        apiRef={apiRef}
        density="compact"
        rows={personList.data?.data || []}
        columns={columns}
        loading={personList.isFetching}
        error={personList.error}
        pagination
        paginationMode="server"
        rowCount={personList.data?.total}
        page={page}
        onPageChange={setPage}
        pageSize={pageSize}
        onPageSizeChange={setPageSize}
        sortingMode="server"
        sortModel={sortModel}
        onSortModelChange={handleSortModelChange}
        filterMode="server"
        onFilterModelChange={handleFilterModelChange}
        filterModel={filterModel}
        editMode="row"
      />
    </DataGridWrapper>
  );
};

const Template: Story<DataGridProps> = (props) => {
  return (
    <CommonDataProvider>
      <Box>
        <DataGridExample {...props} />
      </Box>
    </CommonDataProvider>
  );
};

export const Basic = Template.bind({});

function TableToolbar() {
  return (
    <Box sx={{ display: 'flex', alignItems: 'center', px: 2 }}>
      <h4>It was a wild, cold, seasonable night of March</h4>
      <Box sx={{ flex: '1 1 0' }} />
      <Button>{'Open the door'}</Button>
    </Box>
  );
}

export const WithToolbar = Template.bind({});
WithToolbar.args = {
  components: {
    Toolbar: TableToolbar,
  },
};
