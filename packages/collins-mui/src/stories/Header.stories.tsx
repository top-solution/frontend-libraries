import React from 'react';
import { HashRouter as Router, Link, Route, Routes } from 'react-router-dom';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Typography from '@mui/material/Typography';
import Box from '@mui/system/Box';
import { Story, Meta } from '@storybook/react';

import { Header, HeaderProps } from '../components/Header';
import { ChevronDownIcon, ContentCopyIcon } from '../Icons';
import { CommonDataProvider } from './CommonDataProvider';

function BasicMenu() {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <Button
        id="basic-button"
        aria-controls="basic-menu"
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
      >
        Personality
        <ChevronDownIcon />
      </Button>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        <MenuItem onClick={handleClose}>Dr. Jekyll</MenuItem>
        <MenuItem onClick={handleClose}>Mr. Hyde</MenuItem>
      </Menu>
    </div>
  );
}

export default {
  title: 'Collins MUI/Header',
  component: Header,
  argTypes: {},
} as Meta;

const Template: Story<HeaderProps> = (props) => {
  return (
    <CommonDataProvider>
      <Box sx={{}}>
        <Router>
          <Routes>
            <Route
              path="/page-2"
              element={
                <Box>
                  <Header {...props} breadcrumbs={[{ title: 'Second page', url: '/page-2' }]} />
                  <Box sx={{ margin: 'auto', maxWidth: 600, marginTop: 8 }}>
                    <Button component={Link} to="/page-1" sx={{ margin: 'auto' }}>
                      {'Previous'}
                    </Button>
                    <Button component={Link} to="/page-3" sx={{ margin: 'auto' }}>
                      {'Next'}
                    </Button>
                    <Typography sx={{ whiteSpace: 'pre-wrap' }}>
                      {`
                        Mr. Utterson again walked some way in silence and obviously under a weight of consideration. “You are sure he used a key?” he inquired at last.

                        “My dear sir...” began Enfield, surprised out of himself.

                        “Yes, I know,” said Utterson; “I know it must seem strange. The fact is, if I do not ask you the name of the other party, it is because I know it already. You see, Richard, your tale has gone home. If you have been inexact in any point you had better correct it.”

                        “I think you might have warned me,” returned the other with a touch of sullenness. “But I have been pedantically exact, as you call it. The fellow had a key; and what’s more, he has it still. I saw him use it not a week ago.”

                        Mr. Utterson sighed deeply but said never a word; and the young man presently resumed. “Here is another lesson to say nothing,” said he. “I am ashamed of my long tongue. Let us make a bargain never to refer to this again.”

                        “With all my heart,” said the lawyer. “I shake hands on that, Richard.”
                    `}
                    </Typography>
                  </Box>
                </Box>
              }
            />
            <Route
              path="/page-3"
              element={
                <Box>
                  <Header
                    {...props}
                    breadcrumbs={[
                      { title: 'Second page', url: '/page-2' },
                      { title: 'Third page', url: '/page-3' },
                    ]}
                  />
                  <Box sx={{ margin: 'auto', maxWidth: 600, marginTop: 8 }}>
                    <Button component={Link} to="/page-2" sx={{ margin: 'auto' }}>
                      {'Previous'}
                    </Button>
                    <Typography sx={{ whiteSpace: 'pre-wrap' }}>
                      {`
                        That evening Mr. Utterson came home to his bachelor house in sombre spirits and sat down to dinner without relish. It was his custom of a Sunday, when this meal was over, to sit close by the fire, a volume of some dry divinity on his reading desk, until the clock of the neighbouring church rang out the hour of twelve, when he would go soberly and gratefully to bed. On this night however, as soon as the cloth was taken away, he took up a candle and went into his business room. There he opened his safe, took from the most private part of it a document endorsed on the envelope as Dr. Jekyll’s Will and sat down with a clouded brow to study its contents. The will was holograph, for Mr. Utterson though he took charge of it now that it was made, had refused to lend the least assistance in the making of it; it provided not only that, in case of the decease of Henry Jekyll, M.D., D.C.L., L.L.D., F.R.S., etc., all his possessions were to pass into the hands of his “friend and benefactor Edward Hyde,” but that in case of Dr. Jekyll’s “disappearance or unexplained absence for any period exceeding three calendar months,” the said Edward Hyde should step into the said Henry Jekyll’s shoes without further delay and free from any burthen or obligation beyond the payment of a few small sums to the members of the doctor’s household. This document had long been the lawyer’s eyesore.
                    `}
                    </Typography>
                  </Box>
                </Box>
              }
            />
            <Route
              path="*"
              element={
                <Box>
                  <Header {...props} breadcrumbs={[{ title: 'First page', url: '/page-1' }]} />
                  <Box sx={{ margin: 'auto', maxWidth: 600, marginTop: 8 }}>
                    <Button component={Link} to="/page-2" sx={{ margin: 'auto' }}>
                      {'Next'}
                    </Button>
                    <Typography sx={{ whiteSpace: 'pre-wrap' }}>
                      {`
                      The pair walked on again for a while in silence; and then “Enfield,” said Mr. Utterson, “that’s a good rule of yours.”

                      “Yes, I think it is,” returned Enfield.

                      “But for all that,” continued the lawyer, “there’s one point I want to ask: I want to ask the name of that man who walked over the child.”

                      “Well,” said Mr. Enfield, “I can’t see what harm it would do. It was a man of the name of Hyde.”

                      “Hm,” said Mr. Utterson. “What sort of a man is he to see?”

                      “He is not easy to describe. There is something wrong with his appearance; something displeasing, something down-right detestable. I never saw a man I so disliked, and yet I scarce know why. He must be deformed somewhere; he gives a strong feeling of deformity, although I couldn’t specify the point. He’s an extraordinary looking man, and yet I really can name nothing out of the way. No, sir; I can make no hand of it; I can’t describe him. And it’s not want of memory; for I declare I can see him this moment.”
                    `}
                    </Typography>
                  </Box>
                </Box>
              }
            />
          </Routes>
        </Router>
      </Box>
    </CommonDataProvider>
  );
};

export const Basic = Template.bind({});
Basic.args = {
  applicationTitle: 'Collins MUI Storybook',
  menuItems: [
    {
      label: "Drink Jekyll's serum",
      icon: <ContentCopyIcon />,
      onClick: (closeMenu) => {
        // eslint-disable-next-line no-console
        console.log("I'm not feeling good...");
        closeMenu();
      },
    },
  ],
};

export const WithChildren = Template.bind({});
WithChildren.args = {
  applicationTitle: 'Collins MUI Storybook',
  children: <BasicMenu />,
};
