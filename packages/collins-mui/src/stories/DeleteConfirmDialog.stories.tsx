import React, { useCallback, useState } from 'react';
import Button from '@mui/material/Button';
import Box from '@mui/system/Box';
import { Story, Meta } from '@storybook/react';

import { ApiError } from '@top-solution/collins-utils';
import { DeleteConfirmDialog, DeleteConfirmDialogProps } from '../components/DeleteConfirmDialog';
import { CommonDataProvider } from './CommonDataProvider';

export default {
  title: 'Collins MUI/DeleteConfirmDialog',
  component: DeleteConfirmDialog,
  argTypes: {},
} as Meta;

const Template: Story<DeleteConfirmDialogProps> = (props) => {
  const [open, setOpen] = useState<false | 'succeed' | 'fail'>(false);
  const [inProgress, setInProgress] = useState(false);
  const [error, setError] = useState<ApiError | undefined>(undefined);

  const handleConfirm = useCallback(() => {
    setInProgress(true);
    setTimeout(() => {
      setInProgress(false);
      if (open === 'fail') {
        setError({
          status: 'FETCH_ERROR',
          data: undefined,
          error:
            'The phial, to which I next turned my attention, might have been about half full of a blood-red liquor, which was highly pungent to the sense of smell and seemed to me to contain phosphorus and some volatile ether. At the other ingredients I could make no guess. ',
        } as ApiError);
      } else {
        setOpen(false);
      }
    }, 2500);
  }, [open]);

  const handleClose = useCallback(() => {
    setOpen(false);
  }, []);

  return (
    <CommonDataProvider>
      <Box sx={{ maxWidth: 600 }}>
        <Button
          onClick={() => {
            setOpen('succeed');
            setError(undefined);
          }}
        >
          {'Open dialog'}
        </Button>
        <Button
          onClick={() => {
            setOpen('fail');
            setError(undefined);
          }}
        >
          {'Open dialog (request will fail)'}
        </Button>
        <DeleteConfirmDialog
          {...props}
          open={Boolean(open)}
          onConfirm={() => handleConfirm()}
          onClose={() => handleClose()}
          inProgress={inProgress}
          error={error}
        />
      </Box>
    </CommonDataProvider>
  );
};

export const Basic = Template.bind({});
Basic.args = {
  title: `Vuoi davvero eliminare il contatto "Gabriel John Utterson"?`,
  confirmText: 'conferma',
};
