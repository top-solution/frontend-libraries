import React from 'react';
import Box from '@mui/system/Box';
import { Story, Meta } from '@storybook/react';

import { Logo, LogoWide, NotFound, ServerError, Unauthorized } from '../assets';

export default {
  title: 'Collins Inputs/Assets',
  component: Box,
  argTypes: {},
} as Meta;

const Template: Story = (props) => {
  const { Component, otherProps } = props;
  return (
    <Box {...otherProps} sx={{ height: 200, svg: { height: '100%', width: 'auto' } }}>
      <Component />
    </Box>
  );
};

export const LogodStory = Template.bind({});
LogodStory.storyName = 'Logo';
LogodStory.args = {
  Component: Logo,
};

export const LogoWidedStory = Template.bind({});
LogoWidedStory.storyName = 'LogoWide';
LogoWidedStory.args = {
  Component: LogoWide,
};

export const NotFounddStory = Template.bind({});
NotFounddStory.storyName = 'NotFound';
NotFounddStory.args = {
  Component: NotFound,
};

export const ServerErrordStory = Template.bind({});
ServerErrordStory.storyName = 'ServerError';
ServerErrordStory.args = {
  Component: ServerError,
};

export const UnauthorizeddStory = Template.bind({});
UnauthorizeddStory.storyName = 'Unauthorized';
UnauthorizeddStory.args = {
  Component: Unauthorized,
};
