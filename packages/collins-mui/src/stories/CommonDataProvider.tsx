import React from 'react';
import { Provider } from 'react-redux';

import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query';
import {
  AuthGuard,
  AuthCallback,
  CommonDataApiProvider,
  setAuthApiUrl,
  authSliceReducer,
  commonDataApi,
} from '@top-solution/collins-utils';

const appId = `intranet`;
const authApi = `https://mtcommondata.windows.topsolution.dev`;

setAuthApiUrl(`https://mtcommondata.windows.topsolution.dev/api`);

const store = configureStore({
  reducer: {
    auth: authSliceReducer,
    [commonDataApi.reducerPath]: commonDataApi.reducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(commonDataApi.middleware),
});

setupListeners(store.dispatch);

interface CommonDataProviderProps {
  children: React.ReactNode;
}

export function CommonDataProvider(props: CommonDataProviderProps) {
  const { children } = props;
  const searchParams = new URLSearchParams(window.location.search);

  return (
    <CommonDataApiProvider value={{ appId, authApi }}>
      <Provider store={store}>
        {searchParams.get('token') === null ? (
          <AuthGuard authorizeAppKey="intranetTest" redirectUri={window.location.href}>
            {children}
          </AuthGuard>
        ) : (
          <>
            <AuthCallback
              onAuthenticationSuccess={() => {
                //
              }}
            />
            {children}
          </>
        )}
      </Provider>
    </CommonDataApiProvider>
  );
}
