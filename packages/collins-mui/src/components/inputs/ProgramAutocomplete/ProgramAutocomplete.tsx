import React, { useCallback } from 'react';
import Autocomplete, { AutocompleteProps } from '@mui/material/Autocomplete';
import TextField, { TextFieldProps } from '@mui/material/TextField';
import { useReadProgramsQuery } from '@top-solution/collins-utils';

export type ProgramAutocompleteProps<
  Multiple extends boolean | undefined,
  DisableClearable extends boolean | undefined,
  FreeSolo extends boolean | undefined
> = Omit<AutocompleteProps<string, Multiple, DisableClearable, FreeSolo>, 'options' | 'renderInput' | 'css'> &
  Pick<TextFieldProps, 'label' | 'error' | 'helperText' | 'variant' | 'required' | 'sx'>;

function ProgramAutocompleteComponent<
  Multiple extends boolean | undefined,
  DisableClearable extends boolean | undefined,
  FreeSolo extends boolean | undefined
>(props: ProgramAutocompleteProps<Multiple, DisableClearable, FreeSolo>, ref: React.Ref<unknown>): JSX.Element {
  const { value, label, error, helperText, variant, required, sx, ...autocompleteProps } = props;
  const { data, isFetching, error: loadingError } = useReadProgramsQuery();

  const getOptionLabel = useCallback(
    (id: string) => {
      if (data?.map && data.map[id]) {
        return data?.map[id].name;
      }
      return id;
    },
    [data]
  );

  return (
    <Autocomplete
      value={value}
      options={Object.keys(data?.map ?? {})}
      getOptionLabel={getOptionLabel}
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          variant={variant}
          error={Boolean(loadingError) || error}
          helperText={(loadingError && 'message' in loadingError && loadingError.message) || helperText || undefined}
          required={required}
          inputRef={ref}
          sx={sx}
        />
      )}
      loading={isFetching}
      {...autocompleteProps}
    />
  );
}

export const ProgramAutocomplete = React.memo(React.forwardRef(ProgramAutocompleteComponent));
