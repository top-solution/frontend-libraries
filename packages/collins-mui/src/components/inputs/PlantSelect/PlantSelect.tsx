import React, { useCallback } from 'react';
import MenuItem from '@mui/material/MenuItem';
import TextField, { TextFieldProps } from '@mui/material/TextField';
import { Plant, useReadPlantsQuery } from '@top-solution/collins-utils';

export interface PlantSelectProps extends Omit<TextFieldProps, 'value' | 'onChange' | 'select' | 'css'> {
  value: Plant | null;
  onChange: (event: React.ChangeEvent<HTMLInputElement>, value: Plant | null) => void;
}

function PlantSelectComponent(props: PlantSelectProps, ref: React.Ref<unknown>): JSX.Element {
  const { value, onChange, ...textFieldProps } = props;
  const { data } = useReadPlantsQuery();

  const handleChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const id = event.target.value;
      onChange(event, data?.map[+id] ?? null);
    },
    [data, onChange]
  );

  return (
    <TextField
      className="PlantSelect"
      value={value?.id ?? ''}
      onChange={handleChange}
      select
      SelectProps={{ ref }}
      {...textFieldProps}
    >
      <MenuItem>&nbsp;</MenuItem>
      {data?.list.map(({ id, name }) => (
        <MenuItem key={id} value={id}>
          {name}
        </MenuItem>
      ))}
    </TextField>
  );
}

export const PlantSelect = React.memo(React.forwardRef(PlantSelectComponent));
