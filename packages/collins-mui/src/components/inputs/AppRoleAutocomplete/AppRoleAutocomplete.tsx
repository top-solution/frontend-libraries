import React, { useCallback } from 'react';
import Autocomplete, { AutocompleteProps } from '@mui/material/Autocomplete';
import TextField, { TextFieldProps } from '@mui/material/TextField';
import { AppUserRole, useReadAppUserRoleListQuery } from '@top-solution/collins-utils';

export type AppRoleAutocompleteProps<
  Multiple extends boolean | undefined,
  DisableClearable extends boolean | undefined,
  FreeSolo extends boolean | undefined
> = Omit<
  AutocompleteProps<AppUserRole, Multiple, DisableClearable, FreeSolo>,
  'options' | 'renderInput' | 'onInputChange' | 'css'
> &
  Pick<TextFieldProps, 'label' | 'error' | 'helperText' | 'variant' | 'required' | 'sx'> & {
    appId: string;
  };

function AppRoleAutocompleteComponent<
  Multiple extends boolean | undefined,
  DisableClearable extends boolean | undefined,
  FreeSolo extends boolean | undefined
>(props: AppRoleAutocompleteProps<Multiple, DisableClearable, FreeSolo>, ref: React.Ref<unknown>): JSX.Element {
  const { label, error, helperText, variant, required, sx, appId, ...autocompleteProps } = props;
  const { data, isLoading, error: loadingError } = useReadAppUserRoleListQuery(appId);

  const getOptionLabel = useCallback(
    (option: AppUserRole | string) => {
      if (typeof option === 'string') {
        return option;
      }
      if (autocompleteProps.getOptionLabel) {
        return autocompleteProps.getOptionLabel(option);
      }
      return option.name;
    },
    [autocompleteProps]
  );

  return (
    <Autocomplete
      options={data || []}
      isOptionEqualToValue={(option, value) => option.id === value.id}
      getOptionLabel={getOptionLabel}
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          inputRef={ref}
          variant={variant}
          error={Boolean(loadingError) || error}
          helperText={(loadingError && 'message' in loadingError && loadingError.message) || helperText || undefined}
          required={required}
          sx={sx}
        />
      )}
      loading={isLoading}
      {...autocompleteProps}
    />
  );
}

export const AppRoleAutocomplete = React.memo(React.forwardRef(AppRoleAutocompleteComponent));
