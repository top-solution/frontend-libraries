import React, { useCallback, useMemo, useState } from 'react';
import Autocomplete, { AutocompleteInputChangeReason, AutocompleteProps } from '@mui/material/Autocomplete';
import TextField, { TextFieldProps } from '@mui/material/TextField';
import debounce from 'lodash.debounce';
import { FilterOperator, PagedRequestParams, useLazySearchItemQuery } from '@top-solution/collins-utils';

export type PartNumberAutocompleteProps<
  Multiple extends boolean | undefined,
  DisableClearable extends boolean | undefined,
  FreeSolo extends boolean | undefined
> = Omit<
  AutocompleteProps<string, Multiple, DisableClearable, FreeSolo>,
  'options' | 'renderInput' | 'onInputChange' | 'css'
> &
  Pick<TextFieldProps, 'label' | 'error' | 'helperText' | 'required' | 'sx'> & {
    endItemsOnly?: boolean;
  };

function PartNumberAutocompleteComponent<
  Multiple extends boolean | undefined,
  DisableClearable extends boolean | undefined,
  FreeSolo extends boolean | undefined
>(props: PartNumberAutocompleteProps<Multiple, DisableClearable, FreeSolo>, ref: React.Ref<unknown>): JSX.Element {
  const { label, error, value, helperText, endItemsOnly, placeholder, required, sx, ...autocompleteProps } = props;
  const [needle, setNeedle] = useState('');
  const [searchPartNumber, { data: response, error: requestError, isLoading }] = useLazySearchItemQuery();

  const search = useMemo(
    () =>
      debounce(
        (needle: string) => {
          const params: PagedRequestParams & { isEnditem?: boolean } = {
            offset: 0,
            limit: 10,
            sort: ['pn'],
            filters: [{ field: 'pn', operator: FilterOperator.like, value: `${needle}%` }],
          };
          if (endItemsOnly !== undefined) {
            params.isEnditem = endItemsOnly;
          }
          searchPartNumber(params);
        },
        300,
        { leading: false, trailing: true }
      ),
    [endItemsOnly, searchPartNumber]
  );

  const options = useMemo<string[]>(() => {
    let options: string[] = [];
    if (typeof value === 'string') {
      options = [value as string];
    } else if (Array.isArray(value)) {
      options = [...value];
    }
    if (needle.length >= 3 && response?.data) {
      options = [...options, ...response.data.map(({ pn }) => pn)];
    }
    return options;
  }, [needle, response, value]);

  const handleInputChange = useCallback(
    (_: unknown, value: string, reason: AutocompleteInputChangeReason) => {
      setNeedle(value);
      if (reason === 'input' && value.length >= 3) {
        search(value);
      }
    },
    [search]
  );

  return (
    <Autocomplete
      autoComplete
      value={value}
      options={options}
      filterOptions={(x) => x}
      filterSelectedOptions
      onInputChange={handleInputChange}
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          placeholder={placeholder}
          inputRef={ref}
          error={Boolean(requestError) || error}
          helperText={(requestError && 'message' in requestError && requestError.message) || helperText || undefined}
          required={required}
          sx={sx}
        />
      )}
      noOptionsText={needle.length < 3 ? 'Digita almeno 3 caratteri' : undefined}
      loading={isLoading}
      {...autocompleteProps}
    />
  );
}

export const PartNumberAutocomplete = React.memo(React.forwardRef(PartNumberAutocompleteComponent));
