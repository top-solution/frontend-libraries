import React, { useCallback, useEffect, useMemo, useState } from 'react';
import Autocomplete, { AutocompleteInputChangeReason, AutocompleteProps } from '@mui/material/Autocomplete';
import TextField, { TextFieldProps } from '@mui/material/TextField';
import debounce from 'lodash.debounce';
import { useLazySearchUserListQuery, User } from '@top-solution/collins-utils';

export type UserAutocompleteProps<
  Multiple extends boolean | undefined,
  DisableClearable extends boolean | undefined,
  FreeSolo extends boolean | undefined
> = Omit<
  AutocompleteProps<string, Multiple, DisableClearable, FreeSolo>,
  'options' | 'renderInput' | 'onInputChange' | 'css'
> &
  Pick<TextFieldProps, 'label' | 'error' | 'helperText' | 'required' | 'sx'>;

function UserAutocompleteComponent<
  Multiple extends boolean | undefined,
  DisableClearable extends boolean | undefined,
  FreeSolo extends boolean | undefined
>(props: UserAutocompleteProps<Multiple, DisableClearable, FreeSolo>, ref: React.Ref<unknown>): JSX.Element {
  const { label, error, value, helperText, required, sx, ...autocompleteProps } = props;
  const [needle, setNeedle] = useState(props.value ?? '');
  const [searchUser, { data, error: requestError, isFetching }] = useLazySearchUserListQuery();
  const resultsByUsername = useMemo<Map<string, User> | null>(
    () => data?.reduce((map, user) => map.set(user.username, user), new Map<string, User>()) || null,
    [data]
  );

  const search = useMemo(
    () => debounce((needle: string) => searchUser({ needle }), 500, { leading: true }),
    [searchUser]
  );

  const options = useMemo(() => {
    let options: string[] = [];
    if (value === '') {
      options = [value as string];
    }
    if (needle.length >= 3 && data) {
      if (data) {
        options = [...options, ...data.map(({ username }) => username)];
      }
    }
    return options;
  }, [data, needle, value]);

  const getOptionLabel = useCallback(
    (option: string) => {
      if (autocompleteProps.getOptionLabel) {
        return autocompleteProps.getOptionLabel(option);
      }
      const user = resultsByUsername?.get(option);
      if (user) {
        if (user?.lastname) {
          return `${user.lastname} ${user.firstname} (${user.username})`;
        }
        return user.username;
      }
      return option;
    },
    [autocompleteProps, resultsByUsername]
  );

  const handleInputChange = useCallback(
    (_: unknown, value: string, reason: AutocompleteInputChangeReason) => {
      setNeedle(value);
      if (reason === 'input' && value.length >= 3) {
        search(value);
      }
    },
    [search]
  );

  useEffect(
    () => {
      if (needle.length > 3) {
        search(needle as string);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return (
    <Autocomplete
      autoComplete
      value={value}
      options={options}
      filterOptions={(options) => options}
      filterSelectedOptions
      getOptionLabel={getOptionLabel}
      onInputChange={handleInputChange}
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          inputRef={ref}
          error={Boolean(requestError) || error}
          helperText={(requestError && 'message' in requestError && requestError.message) || helperText || undefined}
          required={required}
          sx={sx}
        />
      )}
      noOptionsText={needle.length < 3 ? 'Digita almeno 3 caratteri' : undefined}
      loading={isFetching}
      {...autocompleteProps}
    />
  );
}

export const UserAutocomplete = React.memo(React.forwardRef(UserAutocompleteComponent));
