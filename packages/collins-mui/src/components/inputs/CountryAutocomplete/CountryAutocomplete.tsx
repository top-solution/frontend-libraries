import React from 'react';
import Autocomplete, { AutocompleteProps } from '@mui/material/Autocomplete';
import TextField, { TextFieldProps } from '@mui/material/TextField';
import { useReadCountriesQuery } from '@top-solution/collins-utils';

export type CountryAutocompleteProps<
  Multiple extends boolean | undefined,
  DisableClearable extends boolean | undefined,
  FreeSolo extends boolean | undefined
> = Omit<
  AutocompleteProps<string, Multiple, DisableClearable, FreeSolo>,
  'options' | 'renderInput' | 'onInputChange' | 'css'
> &
  Pick<TextFieldProps, 'label' | 'error' | 'helperText' | 'variant' | 'required' | 'sx'>;

function CountryAutocompleteComponent<
  Multiple extends boolean | undefined,
  DisableClearable extends boolean | undefined,
  FreeSolo extends boolean | undefined
>(props: CountryAutocompleteProps<Multiple, DisableClearable, FreeSolo>, ref: React.Ref<unknown>): JSX.Element {
  const { value, label, error, helperText, variant, required, sx, ...autocompleteProps } = props;
  const { data, isFetching, error: loadingError } = useReadCountriesQuery();

  return (
    <Autocomplete
      value={value}
      options={data?.list.map(({ id }) => id) || []}
      getOptionLabel={(countryCode) => data?.byISO[countryCode]?.name || countryCode}
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          variant={variant}
          error={Boolean(loadingError) || error}
          helperText={(loadingError && 'message' in loadingError && loadingError.message) || helperText || undefined}
          required={required}
          inputRef={ref}
          sx={sx}
        />
      )}
      loading={isFetching}
      {...autocompleteProps}
    />
  );
}

export const CountryAutocomplete = React.memo(React.forwardRef(CountryAutocompleteComponent));
