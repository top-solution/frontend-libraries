import React, { useCallback, useMemo } from 'react';
import Checkbox from '@mui/material/Checkbox';
import CircularProgress from '@mui/material/CircularProgress';
import FormControl, { FormControlProps } from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormGroup from '@mui/material/FormGroup';
import FormHelperText from '@mui/material/FormHelperText';
import FormLabel from '@mui/material/FormLabel';
import { AppUserRole, useReadAppUserRoleListQuery, ADMIN, USER } from '@top-solution/collins-utils';

export interface AppRoleCheckboxGroupProps extends Pick<FormControlProps, 'sx' | 'variant' | 'error'> {
  appId: string;
  label?: string;
  helperText?: string;
  value?: AppUserRole[];
  onChange: (event: React.ChangeEvent<HTMLInputElement>, value: AppUserRole[]) => void;
  disabled?: boolean;
}

function AppRoleCheckboxGroupComponent(props: AppRoleCheckboxGroupProps): JSX.Element {
  const { appId, label, helperText, value, onChange, disabled, ...formControlProps } = props;
  const { data, isLoading, error: loadingError } = useReadAppUserRoleListQuery(appId);

  const checked = useMemo<Map<number, AppUserRole>>(
    () => (value ?? []).reduce((map, role) => map.set(role.id, role), new Map<number, AppUserRole>()),
    [value]
  );

  const otherRolesDisabled = useMemo<boolean>(
    () => Boolean(Array.from(checked.values()).find((role) => role.name === ADMIN || role.name === USER)),
    [checked]
  );

  const handleChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>, role: AppUserRole, isChecked: boolean) => {
      if (role.name === ADMIN || role.name === USER) {
        if (isChecked) {
          checked.clear();
        }
      }
      if (checked.has(role.id)) {
        checked.delete(role.id);
      } else {
        checked.set(role.id, role);
      }

      onChange(event, Array.from(checked.values()));
    },
    [checked, onChange]
  );

  return (
    <FormControl component="fieldset" variant="standard" error={Boolean(loadingError)} {...formControlProps}>
      {label && <FormLabel component="legend">{label}</FormLabel>}
      {isLoading ? (
        <CircularProgress />
      ) : (
        <FormGroup>
          {data?.map((role) => (
            <FormControlLabel
              key={role.id}
              control={
                <Checkbox
                  value={role.id}
                  checked={checked.has(role.id)}
                  disabled={disabled || (otherRolesDisabled && !checked.has(role.id))}
                  onChange={(event, checked) => handleChange(event, role, checked)}
                />
              }
              label={role.name}
            />
          ))}
        </FormGroup>
      )}
      <FormHelperText>
        {(loadingError && 'message' in loadingError && loadingError.message) || helperText}
      </FormHelperText>
    </FormControl>
  );
}

export const AppRoleCheckboxGroup = React.memo(AppRoleCheckboxGroupComponent);
