import React, { useCallback } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import LoadingButton from '@mui/lab/LoadingButton';
import Button from '@mui/material/Button';
import Dialog, { DialogProps } from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import {
  Program,
  ProgramSchema,
  useUpdateProgramMutation,
  useCreateProgramMutation,
} from '@top-solution/collins-utils';
import { ErrorAlert } from '../errors/ErrorAlert';

type ProgramEditDialogProps = Omit<DialogProps, 'onClose'> & {
  initialValue: Program;
  onClose: (value?: Program) => void;
  editMode?: boolean;
};

export function ProgramEditDialog(props: ProgramEditDialogProps): JSX.Element {
  const { initialValue, onClose, editMode, ...dialogProps } = props;
  const [update, updateStatus] = useUpdateProgramMutation();
  const [create, createStatus] = useCreateProgramMutation();
  const {
    handleSubmit,
    control,
    reset,
    setValue,
    formState: { touchedFields },
  } = useForm<Program>({
    defaultValues: initialValue,
    resolver: zodResolver(ProgramSchema),
  });

  const handleNameChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setValue('name', event.target.value);
      if (!editMode && !touchedFields.id) {
        setValue(
          'id',
          event.target.value
            .toLocaleLowerCase()
            .replaceAll(/[^a-z0-9]/g, '-')
            .replaceAll(/[-]{2,}/g, '-')
            .substring(0, 32)
        );
      }
    },
    [editMode, setValue, touchedFields.id]
  );

  const onSubmit = useCallback(
    async (program: Program) => {
      if (editMode) {
        await update(program).unwrap();
      } else {
        await create(program).unwrap();
      }
      onClose(program);
    },
    [create, editMode, onClose, update]
  );

  const onExited = useCallback(
    (node) => {
      reset();
      if (editMode) {
        updateStatus.reset();
      } else {
        createStatus.reset();
      }
      if (dialogProps.TransitionProps?.onExited) {
        dialogProps.TransitionProps.onExited(node);
      }
    },
    [createStatus, dialogProps.TransitionProps, editMode, reset, updateStatus]
  );

  return (
    <Dialog
      maxWidth="sm"
      fullWidth
      {...dialogProps}
      TransitionProps={{
        ...dialogProps.TransitionProps,
        onExited,
      }}
    >
      <form onSubmit={handleSubmit(onSubmit)}>
        <DialogTitle>{editMode ? `Modifica programma ${initialValue.name}` : 'Nuovo programma'}</DialogTitle>
        <DialogContent sx={{ display: 'flex', flexDirection: 'column', '.MuiFormControl-root': { mt: 1 } }}>
          <Controller
            control={control}
            name="name"
            render={({ field, fieldState: { error } }) => (
              <TextField
                label="Nome"
                error={Boolean(error)}
                helperText={error?.message || ' '}
                {...field}
                onChange={handleNameChange}
              />
            )}
          />
          <Controller
            control={control}
            name="id"
            render={({ field, fieldState: { error } }) => (
              <TextField
                label="Codice"
                error={Boolean(error)}
                helperText={`${
                  error?.message ? error?.message + '. ' : ''
                }Caratteri ammessi: lettere minuscole, numeri, trattini (-)`}
                disabled={editMode}
                {...field}
              />
            )}
          />
          {createStatus.error && <ErrorAlert error={createStatus.error} />}
          {updateStatus.error && <ErrorAlert error={updateStatus.error} />}
        </DialogContent>
        <DialogActions>
          <Button color="secondary" onClick={() => onClose()}>
            Annulla
          </Button>
          <LoadingButton
            color="primary"
            variant="contained"
            type="submit"
            loading={updateStatus.isLoading || createStatus.isLoading}
          >
            Salva
          </LoadingButton>
        </DialogActions>
      </form>
    </Dialog>
  );
}
