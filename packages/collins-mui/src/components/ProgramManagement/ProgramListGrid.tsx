import React, { useCallback, useMemo, useState } from 'react';
import {
  GridActionsCellItem,
  GridActionsColDef,
  GridColDef,
  GridFilterModel,
  GridLinkOperator,
  GridPinnedColumns,
  GridRowParams,
  GridSortModel,
  MuiEvent,
  useGridApiRef,
} from '@mui/x-data-grid-pro';
import {
  usePagination,
  Program,
  useReadProgramsQuery,
  useRemoveProgramMutation,
  useAuth,
} from '@top-solution/collins-utils';
import { PencilIcon, TrashCanIcon } from '../../Icons';
import { DataGrid, DataGridWrapper, DataGridWrapperProps } from '../DataGrid';
import { DeleteConfirmDialog } from '../DeleteConfirmDialog';
import { ProgramAddButton } from './ProgramAddButton';
import { ProgramEditDialog } from './ProgramEditDialog';

const pinnedColumns: GridPinnedColumns = { right: ['actions'] };

type ProgramListGrid = Omit<DataGridWrapperProps, 'children'>;

export function ProgramListGrid(props: ProgramListGrid): JSX.Element {
  const { sx, ...dataGridProps } = props;
  const apiRef = useGridApiRef();
  const { isCommonDataAdmin } = useAuth();
  const { page, setPage, pageSize, setPageSize } = usePagination(0);
  const [sortModel, setSortModel] = useState<GridSortModel>([{ field: 'name', sort: 'asc' }]);
  const [filterModel, setFilterModel] = useState<GridFilterModel>({ items: [], linkOperator: GridLinkOperator.And });
  const [programToRemove, setProgramToRemove] = useState<Program | null>(null);
  const [programToEdit, setProgramToEdit] = useState<Program | null>(null);
  const programs = useReadProgramsQuery();
  const [removeProgram, removeProgramStatus] = useRemoveProgramMutation();

  const handleDeleteVendorConfirm = useCallback(async () => {
    if (programToRemove) {
      await removeProgram(programToRemove).unwrap();
      setProgramToRemove(null);
    }
  }, [removeProgram, programToRemove]);

  const handleEditClick = useCallback(
    ({ row }: GridRowParams) =>
      (event: MuiEvent<React.MouseEvent>) => {
        event.stopPropagation();
        setProgramToEdit(row as Program);
      },
    []
  );

  const handleDeleteClick = useCallback(
    ({ row }: GridRowParams) =>
      (event: MuiEvent<React.MouseEvent>) => {
        event.stopPropagation();
        setProgramToRemove(row as Program);
      },
    []
  );

  const columns = useMemo<(GridColDef | GridActionsColDef)[]>(
    () => [
      { field: 'id', headerName: 'Codice', width: 200 },
      { field: 'name', headerName: 'Nome', width: 200 },
      {
        field: 'actions',
        type: 'actions',
        headerName: 'Azioni',
        renderHeader: () => <ProgramAddButton sx={{ mx: 1 }} />,
        getActions: (params: GridRowParams) => {
          const actions = [
            <GridActionsCellItem icon={<PencilIcon />} key="edit" label="Modifica" onClick={handleEditClick(params)} />,
          ];
          if (isCommonDataAdmin) {
            actions.push(
              <GridActionsCellItem
                icon={<TrashCanIcon />}
                key="delete"
                label="Elimina"
                onClick={handleDeleteClick(params)}
              />
            );
          }
          return actions;
        },
      },
    ],
    [handleDeleteClick, handleEditClick, isCommonDataAdmin]
  );

  const handleSortModelChange = useCallback(
    (sortModel: GridSortModel) => {
      setSortModel(sortModel);
      setPage(0);
    },
    [setPage]
  );

  const handleFilterModelChange = useCallback(
    (filterModel: GridFilterModel) => {
      setFilterModel(filterModel);
      setPage(0);
    },
    [setPage]
  );

  return (
    <DataGridWrapper
      sx={{
        ...sx,
        '.MuiIconButton-root svg': {
          width: 20,
          height: 20,
        },
      }}
      {...dataGridProps}
    >
      <DataGrid
        apiRef={apiRef}
        density="compact"
        rows={programs.data?.list || []}
        columns={columns}
        sessionStorageId="ProgramListPageDataGrid"
        loading={programs.isFetching}
        error={programs.error}
        pagination
        page={page}
        onPageChange={setPage}
        pageSize={pageSize}
        onPageSizeChange={setPageSize}
        sortModel={sortModel}
        onSortModelChange={handleSortModelChange}
        onFilterModelChange={handleFilterModelChange}
        filterModel={filterModel}
        editMode="row"
        pinnedColumns={pinnedColumns}
      />
      {programToRemove && (
        <DeleteConfirmDialog
          title={`Vuoi davvero eliminare il programma "${programToRemove.name}"?`}
          confirmText="confermo"
          open={Boolean(programToRemove)}
          inProgress={removeProgramStatus.isLoading}
          error={removeProgramStatus.error}
          onConfirm={() => handleDeleteVendorConfirm()}
          onClose={() => setProgramToRemove(null)}
        />
      )}
      {programToEdit && (
        <ProgramEditDialog
          initialValue={programToEdit}
          open={Boolean(programToEdit)}
          onClose={() => setProgramToEdit(null)}
          editMode
        />
      )}
    </DataGridWrapper>
  );
}
