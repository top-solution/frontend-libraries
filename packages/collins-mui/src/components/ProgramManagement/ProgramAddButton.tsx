import React, { useState } from 'react';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import { Program } from '@top-solution/collins-utils';
import { PlusCircleIcon } from '../../Icons';
import { ProgramEditDialog } from './ProgramEditDialog';

const defaultValue: Program = {
  id: '',
  name: '',
};

export function ProgramAddButton(props: IconButtonProps): JSX.Element {
  const [dialogOpen, setDialogOpen] = useState(false);

  return (
    <>
      <Tooltip title="Aggiungi programma">
        <IconButton color="primary" onClick={() => setDialogOpen(true)} {...props}>
          <PlusCircleIcon />
        </IconButton>
      </Tooltip>
      <ProgramEditDialog initialValue={defaultValue} open={dialogOpen} onClose={() => setDialogOpen(false)} />
    </>
  );
}
