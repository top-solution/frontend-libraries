import React from 'react';
import Chip from '@mui/material/Chip';
import { GridCellParams } from '@mui/x-data-grid-pro';
import { AppUserRole } from '@top-solution/collins-utils';

export function AppUserRoleCell(props: GridCellParams): JSX.Element {
  return (
    <>
      {props.row.roles.map((role: AppUserRole) => (
        <Chip className="user-role-chip" key={role.id} label={role.name} />
      ))}
    </>
  );
}
