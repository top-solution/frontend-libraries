import React, { useCallback, useMemo, useState } from 'react';
import Dialog from '@mui/material/Dialog';
import {
  GridActionsCellItem,
  GridActionsColDef,
  GridColDef,
  GridFilterModel,
  GridLinkOperator,
  GridPinnedColumns,
  GridRowModel,
  GridRowParams,
  GridSortModel,
  MuiEvent,
} from '@mui/x-data-grid-pro';
import { ZodError } from 'zod';
import {
  AppUser,
  getDisplayName,
  usePagination,
  useReadAppUserListQuery,
  useRemoveAppUserMutation,
  ApiError,
  AppUserRole,
  useReadAppUserRoleListQuery,
} from '@top-solution/collins-utils';
import { PencilIcon, TrashCanIcon } from '../../Icons';
import { DataGridWrapper, DataGridWrapperProps, DataGrid, muiFiltersToPagedRequestFilters } from '../DataGrid';
import { DeleteConfirmDialog } from '../DeleteConfirmDialog';
import { ErrorAlert } from '../errors/ErrorAlert';
import { AppUserAddButton } from './AppUserAddButton';
import { AppUserEditDialog } from './AppUserEditDialog';
import { AppUserRoleCell } from './AppUserRoleCell';

export type AppUserListGridProps = Omit<DataGridWrapperProps, 'children'> & {
  appId: string;
};

const pinnedColumns: GridPinnedColumns = { right: ['actions'] };

export function AppUserListGrid(props: AppUserListGridProps): JSX.Element {
  const { appId, sx, ...dataGridProps } = props;
  const { page, setPage, pageSize, setPageSize } = usePagination(0);
  const [updateError, setUpdateError] = useState<ZodError | ApiError | null>(null);
  const [sortModel, setSortModel] = useState<GridSortModel>([{ field: 'lastname', sort: 'asc' }]);
  const [filterModel, setFilterModel] = useState<GridFilterModel>({ items: [], linkOperator: GridLinkOperator.And });
  const [appUserToRemove, setAppUserToRemove] = useState<AppUser | null>(null);
  const [appUserToEdit, setAppUserToEdit] = useState<AppUser | null>(null);

  const readAppUserListParams = useMemo(
    () => ({
      appId,
      limit: pageSize as number,
      offset: (pageSize as number) * page,
      sort: sortModel.map(({ field, sort }) => `${sort === 'desc' ? '-' : ''}${field}`),
      filters: muiFiltersToPagedRequestFilters(filterModel.items),
    }),
    [appId, filterModel.items, page, pageSize, sortModel]
  );
  const appUserList = useReadAppUserListQuery(readAppUserListParams, { skip: pageSize === undefined });
  const [removeAppUser, removeAppUserStatus] = useRemoveAppUserMutation();

  const appUserRoles = useReadAppUserRoleListQuery(appId);

  const handleDeleteVendorConfirm = useCallback(async () => {
    if (appUserToRemove) {
      await removeAppUser({ appId, user: appUserToRemove }).unwrap();
      setAppUserToRemove(null);
    }
  }, [appUserToRemove, removeAppUser, appId]);

  const handleEditClick = useCallback(
    ({ row }: GridRowParams) =>
      (event: MuiEvent<React.MouseEvent>) => {
        event.stopPropagation();
        setAppUserToEdit(row as AppUser);
      },
    []
  );

  const handleDeleteClick = useCallback(
    ({ row }: GridRowParams) =>
      (event: MuiEvent<React.MouseEvent>) => {
        event.stopPropagation();
        setAppUserToRemove(row as AppUser);
      },
    []
  );

  const handleSortModelChange = useCallback(
    (sortModel: GridSortModel) => {
      setSortModel(sortModel);
      setPage(0);
    },
    [setPage]
  );

  const handleFilterModelChange = useCallback(
    (filterModel: GridFilterModel) => {
      setFilterModel(filterModel);
      setPage(0);
    },
    [setPage]
  );

  const columns = useMemo<(GridColDef | GridActionsColDef)[]>(
    () => [
      { field: 'lastname', headerName: 'Cognome', flex: 1, filterable: true, sortable: true },
      { field: 'firstname', headerName: 'Nome', flex: 1, filterable: true, sortable: true },
      { field: 'username', headerName: 'Username', flex: 1, filterable: true, sortable: false },
      {
        field: 'roles',
        headerName: 'Ruoli',
        type: 'singleSelect',
        valueOptions: appUserRoles.data?.map((role) => ({ value: role.id, label: role.name })),
        renderCell: AppUserRoleCell,
        flex: 2,
        filterable: true,
        sortable: false,
        valueFormatter: ({ value }) => (value as AppUserRole[]).map((role) => role.name).join(', '),
      },
      {
        field: 'actions',
        type: 'actions',
        headerName: 'Azioni',
        renderHeader: () => <AppUserAddButton appId={appId} sx={{ mx: 1 }} />,
        getActions: (params: GridRowParams) => [
          <GridActionsCellItem icon={<PencilIcon />} key="edit" label="Modifica" onClick={handleEditClick(params)} />,
          <GridActionsCellItem
            icon={<TrashCanIcon />}
            key="delete"
            label="Elimina"
            onClick={handleDeleteClick(params)}
          />,
        ],
      },
    ],
    [appId, appUserRoles.data, handleDeleteClick, handleEditClick]
  );

  const rows = useMemo<GridRowModel[]>(
    () => appUserList.data?.data.map((user) => ({ ...user, id: user.username })) || [],
    [appUserList.data?.data]
  );

  return (
    <DataGridWrapper
      sx={{
        ...sx,
        '.MuiIconButton-root svg': {
          width: 20,
          height: 20,
        },
        '.user-role-chip': {
          m: '2px',
        },
      }}
      {...dataGridProps}
    >
      {updateError && (
        <Dialog open={true} onClose={() => setUpdateError(null)}>
          <ErrorAlert error={updateError} />
        </Dialog>
      )}
      <DataGrid
        density="standard"
        rows={rows}
        columns={columns}
        sessionStorageId="AppUserListPageDataGrid"
        loading={appUserList.isFetching}
        error={appUserList.error}
        pagination
        paginationMode="server"
        rowCount={appUserList.data?.total || 0}
        page={page}
        onPageChange={setPage}
        pageSize={pageSize}
        onPageSizeChange={setPageSize}
        sortingMode="server"
        sortModel={sortModel}
        onSortModelChange={handleSortModelChange}
        filterMode="server"
        onFilterModelChange={handleFilterModelChange}
        filterModel={filterModel}
        pinnedColumns={pinnedColumns}
      />
      {appUserToEdit && (
        <AppUserEditDialog
          appId={appId}
          open={Boolean(appUserToEdit)}
          initialValue={appUserToEdit}
          onClose={() => setAppUserToEdit(null)}
          editMode
        />
      )}
      {appUserToRemove && (
        <DeleteConfirmDialog
          title={`Vuoi davvero eliminare l'utente "${getDisplayName(appUserToRemove)}" dall'applicazione?`}
          confirmText="confermo"
          open={Boolean(appUserToRemove)}
          inProgress={removeAppUserStatus.isLoading}
          error={removeAppUserStatus.error}
          onConfirm={() => handleDeleteVendorConfirm()}
          onClose={() => setAppUserToRemove(null)}
        />
      )}
    </DataGridWrapper>
  );
}
