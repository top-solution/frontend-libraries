import React, { useCallback } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import LoadingButton from '@mui/lab/LoadingButton';
import Button from '@mui/material/Button';
import Dialog, { DialogProps } from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import {
  AppUser,
  AppUserSchema,
  useUpdateAppUserMutation,
  useCreateAppUserMutation,
} from '@top-solution/collins-utils';
import { ErrorAlert } from '../errors/ErrorAlert';
import { AppRoleCheckboxGroup } from '../inputs/AppRoleCheckboxGroup';
import { UserAutocomplete } from '../inputs/UserAutocomplete';

type AppUserEditDialogProps = Omit<DialogProps, 'onClose'> & {
  appId: string;
  initialValue: AppUser;
  onClose: (value?: AppUser) => void;
  editMode?: boolean;
};

export function AppUserEditDialog(props: AppUserEditDialogProps): JSX.Element {
  const { appId, initialValue, onClose, editMode, ...dialogProps } = props;
  const [update, updateStatus] = useUpdateAppUserMutation();
  const [create, createStatus] = useCreateAppUserMutation();
  const { handleSubmit, control, reset } = useForm<AppUser>({
    defaultValues: initialValue,
    resolver: zodResolver(AppUserSchema),
  });

  const onSubmit = useCallback(
    async (user: AppUser) => {
      if (editMode) {
        await update({ appId, user }).unwrap();
      } else {
        await create({ appId, user }).unwrap();
      }
      onClose(user);
    },
    [appId, create, editMode, onClose, update]
  );

  const onExited = useCallback(
    (node) => {
      reset();
      if (editMode) {
        updateStatus.reset();
      } else {
        createStatus.reset();
      }
      if (dialogProps.TransitionProps?.onExited) {
        dialogProps.TransitionProps.onExited(node);
      }
    },
    [createStatus, dialogProps.TransitionProps, editMode, reset, updateStatus]
  );

  return (
    <Dialog
      maxWidth="sm"
      fullWidth
      {...dialogProps}
      TransitionProps={{
        ...dialogProps.TransitionProps,
        onExited,
      }}
    >
      <form onSubmit={handleSubmit(onSubmit)}>
        <DialogTitle>Modifica utente</DialogTitle>
        <DialogContent sx={{ display: 'flex', flexDirection: 'column', '.MuiFormControl-root': { mt: 1 } }}>
          <Controller
            control={control}
            name="username"
            render={({ field: { onChange, ...field }, fieldState: { error } }) => (
              <UserAutocomplete
                label="Utente"
                onChange={(_, username) => onChange(username)}
                error={Boolean(error)}
                helperText={error?.message ?? ' '}
                disabled={editMode}
                {...field}
              />
            )}
          />
          <Controller
            control={control}
            name="roles"
            render={({ field: { value, onChange }, fieldState: { error } }) => (
              <AppRoleCheckboxGroup
                appId={appId}
                label="Ruoli"
                value={value}
                onChange={(_, roles) => onChange(roles)}
                error={Boolean(error)}
                helperText={error?.message ? 'Selezionare almeno 1 ruolo' : ' '}
              />
            )}
          />
          {updateStatus.error && <ErrorAlert error={updateStatus.error} />}
          {createStatus.error && <ErrorAlert error={createStatus.error} />}
        </DialogContent>
        <DialogActions>
          <Button color="secondary" onClick={() => onClose()}>
            Annulla
          </Button>
          <LoadingButton
            color="primary"
            variant="contained"
            type="submit"
            loading={updateStatus.isLoading || createStatus.isLoading}
          >
            Salva
          </LoadingButton>
        </DialogActions>
      </form>
    </Dialog>
  );
}
