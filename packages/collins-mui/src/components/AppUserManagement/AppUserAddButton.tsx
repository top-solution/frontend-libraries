import React, { useState } from 'react';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import { AppUser } from '@top-solution/collins-utils';
import { PlusCircleIcon } from '../../Icons';
import { AppUserEditDialog } from './AppUserEditDialog';

type AppUserAddButtonProps = IconButtonProps & {
  appId: string;
};

const initialValues = {
  username: '',
  roles: [],
} as unknown as AppUser;

export function AppUserAddButton(props: AppUserAddButtonProps): JSX.Element {
  const { appId, ...buttonProps } = props;
  const [dialogOpen, setDialogOpen] = useState(false);

  return (
    <>
      <Tooltip title="Aggiungi utente">
        <IconButton color="primary" onClick={() => setDialogOpen(true)} {...buttonProps}>
          <PlusCircleIcon />
        </IconButton>
      </Tooltip>
      <AppUserEditDialog
        appId={appId}
        open={dialogOpen}
        initialValue={initialValues}
        onClose={() => setDialogOpen(false)}
      />
    </>
  );
}
