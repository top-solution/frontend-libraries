import React from 'react';
import { Link } from 'react-router-dom';
import AppBar, { AppBarProps } from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { useTheme } from '@mui/material/styles';
import Toolbar from '@mui/material/Toolbar';
import { useAuth } from '@top-solution/collins-utils';
import { Logo } from '../../assets';
import { ChevronRightIcon } from '../../Icons';
import { UserMenu, MenuItem } from './UserMenu';

export type Breadcrumb = {
  title: React.ReactNode;
  url?: string;
};

export interface HeaderProps extends AppBarProps {
  applicationTitle: string;
  breadcrumbs?: Breadcrumb[];
  children?: React.ReactNode | React.ReactNode[];
  menuItems?: MenuItem[];
  smallScreenBreakpoint?: number;
}

export function Header(props: HeaderProps): JSX.Element {
  const { applicationTitle, breadcrumbs, children, menuItems, smallScreenBreakpoint, ...otherProps } = props;
  const { username } = useAuth();
  const theme = useTheme();
  const breakpoint = smallScreenBreakpoint || theme.breakpoints.values.md;

  return (
    <AppBar
      sx={{
        bgcolor: 'grey.900',
        '.MuiButton-root': {
          '&, &.Mui-disabled': {
            color: 'inherit',
          },
        },
        '.breadcrumb-component': {
          display: 'flex',
          alignItems: 'center',
        },
        [theme.breakpoints.down(breakpoint)]: {
          '.app-title, .breadcrumb-component': {
            display: 'none',
          },
        },
      }}
      {...otherProps}
    >
      <Toolbar>
        <Button component={Link} to="/" size="large" sx={{}}>
          <Box component={Logo} sx={{ marginRight: 2, width: 36 }} />
          <span className="app-title">{applicationTitle}</span>
        </Button>
        {breadcrumbs?.map(({ title, url }, index) => (
          <div key={url || index} className="breadcrumb-component">
            <ChevronRightIcon style={{ color: 'inherit' }} />
            <Button component={Link} to={url || ''} disabled={!url} size="large">
              {title}
            </Button>
          </div>
        ))}
        <Box sx={{ flexGrow: 1 }} />
        {children ? (
          <Box sx={{ mr: 1, display: 'flex', alignItems: 'center', flex: '0 1 auto' }}>{children}</Box>
        ) : null}
        {username && <UserMenu menuItems={menuItems} />}
      </Toolbar>
    </AppBar>
  );
}
