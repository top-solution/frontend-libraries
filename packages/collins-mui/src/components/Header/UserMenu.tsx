import React, { useCallback, useMemo, useState } from 'react';
import { AvatarProps } from '@mui/material/Avatar';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { useAuth } from '@top-solution/collins-utils';
import { AccountOutline } from '../../assets';
import { UserAvatar } from './UserAvatar';

export interface MenuItem {
  label: string;
  icon: JSX.Element;
  disabled?: boolean;
  onClick: (closeMenu: () => void) => void;
}
export interface UserMenuProps extends AvatarProps {
  menuItems?: MenuItem[];
}

function UserMenuComponent(props: UserMenuProps): JSX.Element {
  const { menuItems, ...avatarProps } = props;
  const { firstname, lastname, username } = useAuth();
  const [anchorEl, setAnchorEl] = useState(null as null | HTMLElement);

  const data = useMemo(() => {
    if (firstname && lastname) {
      return {
        initials: lastname[0].toUpperCase() + firstname[0].toUpperCase(),
        displayName: `${lastname} ${firstname}`,
      };
    }
    if (username) {
      return {
        initials: username[0].toUpperCase(),
        displayName: username,
      };
    }
    return null;
  }, [firstname, lastname, username]);

  const handleOpen = useCallback((event: React.MouseEvent<HTMLElement>) => setAnchorEl(event.currentTarget), []);
  const handleClose = useCallback(() => setAnchorEl(null), []);

  const menu = useMemo(() => {
    const menu = [];
    if (data?.displayName) {
      menu.push({
        label: data.displayName,
        disabled: true,
        icon: <AccountOutline />,
        onClick: undefined,
      });
    }
    return menu;
  }, [data]);

  return (
    <>
      <IconButton onClick={handleOpen}>
        <UserAvatar username={username} sx={{ bgcolor: 'rgb(180, 65, 22)' }} {...avatarProps} />
      </IconButton>
      <Menu open={Boolean(anchorEl)} onClose={handleClose} anchorEl={anchorEl} keepMounted>
        {menu.map((menuItem, index) => (
          <MenuItem key={index} disabled={menuItem.disabled} onClick={menuItem.onClick}>
            <ListItemIcon sx={{ minWidth: 32 }}>{menuItem.icon}</ListItemIcon>
            <ListItemText primary={menuItem.label} />
          </MenuItem>
        ))}
        {menuItems
          ? [
              <Divider key="divider" />,
              menuItems.map((menuItem, index) => (
                <MenuItem key={index} disabled={menuItem.disabled} onClick={() => menuItem.onClick(handleClose)}>
                  <ListItemIcon sx={{ minWidth: 32 }}>{menuItem.icon}</ListItemIcon>
                  <ListItemText primary={menuItem.label} />
                </MenuItem>
              )),
            ]
          : null}
      </Menu>
    </>
  );
}

export const UserMenu = React.memo(UserMenuComponent);
