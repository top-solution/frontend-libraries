import React, { useMemo } from 'react';
import Avatar, { AvatarProps } from '@mui/material/Avatar';
import Skeleton from '@mui/material/Skeleton';
import { useReadUserQuery } from '@top-solution/collins-utils';

export interface UserAvatarProps extends AvatarProps {
  username: string | null;
  size?: number | Record<string, number>;
}

function UserAvatarComponent(props: UserAvatarProps): React.ReactElement {
  const { username, sx, ...avatarProps } = props;
  const { data: user, isLoading: userLoading } = useReadUserQuery({ username: username ?? '' }, { skip: !username });

  const size = props.size || 40;

  const initials = useMemo(() => {
    if (user?.lastname && user.firstname) {
      return user.lastname[0].toUpperCase() + user.firstname[0].toUpperCase();
    } else if (username) {
      return username[0].toUpperCase();
    }
    return;
  }, [user, username]);

  const fontSize = useMemo(() => {
    if (typeof size === 'number') {
      return size / 2;
    }
    if (typeof size === 'object') {
      const fontSize: Record<string, number> = {};
      Object.entries(size).forEach(([key, value]) => {
        fontSize[key] = value / 2;
      });
      return fontSize;
    }
    return undefined;
  }, [size]);

  if (!username || userLoading) {
    return (
      <Skeleton variant="circular" sx={sx}>
        <Avatar sx={{ width: size, height: size }} />
      </Skeleton>
    );
  }

  return (
    <Avatar {...avatarProps} sx={{ width: size, height: size, fontSize, ...sx }}>
      {initials}
    </Avatar>
  );
}

export const UserAvatar = React.memo(UserAvatarComponent);
