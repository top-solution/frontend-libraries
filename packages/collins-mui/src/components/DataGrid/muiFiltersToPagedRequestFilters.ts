import { GridFilterItem } from '@mui/x-data-grid-pro';
import { FilterOperator, PagedRequestFilter } from '@top-solution/collins-utils';

export function muiFiltersToPagedRequestFilters(filters: GridFilterItem[]): PagedRequestFilter[] {
  const parsedFilters: PagedRequestFilter[] = [];
  for (let i = 0; i < filters.length; i++) {
    const filter = filters[i];
    let value = filter.value;
    let operator: null | FilterOperator = null;

    switch (filter.operatorValue) {
      case 'is':
        if (filter.value !== '' && filter.value !== undefined) {
          operator = FilterOperator.equals;
        }
        break;
      case 'not':
        if (filter.value !== '' && filter.value !== undefined) {
          operator = FilterOperator.notEquals;
        }
        break;
      case 'isAnyOf':
        if (Array.isArray(filter.value) && filter.value.length > 0) {
          operator = FilterOperator.in;
          value = filter.value.join(',');
        }
        break;
      case 'contains':
        if (filter.value) {
          operator = FilterOperator.like;
          value = `%${value}%`;
        }
        break;
      case 'startsWith':
        if (filter.value) {
          operator = FilterOperator.like;
          value = `${value}%`;
        }
        break;
      case 'endsWith':
        if (filter.value) {
          operator = FilterOperator.like;
          value = `%${value}`;
        }
        break;
      case 'isEmpty':
        operator = FilterOperator.equals;
        value = '';
        break;
      case 'isNotEmpty':
        operator = FilterOperator.notEquals;
        value = '';
        break;
      case '=':
      case 'equals':
        if (filter.value !== undefined) {
          operator = FilterOperator.equals;
        }
        break;
      case '!=':
        if (filter.value !== undefined) {
          operator = FilterOperator.notEquals;
        }
        break;
      case '>':
        if (filter.value !== undefined) {
          operator = FilterOperator.greaterThen;
        }
        break;
      case '>=':
        if (filter.value !== undefined) {
          operator = FilterOperator.greaterOrEqualThan;
        }
        break;
      case '<':
        if (filter.value !== undefined) {
          operator = FilterOperator.lessThan;
        }
        break;
      case '<=':
        if (filter.value !== undefined) {
          operator = FilterOperator.lessOrEqualThan;
        }
        break;
      case 'before':
        if (filter.value !== undefined) {
          operator = FilterOperator.lessThan;
        }
        break;
      case 'after':
        if (filter.value !== undefined) {
          operator = FilterOperator.greaterThen;
        }
        break;
      default:
        // eslint-disable-next-line no-console
        console.warn(`Unsupported operator "${filter.operatorValue}" provided`, filter);
        break;
    }

    if (operator && filter.columnField) {
      parsedFilters.push({
        field: filter.columnField,
        value,
        operator,
      });
    }
  }

  return parsedFilters;
}
