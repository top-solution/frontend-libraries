import React from 'react';
import Box, { BoxProps } from '@mui/material/Box';
import Paper from '@mui/material/Paper';

export type DataGridWrapperProps = BoxProps & {
  autoHeight?: boolean;
  offsetHeight?: string | number;
};

const DEFAULT_OFFSET_HEIGHT = 80;

export function DataGridWrapper(props: DataGridWrapperProps): JSX.Element {
  const { autoHeight, offsetHeight, component, children, sx, ...boxProps } = props;

  const height = autoHeight
    ? undefined
    : `calc(100vh - ${typeof offsetHeight === 'string' ? offsetHeight : `${offsetHeight || DEFAULT_OFFSET_HEIGHT}px`})`;

  return (
    <Box
      component={component || Paper}
      sx={{
        height,
        '& .MuiDataGrid-cell .Mui-error': {
          bgcolor: 'error.main',
          color: 'error.contrastText',
        },
        ...sx,
      }}
      {...boxProps}
    >
      {children}
    </Box>
  );
}
