import { useCallback, useMemo } from 'react';
import { GridColDef, GridValueFormatterParams } from '@mui/x-data-grid-pro';
import { useReadProgramsQuery } from '@top-solution/collins-utils';

export function useProgramGridColDef(): GridColDef {
  const { data } = useReadProgramsQuery();
  const valueOptions = useMemo(() => data?.list.map((program) => ({ value: program.id, label: program.name })), [data]);
  const valueFormatter = useCallback(
    ({ value }: GridValueFormatterParams) =>
      typeof value === 'string' ? data?.map[value]?.name || `⚠️ ${value}` : value,
    [data]
  );
  return {
    field: 'programId',
    headerName: 'Programma',
    type: 'singleSelect',
    valueOptions,
    valueFormatter,
  };
}
