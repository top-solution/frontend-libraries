import React, { useMemo } from 'react';
import { styled } from '@mui/material';
import { GridColDef } from '@mui/x-data-grid-pro';
import { useReadCountriesQuery, CountryFlag, CountryFlagProps } from '@top-solution/collins-utils';

const CountryGridCellWrapper = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',

  '.CountryFlag-root': {
    height: '1.5em',
    marginRight: theme.spacing(0.5),
    boxShadow: theme.shadows['2'],
  },
}));

export type CountryGridCellProps = CountryFlagProps & {
  hideFlag?: boolean;
  hideLabel?: boolean;
};

function CountryGridCell(props: CountryGridCellProps): JSX.Element {
  const { countryCode, format, hideFlag, hideLabel } = props;
  const { data } = useReadCountriesQuery();

  return (
    <CountryGridCellWrapper>
      {!hideFlag && <CountryFlag countryCode={countryCode} format={format} />}
      {!hideLabel && (data?.byISO[countryCode]?.name || countryCode)}
    </CountryGridCellWrapper>
  );
}

export type UseCountryGridColDefOptions = Omit<CountryGridCellProps, 'countryCode'>;

export function useCountryGridColDef(options?: UseCountryGridColDefOptions): GridColDef {
  const { data } = useReadCountriesQuery();
  const valueOptions = useMemo(() => data?.list.map((country) => ({ value: country.id, label: country.name })), [data]);
  return {
    field: 'country',
    headerName: 'Nazione',
    type: 'singleSelect',
    valueOptions,
    renderCell: ({ value }) =>
      typeof value === 'string' ? <CountryGridCell countryCode={value} {...options} /> : null,
  };
}
