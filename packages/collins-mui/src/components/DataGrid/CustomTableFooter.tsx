import React from 'react';
import Box from '@mui/material/Box';
import {
  GridFooterContainer,
  GridFooterContainerProps,
  GridRowCount,
  useGridRootProps,
  GridSelectedRowCount,
  useGridApiContext,
  GridToolbarColumnsButton,
  GridToolbarFilterButton,
  GridToolbarDensitySelector,
  GridToolbarExport,
  GridCsvExportOptions,
} from '@mui/x-data-grid-pro';

export type CustomTableFooterProps = GridFooterContainerProps & {
  disableExportSelector?: boolean;
  customButtons?: React.ReactNode[] | React.ReactNode;
  children: React.ReactNode[] | React.ReactNode;
  csvOptions?: GridCsvExportOptions;
};

export function CustomTableFooter(props: CustomTableFooterProps): JSX.Element {
  const { children, customButtons, disableExportSelector, csvOptions, ...otherProps } = props;
  const apiRef = useGridApiContext();
  const rootProps = useGridRootProps();
  const state = apiRef.current.state;
  const totalRowCount = state.rows.totalRowCount;
  const selectedRowCount = state.selection.length;
  const visibleTopLevelRowCount = state.rows.totalTopLevelRowCount;

  const selectedRowCountElement =
    !rootProps.hideFooterSelectedRowCount && selectedRowCount > 0 ? (
      <GridSelectedRowCount selectedRowCount={selectedRowCount} />
    ) : (
      <div />
    );

  const rowCountElement =
    !rootProps.hideFooterRowCount && !rootProps.pagination ? (
      <GridRowCount rowCount={totalRowCount} visibleRowCount={visibleTopLevelRowCount} />
    ) : null;

  const paginationElement = rootProps.pagination &&
    !rootProps.hideFooterPagination &&
    rootProps.components.Pagination && <rootProps.components.Pagination {...rootProps.componentsProps?.pagination} />;

  return (
    <GridFooterContainer {...otherProps}>
      <Box sx={{ pl: 1, flex: 1 }}>
        {!rootProps.disableColumnSelector && <GridToolbarColumnsButton />}
        {!rootProps.disableColumnFilter && <GridToolbarFilterButton />}
        {!rootProps.disableDensitySelector && <GridToolbarDensitySelector />}
        {!disableExportSelector && (
          <GridToolbarExport csvOptions={{ delimiter: ';', utf8WithBom: true, ...csvOptions }} />
        )}
        {customButtons}
      </Box>
      {children}
      {selectedRowCountElement}
      {rowCountElement}
      {paginationElement}
    </GridFooterContainer>
  );
}
