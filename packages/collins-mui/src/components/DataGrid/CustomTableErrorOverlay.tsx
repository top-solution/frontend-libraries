import React from 'react';
import { GridOverlay } from '@mui/x-data-grid-pro';
import { ZodError } from 'zod';
import { ServerErrorData } from '@top-solution/collins-utils';
import { ErrorAlert } from '../errors/ErrorAlert';

type CustomTableErrorOverlayProps = {
  message: string;
  name: string;
  stack: string;
  data: string | ServerErrorData;
  error: string | Error;
};

export function CustomTableErrorOverlay(props: CustomTableErrorOverlayProps): JSX.Element {
  let error: Error | ZodError;
  if (props.name === 'ZodError') {
    error = new ZodError(JSON.parse(props.message));
  } else {
    let message = props.message;
    if (props.data && typeof props.data === 'object' && 'message' in props.data) {
      message = props.data.message;
    } else if (typeof props.error === 'string') {
      message = props.error;
    } else if (props.error && typeof props.error === 'object' && 'message' in props.error) {
      message = props.error.message;
    }
    error = new Error(message);
    error.stack = props.stack;
    error.name = props.name;
  }
  return (
    <GridOverlay>
      <ErrorAlert error={error} sx={{ m: 2 }} />
    </GridOverlay>
  );
}
