import { useCallback, useMemo } from 'react';
import { GridColDef, GridValueFormatterParams } from '@mui/x-data-grid-pro';
import { useReadPlantsQuery } from '@top-solution/collins-utils';

export function usePlantColDef(): GridColDef {
  const { data } = useReadPlantsQuery();
  const valueOptions = useMemo(() => data?.list.map((plant) => ({ value: plant.id, label: plant.name })), [data]);
  const valueFormatter = useCallback(
    ({ value }: GridValueFormatterParams) =>
      typeof value === 'number' ? data?.map[value]?.name || `⚠️ ${value}` : value,
    [data]
  );
  return {
    field: 'plantId',
    headerName: 'Stabilimento',
    type: 'singleSelect',
    valueOptions,
    valueFormatter,
  };
}
