import { useCallback, useMemo, useRef, useState } from 'react';
import {
  GridColDef,
  GridColumnOrderChangeParams,
  GridColumnVisibilityModel,
  GridPinnedColumns,
} from '@mui/x-data-grid-pro';

const SESSION_STORAGE_KEY = 'CUSTOM_GRID_COLUMNS_CONFIG';

type GridColumnsConfigState = {
  pinned?: GridPinnedColumns;
  visible?: Record<string, boolean>;
  order?: string[];
};

type UseGridColumnsConfigParams = {
  columns: GridColDef[];
  gridId?: string;
  pinnedColumns?: GridPinnedColumns;
  columnVisibilityModel?: GridColumnVisibilityModel;
};

export function useGridColumnsConfig(params: UseGridColumnsConfigParams): {
  columns: GridColDef[];
  columnVisibilityModel?: GridColumnVisibilityModel;
  onColumnVisibilityModelChange?: (columnVisibilityModel: GridColumnVisibilityModel) => void;
  onColumnOrderChange?: (params: GridColumnOrderChangeParams) => void;
  pinnedColumns?: GridPinnedColumns;
  onPinnedColumnsChange?: (pinnedColumns: GridPinnedColumns) => void;
} {
  const { gridId } = params;

  const config = useRef<Record<string, GridColumnsConfigState>>(
    JSON.parse(sessionStorage.getItem(SESSION_STORAGE_KEY) || '{}')
  );

  const [columnVisibilityModel, setColumnVisibilityModel] = useState<GridColumnVisibilityModel | undefined>(
    gridId ? { ...params.columnVisibilityModel, ...config.current[gridId]?.visible } : params.columnVisibilityModel
  );

  const [pinnedColumns, setPinnedColumns] = useState(
    gridId ? { ...params.pinnedColumns, ...config.current[gridId]?.pinned } : params.pinnedColumns
  );

  const columns = useMemo<GridColDef[]>(() => {
    if (gridId === undefined) {
      return params.columns;
    }
    const order = config.current[gridId]?.order || params.columns.map((column) => column.field);
    const columnsMap = params.columns.reduce(
      (map, column) => map.set(column.field, column),
      new Map<string, GridColDef>()
    );
    return order.map((field) => ({ ...columnsMap.get(field) } as GridColDef));
  }, [gridId, params.columns]);

  const updateConfig = useCallback(
    (state: GridColumnsConfigState) => {
      if (gridId) {
        const gridConfig: GridColumnsConfigState = {
          ...config.current[gridId],
          ...state,
        };
        config.current[gridId] = gridConfig;
        sessionStorage.setItem(SESSION_STORAGE_KEY, JSON.stringify(config.current));
      }
    },
    [gridId]
  );

  const onColumnVisibilityModelChange = useMemo(
    () =>
      gridId === undefined
        ? undefined
        : (columnVisibilityModel: GridColumnVisibilityModel) => {
            setColumnVisibilityModel(columnVisibilityModel);
            updateConfig({ visible: columnVisibilityModel });
          },
    [gridId, updateConfig]
  );

  const onColumnOrderChange = useMemo(
    () =>
      gridId === undefined
        ? undefined
        : (params: GridColumnOrderChangeParams) => {
            const order = config.current[gridId]?.order || columns.map((column) => column.field);
            const target = order[params.targetIndex];
            const old = order[params.oldIndex];
            order[params.oldIndex] = target;
            order[params.targetIndex] = old;
            updateConfig({ order });
          },
    [columns, gridId, updateConfig]
  );

  const onPinnedColumnsChange = useMemo(
    () =>
      gridId === undefined
        ? undefined
        : (pinned: GridPinnedColumns) => {
            setPinnedColumns(pinned);
            updateConfig({ pinned });
          },
    [gridId, updateConfig]
  );

  return {
    columns,
    columnVisibilityModel,
    onColumnVisibilityModelChange,
    onColumnOrderChange,
    pinnedColumns,
    onPinnedColumnsChange,
  };
}
