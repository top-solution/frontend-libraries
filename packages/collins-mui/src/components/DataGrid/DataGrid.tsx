import React, { useCallback } from 'react';
import {
  DataGridPro,
  DataGridProProps,
  GridLinkOperator,
  GridSortDirection,
  MuiBaseEvent,
  MuiEvent,
} from '@mui/x-data-grid-pro';
import { rowsPerPageOptions } from '@top-solution/collins-utils';
import { CustomTableErrorOverlay } from './CustomTableErrorOverlay';
import { CustomTableFooter } from './CustomTableFooter';
import { useGridColumnsConfig } from './useGridColumnsConfig';

const sortingOrder: GridSortDirection[] = ['asc', 'desc'];

export type DataGridProps = DataGridProProps & {
  sessionStorageId?: string;
};

export function DataGrid(props: DataGridProps): JSX.Element {
  const {
    components,
    componentsProps,
    columns,
    sessionStorageId,
    pinnedColumns,
    columnVisibilityModel,
    ...dataGridProps
  } = props;
  const preventDefaultMui = useCallback((_: unknown, event: MuiEvent<MuiBaseEvent>) => {
    event.defaultMuiPrevented = true;
  }, []);

  const config = useGridColumnsConfig({
    columns,
    pinnedColumns,
    columnVisibilityModel,
    gridId: sessionStorageId,
  });

  return (
    <DataGridPro
      columns={config.columns}
      pinnedColumns={config.pinnedColumns}
      onColumnOrderChange={config.onColumnOrderChange}
      columnVisibilityModel={config.columnVisibilityModel}
      onColumnVisibilityModelChange={config.onColumnVisibilityModelChange}
      onPinnedColumnsChange={config.onPinnedColumnsChange}
      rowsPerPageOptions={rowsPerPageOptions}
      sortingOrder={sortingOrder}
      disableSelectionOnClick
      onRowEditStart={preventDefaultMui}
      onRowEditStop={preventDefaultMui}
      onCellClick={preventDefaultMui}
      components={{
        ErrorOverlay: CustomTableErrorOverlay,
        Footer: CustomTableFooter,
        ...components,
      }}
      componentsProps={{
        ...componentsProps,
        filterPanel: {
          ...componentsProps?.filterPanel,
          linkOperators:
            componentsProps?.filterPanel?.linkOperators ?? props.filterMode === 'server'
              ? [GridLinkOperator.And]
              : undefined,
        },
      }}
      {...dataGridProps}
    />
  );
}
