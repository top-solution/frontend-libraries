import React from 'react';
import Alert, { AlertProps } from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Box from '@mui/material/Box';
import { FetchBaseQueryError } from '@reduxjs/toolkit/query';
import { ZodError } from 'zod';
import { ApiError, ServerError } from '@top-solution/collins-utils';
import { ErrorDetails } from '../ErrorDetails';

function ApiErrorAlert(props: AlertProps & { error: FetchBaseQueryError }): JSX.Element {
  const { error, ...alertProps } = props;
  const serverError = error as ServerError;
  return (
    <Alert severity={error.status >= 500 ? 'error' : 'warning'} {...alertProps}>
      <AlertTitle>Errore del server ({error.status})</AlertTitle>
      {serverError.data?.message && <p>{serverError.data.message}</p>}
      <ErrorDetails title="Dettagli" content={JSON.stringify(error, null, 2)} />
    </Alert>
  );
}

function ValidationErrorAlert(props: AlertProps & { error: ZodError }): JSX.Element {
  const { error, ...alertProps } = props;
  return (
    <Alert severity="warning" {...alertProps}>
      <AlertTitle>Errore di validazione</AlertTitle>
      {error.issues.map((issue, index) => {
        const path = issue.path.length > 0 ? `${issue.path.join('.')}` : '';
        return (
          <p key={`${path}-${index}`}>
            {path && <strong>{path}: </strong>}
            {issue.message}
          </p>
        );
      })}
      <ErrorDetails title="Dettagli" content={JSON.stringify(error, null, 2)} />
    </Alert>
  );
}

export type ErrorAlertProps = AlertProps & {
  error: ZodError | ApiError | Error;
};

export function zodErrorToHumanReadableError(error: ZodError): string {
  return error.issues
    .map((issue) => {
      const path = issue.path.length > 0 ? `${issue.path.join('.')}: ` : '';

      return `${path}${issue.message}${issue.code ? ' – ' + issue.code : ''}`;
    })
    .join(',');
}

function ErrorAlertComponent(props: ErrorAlertProps) {
  const { error, ...alertProps } = props;
  const apiError = error as FetchBaseQueryError;
  const zodError = error as ZodError;
  return (
    <Box sx={{ '& .MuiAlert-message': { flex: '1', maxWidth: 'calc(100% - 32px)' } }}>
      {apiError.status ? (
        <ApiErrorAlert error={apiError} {...alertProps} />
      ) : zodError.issues ? (
        <ValidationErrorAlert error={zodError} {...alertProps} />
      ) : (
        <Alert severity="error" {...alertProps}>
          <AlertTitle>Si è verificato un errore</AlertTitle>
          <Box sx={{ mb: 1 }}>{'message' in error && error.message}</Box>
          {'stack' in error && error.stack && <ErrorDetails title="Dettagli" content={error.stack} />}
        </Alert>
      )}
    </Box>
  );
}

export const ErrorAlert = React.memo(ErrorAlertComponent);
