import React from 'react';
import Box, { BoxProps } from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import ServerErrorImage from '../../../assets/ServerError';
import { ErrorAlert, ErrorAlertProps } from '../../../components/errors/ErrorAlert';

export interface ServerErrorBoxProps extends BoxProps {
  error: ErrorAlertProps['error'];
}

function ServerErrorBoxComponent(props: ServerErrorBoxProps): React.ReactElement {
  const { sx, error, ...BoxProps } = props;
  return (
    <Box sx={{ textAlign: 'center', ...sx }} {...BoxProps}>
      <Typography variant="h4" component="div" sx={{ mb: 4 }}>
        {'Errore del server'}
      </Typography>
      <ServerErrorImage />
      <ErrorAlert error={error} sx={{ textAlign: 'left', mt: 4 }} />
    </Box>
  );
}

export const ServerErrorBox = React.memo(ServerErrorBoxComponent);
