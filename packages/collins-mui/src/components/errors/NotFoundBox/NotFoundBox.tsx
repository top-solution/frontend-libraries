import React from 'react';
import Alert from '@mui/material/Alert';
import Box, { BoxProps } from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import NotFoundImage from '../../../assets/NotFound';

export interface NotFoundBoxProps extends BoxProps {
  description?: string;
}

function NotFoundBoxComponent(props: NotFoundBoxProps): React.ReactElement {
  const { sx, description, ...boxProps } = props;
  return (
    <Box sx={{ textAlign: 'center', ...sx }} {...boxProps}>
      <Typography variant="h4" component="div" sx={{ mb: 4 }}>
        {'Pagina non trovata'}
      </Typography>
      <NotFoundImage />
      {description && (
        <Alert severity="warning" sx={{ textAlign: 'left', mt: 2 }}>
          {description}
        </Alert>
      )}
    </Box>
  );
}

export const NotFoundBox = React.memo(NotFoundBoxComponent);
