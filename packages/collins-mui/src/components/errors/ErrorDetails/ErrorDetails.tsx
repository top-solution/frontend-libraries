import React from 'react';
import Accordion, { AccordionProps } from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Box from '@mui/system/Box';
import copy from 'copy-to-clipboard';
import { ChevronDownIcon, ContentCopyIcon } from '../../../Icons';

export interface ErrorDetailsProps extends Omit<AccordionProps, 'children'> {
  title: string;
  content: string;
}

function ErrorDetailsComponent(props: ErrorDetailsProps): JSX.Element {
  const { title, content, ...accordionProps } = props;

  return (
    <Accordion
      {...accordionProps}
      sx={{
        bgcolor: 'transparent',
        boxShadow: 0,
        outline: 0,
        border: 1,
        borderColor: 'divider',
        '&::before': { content: 'none' },
        ...accordionProps,
      }}
    >
      <AccordionSummary expandIcon={<ChevronDownIcon />}>{title}</AccordionSummary>
      <AccordionDetails
        sx={{ position: 'relative', display: 'flex', alignItems: 'stretch', p: 0, bgcolor: 'rgba(0,0,0,.05)' }}
      >
        <Box
          component="pre"
          sx={{
            flex: '1 1 auto',
            m: 0,
            p: 0,
            input: {
              paddingX: 1,
              height: 30,
              border: 0,
              backgroundColor: 'transparent',
              width: '100%',
              fontSize: 'inherit',
              fontFamily: 'inherit',
              boxSizing: 'border-box',
              borderTopLeftRadius: 0,
              borderTopRightRadius: 0,
              borderBottomRightRadius: 0,
              borderBottomLeftRadius: 2,
              '&:focus': {
                outline: 'none',
              },
            },
          }}
        >
          <input
            value={content}
            onClick={(event) => (event.target as HTMLInputElement).select()}
            onChange={(e) => e.preventDefault()}
          />
        </Box>
        <Tooltip title="Copia">
          <IconButton
            size="small"
            color="secondary"
            onClick={() => copy(content)}
            sx={{ height: 28, width: 28, mx: 0.5, mt: 0.25 }}
          >
            <ContentCopyIcon sx={{ fontSize: '1.2em' }} />
          </IconButton>
        </Tooltip>
      </AccordionDetails>
    </Accordion>
  );
}

export const ErrorDetails = React.memo(ErrorDetailsComponent);
