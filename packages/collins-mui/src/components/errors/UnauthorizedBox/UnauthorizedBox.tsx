import React from 'react';
import Alert from '@mui/material/Alert';
import Box, { BoxProps } from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import UnauthorizedImage from '../../../assets/Unauthorized';

export interface UnauthorizedBoxProps extends BoxProps {
  description?: string;
}

function UnauthorizedBoxComponent(props: UnauthorizedBoxProps): React.ReactElement {
  const { sx, description, ...BoxProps } = props;
  return (
    <Box sx={{ textAlign: 'center', ...sx }} {...BoxProps}>
      <Typography variant="h4" component="div" sx={{ mb: 4 }}>
        {'Accesso non autorizzato'}
      </Typography>
      <UnauthorizedImage />
      {description && (
        <Alert severity="warning" sx={{ textAlign: 'left', mt: 2 }}>
          {description}
        </Alert>
      )}
    </Box>
  );
}

export const UnauthorizedBox = React.memo(UnauthorizedBoxComponent);
