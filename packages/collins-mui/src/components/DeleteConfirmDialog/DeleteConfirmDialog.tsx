import React from 'react';
import { ConfirmDialog, ConfirmDialogProps } from '../ConfirmDialog';

function DeleteConfirmDialogComponent(props: ConfirmDialogProps): JSX.Element {
  return <ConfirmDialog {...props} confirmButtonText={'Elimina'} confirmColor={'error'} />;
}

export const DeleteConfirmDialog = React.memo(DeleteConfirmDialogComponent);
