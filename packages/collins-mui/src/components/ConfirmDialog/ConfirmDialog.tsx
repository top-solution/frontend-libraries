import React, { useCallback, useEffect, useState } from 'react';
import LoadingButton from '@mui/lab/LoadingButton';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Button, { ButtonProps } from '@mui/material/Button';
import Dialog, { DialogProps } from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import { ApiError } from '@top-solution/collins-utils';
import { ErrorAlert } from '../errors/ErrorAlert';

export type DialogOnClose = (
  event: React.UIEvent,
  reason: 'backdropClick' | 'escapeKeyDown' | 'cancelClick' | 'submitClick'
) => void;

export interface ConfirmDialogProps extends Omit<DialogProps, 'onClose' | 'title'> {
  title: React.ReactNode | React.ReactNode[];
  description?: React.ReactNode | React.ReactNode[];
  confirmText?: string;
  confirmButtonText?: string;
  error?: ApiError | Error;
  confirmColor?: ButtonProps['color'];
  inProgress?: boolean;
  onConfirm: () => void;
  onClose: DialogOnClose;
}

function ConfirmDialogComponent(props: ConfirmDialogProps): JSX.Element {
  const {
    title,
    description,
    confirmText,
    error,
    inProgress,
    onClose,
    onConfirm,
    open,
    confirmButtonText,
    confirmColor,
    ...dialogProps
  } = props;
  const [confirmTextInput, setConfirmTextInput] = useState('');

  useEffect(() => {
    if (!open) {
      setConfirmTextInput('');
    }
  }, [open]);

  const handleSubmit = useCallback(
    (event: React.SyntheticEvent<HTMLFormElement>) => {
      event.preventDefault();
      onConfirm();
    },
    [onConfirm]
  );

  return (
    <Dialog
      open={open}
      onClose={onClose}
      {...dialogProps}
      sx={{
        '[role="alert"]': {
          mt: 2,
        },
        code: {
          border: '1px solid currentColor',
          borderRadius: 1,
          paddingY: 0.25,
          paddingX: 0.5,
          marginX: '2px',
        },
        '.MuiDialogContent-root:empty': {
          display: 'none',
        },
      }}
    >
      <form onSubmit={handleSubmit}>
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
          {description}
          {confirmText && !error && (
            <Alert severity="warning">
              <AlertTitle>Questa operazione non è annullabile</AlertTitle>
              Per continuare digita <code>{confirmText}</code>:
              <TextField
                variant="standard"
                fullWidth
                autoFocus={Boolean(confirmText)}
                value={confirmTextInput}
                onChange={(event) => setConfirmTextInput(event.target.value)}
                sx={{ mt: 1 }}
              />
            </Alert>
          )}
          {error && <ErrorAlert error={error} />}
        </DialogContent>
        <DialogActions>
          <Button color="inherit" onClick={(e) => onClose(e, 'cancelClick')}>
            Annulla
          </Button>
          {error ? null : (
            <LoadingButton
              type="submit"
              disabled={Boolean(confirmText) && confirmText !== confirmTextInput}
              loading={inProgress}
              variant="contained"
              color={confirmColor || 'primary'}
              autoFocus={!confirmText}
            >
              {confirmButtonText || 'Ok'}
            </LoadingButton>
          )}
        </DialogActions>
      </form>
    </Dialog>
  );
}

export const ConfirmDialog = React.memo(ConfirmDialogComponent);
