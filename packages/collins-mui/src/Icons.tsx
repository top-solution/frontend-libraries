import React from 'react';
import Box, { BoxProps } from '@mui/material/Box';
import ChevronDown from './assets/ChevronDown';
import ChevronRight from './assets/ChevronRight';
import ContentCopy from './assets/ContentCopy';
import Pencil from './assets/Pencil';
import PlusCircle from './assets/PlusCircle';
import TrashCan from './assets/TrashCan';

export const createIcon = (IconN: typeof ContentCopy) =>
  function Icon(props: BoxProps): JSX.Element {
    return (
      <Box
        component={IconN}
        {...props}
        viewBox="0 0 24 24"
        sx={{
          width: '1em',
          height: '1em',
          fill: 'currentcolor',
          fontSize: '1.5em',
          display: 'inline-block',
          userSelect: 'none',
          flexShrink: 0,
          path: {
            fill: 'currentColor',
          },
          ...props.sx,
        }}
      />
    );
  };

export const ChevronDownIcon = createIcon(ChevronDown);
export const ChevronRightIcon = createIcon(ChevronRight);
export const ContentCopyIcon = createIcon(ContentCopy);
export const PencilIcon = createIcon(Pencil);
export const PlusCircleIcon = createIcon(PlusCircle);
export const TrashCanIcon = createIcon(TrashCan);
