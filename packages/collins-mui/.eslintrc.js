const common = require('../../common/.eslintrc.js');
const path = require('path');

module.exports = {
  ...common,
  parserOptions: {
    ...common.parserOptions,
    project: path.join(__dirname, 'tsconfig.json'),
  },
  ignorePatterns: ['.eslintrc.js', 'rollup.config.js', 'src/assets/**'],
};
