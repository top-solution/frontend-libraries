import React, { useMemo, useState } from 'react';
import { ResourceStatus, ResourceStatusEvent } from './useResource';

function bgResourceColor(status: ResourceStatus) {
  switch (status) {
    case ResourceStatus.FETCHED:
      return '#2e7d32';
    case ResourceStatus.FETCHING:
      return '#f9a825';
    case ResourceStatus.STALE:
      return '#c62828';
  }
  return '#757575';
}

const rootCommonStyle = {
  position: 'fixed',
  right: 0,
  bottom: 0,
  color: '#fafafa',
  backgroundColor: '#383838',
  borderRadius: '2px',
  zIndex: 100000000,
  fontFamily: 'monospace',
} as React.CSSProperties;

const rootMinimizedStyle = {
  ...rootCommonStyle,
  padding: '4px 16px',
  cursor: 'pointer',
} as React.CSSProperties;

const rootStyle = {
  ...rootCommonStyle,
  width: '800px',
  height: '300px',
  display: 'flex',
  flexDirection: 'column',
} as React.CSSProperties;

const tabsStyle = { display: 'flex', width: '100%' } as React.CSSProperties;

const tabStyle = { flex: '1 1 auto', cursor: 'pointer', textAlign: 'center', padding: '8px 0' } as React.CSSProperties;

const ulStyle = { listStyle: 'none', margin: 0, padding: 0, overflow: 'auto' } as React.CSSProperties;

const liStyle = {
  borderBottom: '1px solid #616161',
  padding: '6px 8px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
} as React.CSSProperties;

const statusStyle = {
  fontWeight: 'bold',
  borderRadius: '3px',
  padding: '2px 4px',
  cursor: 'pointer',
} as React.CSSProperties;

const closeButtonStyle = {
  padding: '10px',
  width: '35px',
  textAlign: 'center',
  cursor: 'pointer',
} as React.CSSProperties;

function StatusLabel(props: { status: ResourceStatus }): JSX.Element {
  return (
    <span
      style={{
        ...statusStyle,
        backgroundColor: bgResourceColor(props.status),
      }}
    >
      {props.status}
    </span>
  );
}

function typeMessage(type: ResourceStatusEvent, name: string, status?: ResourceStatus) {
  switch (type) {
    case ResourceStatusEvent.REGISTERED:
      return `Registered "${name}"`;
    case ResourceStatusEvent.UNREGISTERED:
      return `Unregistered "${name}"`;
    case ResourceStatusEvent.FETCHED:
      return (
        <>
          {`Fetched "${name}", previous status was `}
          {status && <StatusLabel status={status} />}
        </>
      );
    case ResourceStatusEvent.CHANGED_STATE:
      return (
        <>
          {`"${name}" changed state to `}
          {status && <StatusLabel status={status} />}
        </>
      );
  }
  return '';
}

interface UseResourceDebugPanelProps {
  statuses: Record<string, ResourceStatus>;
  debugEvents: [number, ResourceStatusEvent, string, ResourceStatus?][];
}

export default function UseResourceDebugPanel(props: UseResourceDebugPanelProps) {
  const [events, setEvents] = useState(false);
  const [minimized, setMinimized] = useState(false);

  const eventItems = useMemo(() => {
    const items = [];

    for (let i = 0; i < props.debugEvents.length; i++) {
      const [timestamp, type, name, status] = props.debugEvents[i];
      items.push(
        <li key={`e-${timestamp}-${i}`} style={liStyle}>
          {`${new Date(timestamp).toISOString()}`}
          <span>{typeMessage(type, name, status)}</span>
        </li>
      );
    }

    return items;
  }, [props.debugEvents]);

  const statusItems = useMemo(() => {
    const items = [];

    for (const [name, status] of Object.entries(props.statuses)) {
      items.push(
        <li key={`s-${name}`} style={liStyle}>
          {`${name}`}
          <StatusLabel status={status} />
        </li>
      );
    }

    return items;
  }, [props.statuses]);

  if (minimized) {
    return (
      <div onClick={() => setMinimized(false)} style={rootMinimizedStyle}>
        {'useResource() 🗖'}
      </div>
    );
  }

  return (
    <div style={rootStyle}>
      <div style={tabsStyle}>
        <div
          style={{ ...tabStyle, backgroundColor: events ? undefined : '#ffffff16' }}
          onClick={() => setEvents(false)}
        >
          Status
        </div>
        <div style={{ ...tabStyle, backgroundColor: events ? '#ffffff16' : undefined }} onClick={() => setEvents(true)}>
          Events
        </div>
        <div style={closeButtonStyle} onClick={() => setMinimized(true)}>
          🗕
        </div>
      </div>
      <ul style={ulStyle}>{events ? eventItems : statusItems}</ul>
    </div>
  );
}
