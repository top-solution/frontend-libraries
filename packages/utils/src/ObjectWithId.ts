export type ObjectWithId = {
  id: string | number;
};
