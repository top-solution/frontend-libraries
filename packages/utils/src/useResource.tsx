import React, { useEffect, useState, useContext, useCallback, useMemo } from 'react';
import { RequestError } from './RequestError';
import { RequestState } from './RequestState';
import { usePrevious } from './usePrevious';
import UseResourceDebugPanel from './UseResourceDebugPanel';

export enum ResourceStatus {
  NOT_FETCHED = 'NOT_FETCHED',
  FETCHING = 'FETCHING',
  FETCHED = 'FETCHED',
  STALE = 'STALE',
}

export enum ResourceStatusEvent {
  REGISTERED = 'REGISTERED',
  UNREGISTERED = 'UNREGISTERED',
  FETCHED = 'FETCHED',
  CHANGED_STATE = 'CHANGED_STATE',
}

type ResourceContextValue = {
  statuses: Record<string, ResourceStatus>;
  setResourceStatus: (name: string, status: ResourceStatus) => void;
  registerResource: (name: string, fn: () => void) => void;
  unregisterResource: (name: string) => void;
};

const ResourceContext = React.createContext<ResourceContextValue>({
  statuses: {},
  setResourceStatus: () => {
    /* */
  },
  registerResource: () => {
    /* */
  },
  unregisterResource: () => {
    /* */
  },
});

interface ResourceProviderProps {
  children: React.ReactNode;
  debug?: boolean | undefined;
}

/**
 * Centralized provider to sync together all resources hook and prevent multiple resource fetches.
 * Must wrap all components using any resource hook.
 */
export const ResourceProvider = (props: ResourceProviderProps): JSX.Element => {
  const [statuses, setStatuses] = useState<Record<string, ResourceStatus>>({});
  const [fetchers, setFetchers] = useState<Record<string, () => void>>({});
  const [debugEvents, setDebugEvents] = useState<[number, ResourceStatusEvent, string, ResourceStatus?][]>([]);

  const setResourceStatus = useCallback(
    (name: string, status: ResourceStatus) => {
      setStatuses((prevState) => ({ ...prevState, [name]: status }));
      setDebugEvents((ev) => {
        return [...ev, [new Date().valueOf(), ResourceStatusEvent.CHANGED_STATE, name, status]];
      });
    },
    [setStatuses]
  );

  /**
   * Registers a new resource with the associated fetching function, and marks the new resource as not
   * fetched yet.
   * @param name - The resource name
   * @param fn - The fetcher function
   */
  const registerResource = (name: string, fn: () => void) => {
    if (!statuses[name]) {
      setFetchers((prevState) => ({ ...prevState, [name]: fn }));
      setResourceStatus(name, ResourceStatus.NOT_FETCHED);
      if (props.debug === true) {
        setDebugEvents((ev) => {
          return [...ev, [new Date().valueOf(), ResourceStatusEvent.REGISTERED, name]];
        });
      }
    }
  };

  /**
   * Registers a new resource with the associated fetching function, and marks the new resource as not
   * fetched yet.
   * @param name - The resource name
   * @param fn - The fetcher function
   */
  const unregisterResource = (name: string) => {
    if (statuses[name]) {
      setFetchers((prevState) => {
        const newFetchers = { ...prevState };
        delete newFetchers[name];
        return newFetchers;
      });
      setStatuses((prevState) => {
        const newStatuses = { ...prevState };
        delete newStatuses[name];
        return newStatuses;
      });

      if (props.debug === true) {
        setDebugEvents((ev) => [...ev, [new Date().valueOf(), ResourceStatusEvent.UNREGISTERED, name]]);
      }
    }
  };

  /**
   * The fetching is delegated to a single provider: this prevents fetching the same resource multiple
   * times across the application.
   */
  useEffect(() => {
    let changed = false;
    const newStatuses = Object.keys(statuses).reduce((newStatuses, name) => {
      let status = statuses[name];

      if (statuses[name] === ResourceStatus.NOT_FETCHED || statuses[name] === ResourceStatus.STALE) {
        const previusState = statuses[name];

        fetchers[name]();
        changed = true;
        status = ResourceStatus.FETCHING;

        if (props.debug === true) {
          setDebugEvents((ev) => [...ev, [new Date().valueOf(), ResourceStatusEvent.FETCHED, name, previusState]]);
        }
      }
      return {
        ...newStatuses,
        [name]: status,
      };
    }, {});

    if (changed) {
      setStatuses((prevState) => ({
        ...prevState,
        ...newStatuses,
      }));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [statuses]);

  const debugPanel = useMemo(() => {
    if (props.debug !== true) {
      return null;
    }

    return <UseResourceDebugPanel statuses={statuses} debugEvents={debugEvents} />;
  }, [props.debug, statuses, debugEvents]);

  return (
    <ResourceContext.Provider value={{ statuses, setResourceStatus, registerResource, unregisterResource }}>
      {debugPanel}
      {props.children}
    </ResourceContext.Provider>
  );
};

export type Resource<T, R extends RequestState = RequestState> = [T, R, RequestError | null, () => void];

/**
 * Hook to access a single resource without having to call the fetch function in an useEffect.
 * This is not meant to be used directly in components, but rather in state hooks.
 * @param data The resource data straight outta redux
 * @param fetcher The fetcher function
 * @param requestState The RequestState object associated with the fetcher function above
 */
export function useResource<T, R extends RequestState>(
  name: string,
  data: T,
  fetcher: () => void,
  requestState: R,
  options?: Partial<{ purgeOnNameChange: boolean }>
): Resource<T, R> {
  const { statuses, setResourceStatus, registerResource, unregisterResource } = useContext(ResourceContext);

  useEffect(() => {
    registerResource(name, fetcher);
  }, [name, fetcher, registerResource]);

  const previousName = usePrevious(name) || name;

  useEffect(() => {
    if (options?.purgeOnNameChange) {
      if (previousName !== name) {
        unregisterResource(previousName);
      }
    }
  }, [name, previousName, options, unregisterResource]);

  const currentStatus = useMemo(() => statuses[name], [name, statuses]);

  useEffect(() => {
    if (!requestState.inProgress && requestState.lastUpdate) {
      // TODO: Remove this deprectation
      if (requestState.lastUpdate instanceof Date) {
        // eslint-disable-next-line no-console
        console.warn(
          'Deprecation: Dates are not plain objects and should not used in redux stores. See https://redux.js.org/style-guide/style-guide#do-not-put-non-serializable-values-in-state-or-actions'
        );
      }

      if (currentStatus === ResourceStatus.FETCHING) {
        setResourceStatus(name, ResourceStatus.FETCHED);
      }
    }
  }, [name, currentStatus, requestState, setResourceStatus]);

  // Call this to invalidate the current resource cached values - it will trigger a re-fetch
  const invalidate = useCallback(() => {
    setResourceStatus(name, ResourceStatus.STALE);
  }, [name, setResourceStatus]);

  return [data, requestState, requestState.error, invalidate];
}
