import { RequestError } from './RequestError';

export type RequestState = {
  inProgress: boolean;
  lastUpdate: null | number | string | Date; // TODO: Remove Date as it is not serializable
  error: null | RequestError;
};
