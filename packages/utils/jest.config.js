module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  globals: {
    window: {
      document: {},
      location: {},
    }
  }
};