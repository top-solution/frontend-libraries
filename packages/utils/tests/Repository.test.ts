import { Repository } from '../src';

const API_URL = 'https://example.com';
const RESOURCE_URL = '/test';
class MockRepository extends Repository {
  getAxiosConfig = jest.fn(() => Promise.resolve(this.axios));
}

const basicConfiguration = {
  apiURL: API_URL,
  resourceURL: RESOURCE_URL,
};

let repo: MockRepository;

beforeEach(() => {
  repo = new MockRepository(basicConfiguration);
});

describe('Test Repository configuration', () => {
  test('create a basic Repository', async () => {
    expect.assertions(3);

    const getResult = await repo.getAxiosConfig();
    expect(getResult.defaults.baseURL).toEqual(`${API_URL}${RESOURCE_URL}`);
    expect(getResult.defaults.headers['Content-Type']).toEqual('application/json');

    expect(repo.getAxiosConfig).toHaveBeenCalled();
  });

  test('Create a Repository overwriting the Content-Type', async () => {
    expect.assertions(1);

    const contentType = 'multipart/form-data; boundary=something';

    repo = new MockRepository({
      ...basicConfiguration,
      axiosConfig: {
        headers: {
          'Content-Type': contentType,
        },
      },
    });

    const getResult = await repo.getAxiosConfig();
    expect(getResult.defaults.headers['Content-Type']).toEqual(contentType);
  });

  test('Create a Repository with an additional axios header', async () => {
    expect.assertions(1);

    const proxyAuthorization = 'Basic YWxhZGRpbjpvcGVuc2VzYW1l';

    repo = new MockRepository({
      ...basicConfiguration,
      axiosConfig: {
        headers: {
          'Proxy-Authorization': proxyAuthorization,
        },
      },
    });

    const getResult = await repo.getAxiosConfig();
    expect(getResult.defaults.headers['Proxy-Authorization']).toEqual(proxyAuthorization);
  });

  test('Create a Repository with additional axios configuration', async () => {
    expect.assertions(1);

    const proxyConfig = {
      host: '10.100.100.1',
      port: 5000,
    };

    repo = new MockRepository({
      ...basicConfiguration,
      axiosConfig: {
        proxy: proxyConfig,
      },
    });

    const getResult = await repo.getAxiosConfig();
    expect(getResult.defaults.proxy).toEqual(proxyConfig);
  });
});

describe('Test token updating', () => {
  test('Token is not defined on initialization', async () => {
    expect.assertions(1);

    const getResult = await repo.getAxiosConfig();
    expect(getResult.defaults.headers).not.toHaveProperty('Authorization');
  });

  test('Update token', async () => {
    expect.assertions(3);

    let getResult = await repo.getAxiosConfig();
    expect(getResult.defaults.headers).not.toHaveProperty('Authorization');

    const token = 'add4c1f3-5209-4caf-87d7-f88fbfd5f235';

    repo.updateAuthToken(token);

    getResult = await repo.getAxiosConfig();
    expect(getResult.defaults.headers).toHaveProperty('Authorization');

    expect(getResult.defaults.headers['Authorization']).toEqual(`Bearer ${token}`);
  });
});
