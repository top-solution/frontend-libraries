import { useState, useCallback, useMemo, useEffect } from 'react';
import debounce from 'lodash.debounce';
import { SchemaOf, ValidationError } from 'yup';

const extractPathRoot = /^([^.[]+)[.[]+(.+)/;

export type FormError = ValidationError & { nested?: Partial<Record<string, ValidationError>> };
export type FormValues = { [field: string]: unknown };
export type FormTouchedFields<T> = Partial<Record<keyof T, boolean>>;
export type FormErrors<T> = Partial<Record<keyof T, FormError>>;

export type FormState<T> = {
  isPristine: boolean;
  isValid: boolean;
  values: T;
  touched: FormTouchedFields<T>;
  errors: FormErrors<T>;
};

type UseFormReturnType<T> = {
  form: FormState<T>;
  validate: (field?: keyof T) => Promise<void>;
  reset: () => void;
  resetTouched: () => void;
  resetErrors: () => void;
  setValue: (value: unknown, field: keyof T) => void;
  setTouched: (field: keyof T) => void;
  replaceValues: (values: Partial<T>) => void;
  onSubmit: (event?: React.FormEvent) => Promise<void>;
};

type UseFormParams<T> = {
  initialValues: T;
  schema: SchemaOf<unknown>;
  handleSubmit?: (values: T) => void;
  skipValidationOnChange?: boolean;
};

function isValidationError(error: ValidationError | Error): error is ValidationError {
  return 'path' in error && 'inner' in error && Array.isArray(error.inner);
}

function yupErrorToFormErrors<T>(validationError: ValidationError) {
  if (!validationError.inner) {
    return {};
  }

  return validationError.inner.reduce((errors: FormErrors<T>, error: ValidationError) => {
    if (error.path) {
      const pathComponents = extractPathRoot.exec(error.path);
      const path = pathComponents && pathComponents.length > 2 ? pathComponents[1] : error.path;
      const previousError = errors[path as keyof T];
      const currentError = previousError || ({ ...error, path } as FormError);

      if (path !== error.path && currentError) {
        currentError.nested = {
          ...currentError.nested,
          [error.path]: error,
        };
      }

      return {
        ...errors,
        [path]: currentError,
      };
    }

    return errors;
  }, {});
}

/**
 * Custom hook to quickly create React forms with an opinionated beavior.
 * Validation is intended to be managed automatically by this hook, using [`yup`](https://github.com/jquense/yup) schemas.
 * @param useFormParams the form configuration object
 * @param useFormParams.initialValues the form initial values
 * @param useFormParams.schema  the yup validation schema
 * @param useFormParams.handleSubmit the function to be called on submit if the form is valid
 */
export default function useForm<T extends FormValues>({
  initialValues,
  schema,
  handleSubmit,
  skipValidationOnChange,
}: UseFormParams<T>): UseFormReturnType<T> {
  const [values, setValues] = useState(initialValues);
  const [errors, setErrors] = useState<FormErrors<T>>({});
  const [touched, setTouched] = useState<FormTouchedFields<T>>({});

  const isValid = useMemo(() => Object.keys(errors).length === 0, [errors]);
  const isPristine = useMemo(() => !Object.values(touched).some(Boolean), [touched]);

  /**
   * Returns a `FormErrors` object containing all the validation errors for the provided values.
   * * @param values the values to be checked
   */
  const getErrors = useCallback(
    async (values: T) => {
      try {
        await schema.validate({ ...values }, { abortEarly: false });
      } catch (error) {
        if (!isValidationError(error)) {
          throw error;
        }

        return yupErrorToFormErrors<T>(error);
      }

      return {};
    },
    [schema]
  );

  /**
   * Reset all fields to the initial values, clears all the errors and touched flags,
   * the form `isPristine` again
   */
  const reset = useCallback(async () => {
    setValues(initialValues);
    setTouched({});
    setErrors(await getErrors(initialValues));
  }, [initialValues, getErrors]);

  /**
   * Clears all the touched flags, the form `isPristine` again
   */
  const resetTouched = useCallback(async () => {
    setTouched({});
  }, []);

  /**
   * Clears all the form errors
   */
  const resetErrors = useCallback(async () => {
    setErrors({});
  }, []);

  /**
   * Validates the form against the passed values.
   * @param values the values to validate the form against
   */
  const validate = useCallback(
    async (values: T) => {
      const errors = await getErrors(values);

      setErrors(errors);
    },
    [getErrors, setErrors]
  );

  const debouncedValidate = useMemo(() => debounce(validate, 200, { leading: true }), [validate]);

  /**
   * Updates a form field.
   * @param value the value to be set
   * @param field the name of the field to be updated
   */
  const setValue = useCallback(
    (value: unknown, field: keyof T) => {
      setValues((values) => ({
        ...values,
        [field]: value,
      }));
    },
    [setValues]
  );

  /**
   * Replaces current form values with the values provided.
   * If some fields are not specified, those fields will remain untouched in the form model.
   * It's possibile to force a validation against the updated values.
   * @param values the values to be updated
   */
  const replaceValues = useCallback(
    (newValues: Partial<T>) => {
      setValues((values) => ({
        ...values,
        ...newValues,
      }));
    },
    [setValues]
  );

  /**
   * Validates the form, then calls the `handleSubmit` function when valid.
   * If a submit event is passed, it will `event.preventDefault()`.
   * @event event the submit form event
   */
  const onSubmit = useCallback(
    async (event?: React.FormEvent) => {
      if (event && event.preventDefault) {
        event.preventDefault();
      }

      const errors = await getErrors(values);

      if (Object.keys(errors).length === 0 && handleSubmit) {
        handleSubmit(values);
      } else {
        setTouched(
          Object.keys(values).reduce(
            (touched, key) => ({
              ...touched,
              [key]: true,
            }),
            {}
          )
        );
      }
    },
    [getErrors, values, handleSubmit]
  );

  const userDebouncedValidate = useCallback(() => {
    return debouncedValidate(values);
  }, [debouncedValidate, values]);

  const userSetTouched = useCallback(
    (field: keyof T) => {
      setTouched((touched) => ({
        ...touched,
        [field]: true,
      }));
    },
    [setTouched]
  );

  useEffect(() => {
    validate(initialValues);
  }, [validate, initialValues]);

  useEffect(() => {
    if (skipValidationOnChange !== true) {
      debouncedValidate(values);
    }
  }, [debouncedValidate, values, skipValidationOnChange]);

  return {
    form: {
      values,
      errors,
      isValid,
      touched,
      isPristine,
    },
    validate: userDebouncedValidate,
    reset,
    resetTouched,
    resetErrors,
    setValue,
    replaceValues,
    onSubmit,
    setTouched: userSetTouched,
  };
}
