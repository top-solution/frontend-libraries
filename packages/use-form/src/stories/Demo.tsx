import React, { useEffect, useState } from 'react';
import * as yup from 'yup';
import useForm from '../useForm';

import './Demo.css';

const schema = yup.object().shape({
  name: yup.string().required(),
  email: yup.string().email().required(),
  role: yup
    .object()
    .shape({
      id: yup.number().positive(),
    })
    .required(),
  autodeleted: yup.string(),
});

export type DemoProps = {
  initialValues: DemoForm;
  onSubmit: (values: DemoForm) => void;
};

type Role = {
  id: number;
  label: string;
};

type DemoForm = {
  name: string;
  email: string;
  role: Role | null;
  autodeleted: string;
};

export const roles = [
  { id: 2, label: 'Invalid role' },
  { id: 1, label: 'Valid role' },
] as Role[];

export function Demo(props: DemoProps) {
  const { initialValues } = props;
  const [skipValidationOnChange, setSkipValidationOnChange] = useState(false);
  const handleSubmit = (values: DemoForm) => {
    props.onSubmit(values);
  };

  const { form, setValue, onSubmit, setTouched, reset, resetTouched, resetErrors } = useForm<DemoForm>({
    initialValues,
    schema,
    handleSubmit,
    skipValidationOnChange,
  });

  useEffect(() => {
    return setValue('', 'autodeleted');
  }, [form.values.role, setValue]);

  return (
    <div className="container">
      <form onSubmit={onSubmit} className="form">
        <input
          type="text"
          name="name"
          placeholder="Name"
          value={form.values.name}
          onChange={(event) => setValue(event.target.value, 'name')}
          onBlur={() => setTouched('name')}
        />
        {form.errors.name && form.touched.name ? <p style={{ color: 'red' }}>{form.errors.name.errors[0]}</p> : <p></p>}
        <input
          type="text"
          name="email"
          placeholder="Email"
          value={form.values.email}
          onChange={(event) => setValue(event.target.value, 'email')}
          onBlur={() => setTouched('email')}
        />
        {form.errors.email && form.touched.email ? (
          <p style={{ color: 'red' }}>{form.errors.email.errors[0]}</p>
        ) : (
          <p></p>
        )}
        <select
          value={form.values.role?.id}
          onChange={(event) => {
            setValue(roles.find(({ id }) => id === parseInt(event.target.value)) || null, 'role');
          }}
          onBlur={() => setTouched('role')}
        >
          <option></option>
          {roles.map(({ id, label }) => (
            <option key={id} value={id}>
              {label}
            </option>
          ))}
        </select>
        {form.errors.role && (form.touched.role || form.values.role) ? (
          <p style={{ color: 'red' }}>{form.errors.role.errors[0]}</p>
        ) : (
          <p></p>
        )}
        <input
          type="text"
          name="autodeleted"
          placeholder={'This field is wiped each time "Role" changes'}
          value={form.values.autodeleted}
          onChange={(event) => setValue(event.target.value, 'autodeleted')}
          onBlur={() => setTouched('autodeleted')}
        />
        {form.errors.autodeleted && form.touched.autodeleted ? (
          <p style={{ color: 'red' }}>{form.errors.autodeleted.errors[0]}</p>
        ) : (
          <p></p>
        )}
        <label htmlFor="skipvalidation">
          <input
            type="checkbox"
            id="skipvalidation"
            name="skipvalidation"
            checked={skipValidationOnChange}
            onChange={() => setSkipValidationOnChange(!skipValidationOnChange)}
          ></input>
          Skip validation on change
        </label>
        <br></br>
        <button type="button" onClick={reset}>
          {'Reset'}
        </button>
        <button type="button" onClick={resetTouched}>
          {'Reset Touched'}
        </button>
        <button type="button" onClick={resetErrors}>
          {'Reset Errors'}
        </button>
        <button type="submit" disabled={!form.isValid}>
          Submit (disabled until form is valid)
        </button>
        <button type="submit">Submit</button>
      </form>
      <div className="state">
        <pre>
          <code>{JSON.stringify(form, null, 2)}</code>
        </pre>
      </div>
    </div>
  );
}
