import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import { Demo, DemoProps, roles } from './Demo';

import Docs from './useForm.mdx';

export default {
  title: 'Example/useForm',
  component: Demo,
  argTypes: {
    onSubmit: { action: 'onSubmit' },
  },
  parameters: {
    docs: {
      page: Docs,
    },
  },
} as Meta;

const Template: Story<DemoProps> = (args) => <Demo {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  initialValues: {
    name: '',
    email: '',
    role: null,
  },
};

export const InitalValues = Template.bind({});
InitalValues.args = {
  initialValues: {
    name: 'Jake Hunter',
    email: 'jake@hunter.com',
    role: roles[1],
  },
};

// export const Secondary = Template.bind({});
// Secondary.args = {
//   label: 'Button',
// };

// export const Large = Template.bind({});
// Large.args = {
//   size: 'large',
//   label: 'Button',
// };

// export const Small = Template.bind({});
// Small.args = {
//   size: 'small',
//   label: 'Button',
// };
