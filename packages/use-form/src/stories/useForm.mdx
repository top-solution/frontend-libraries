# useForm

Custom hook to quickly create React forms with an opinionated behavior.
Validation is intended to be managed automatically by this hook, using [`yup`](https://github.com/jquense/yup)
schemas.

## Usage

See also the sources of this Storybook Stories.

```jsx
import React from 'react';
import * as yup from 'yup';
import useForm from '../useForm';

const schema = yup.object().shape({
  name: yup.string().required(),
  email: yup.string().email().required(),
});

export type DemoProps = {
  initialValues: DemoForm;
  onSubmit: (values: DemoForm) => void;
};

type DemoForm = {
  name: string;
  email: string;
};

const handleSubmit = (values: DemoForm) => {
  props.onSubmit(values);
};

const { form, setValue, onSubmit, setTouched, reset } = useForm<DemoForm>({
  initialValues: props.initialValues,
  schema,
  handleSubmit
});

return (
  <form onSubmit={onSubmit}>
    <input
      type="text"
      name="name"
      value={form.values.name}
      onChange={(event) => setValue(event.target.value, 'name')}
      onBlur={() => setTouched('name')}
    />
    {form.errors.name && form.touched.name && (
      <p style={{ color: 'red' }}>{form.errors.name.errors[0]}</p>
    )}
    <input
      type="text"
      name="email"
      value={form.values.email}
      onChange={(event) => setValue(event.target.value, 'email')}
      onBlur={() => setTouched('email')}
    />
    {form.errors.email && form.touched.email && (
      <p style={{ color: 'red' }}>{form.errors.email.errors[0]}</p>
    )}
    <button type="button" onClick={reset}>
      {'Reset'}
    </button>
    <button type="submit">
      {'Submit'}
    </button>
  </form>
);
```

## Motivation
We were tired to reinvent the wheel on each React form on each project. We tried some libraries
(like [React Hook Form](https://react-hook-form.com/)) but they are so un-opinionated we had to
write copious amounts of glue code anyway.

[Material UI](https://material-ui.com/) sure didn't help.

This is an attempt to write an all-in-on form hook starting from few UI/UX assumptions about the
form behavior.

## Concepts

### Field
Each field of the form, indexed by a `string`.

### Errors
All form fields can have one or more *errors*, depending on the result of the validation. All
errors are instances of the `yup` [`ValidationError`](https://github.com/jquense/yup#validationerrorerrors-string--arraystring-value-any-path-string).
Validation is asynchronous, so the `errors` object returned by this hook may change some time
after a value is set.

### Valid
The form is *valid* when no field has errors.

### Touched
A field can be set *touched*, this is useful to hide a validation error until the user has
made some kind of interaction with the relative input.

The *touched* status is not set automatically but must be set after a UI
event, like an input `blur` event.

### Pristine
The form is *valid* when no field has been touched.

## Requirements
`yup` and `React` are peer dependencies (this library uses the ones installed on the host project).

## API

`useForm<T>(initialValues: T, schema: Schema<unknown>, handleSubmit?: (values: T) => void): UseFormReturnType<T>`

### Type variables
* `T`: this is the type of the form values.

### Arguments

* `initialValues`: initial values of the form. Used only when the form is created and when
    `reset()` is called.
* `schema`: the `Yup` schema to validate the form against.
* `handleSubmit(values: T)`: your custom submit function. This is when the `useForm` `onSubmit`
    function is called, **only** when the validation passes.

### Returned value

* `UseFormReturnType`: object.
  * `form: FormState<T>`
    * `values: T`: the current form values.
    * `touched: FormTouchedFields<T>`: a map containing the touched state for each field.
    * `errors: FormErrors<T>`: a map containing the errors (see above) for each field.
    * `isPristine: boolean`: the pristine status of the form.
    * `isValid: boolean`: the valid status of the form.
  * `setValue: (value: unknown, field: keyof T, skipValidation?: boolean) => void`: set the value
      of a field and trigger validation.
    * `value: unknown`: the new value of the field.
    * `field: keyof T`: the field name.
    * `skipValidation?: boolean`: if true, the validation function will not be called.
  * `replaceValues: (values: Partial<T>, skipValidation?: boolean) => void`: set the value of a
      field and trigger validation.
    * `values: Partial<T>`: the new form values, a subset of the form values.
    * `skipValidation?: boolean`: if true, the validation function will not be called.
  * `setTouched: (field: keyof T) => void`: marks a field as touched (see above).
    * `field: keyof T`: the field name.
  * `onSubmit: (event?: React.FormEvent) => Promise<void>`: the function to call to submit the
      form. Validation will be performed.
  * `validate: (field?: keyof T) => Promise<void>`: the validate function. It can be called
      explicitly when needed. This function is debounced.
  * `reset: () => void`: the reset function.