import { useCallback, useEffect, useMemo, useState } from 'react';
import { useReadAppConfigQuery, useUpdateAppConfigMutation } from '../api/appConfigApi';
import { defaultPageSize, rowsPerPageOptions } from '../entities/Pagination';

type UsePageSize = {
  isLoading: boolean;
  pageSize: number | undefined;
  setPageSize: (pageSize: number) => void;
};

type UsePagination = UsePageSize & {
  page: number;
  setPage: (page: number) => void;
};

export function usePageSize(): UsePageSize {
  const [pageSizeState, setPageSizeState] = useState<undefined | number>();

  const { data: config, isLoading } = useReadAppConfigQuery(undefined, {
    skip: pageSizeState !== undefined,
  });
  const [updateConfig] = useUpdateAppConfigMutation();

  const setPageSize = useCallback(
    (pageSize: number) => {
      setPageSizeState(pageSize);
      updateConfig({ ...config, pageSize });
    },
    [config, updateConfig]
  );
  const pageSize = useMemo(() => {
    if (pageSizeState) {
      return pageSizeState;
    }
    if (typeof config?.pageSize === 'number' && rowsPerPageOptions.indexOf(config.pageSize) > -1) {
      return config.pageSize;
    }
    if (!isLoading) {
      return defaultPageSize;
    }
  }, [config?.pageSize, isLoading, pageSizeState]);

  return {
    isLoading,
    pageSize,
    setPageSize,
  };
}

export function usePagination(page: number): UsePagination {
  const { pageSize, setPageSize, isLoading } = usePageSize();
  const [pageState, setPageState] = useState(page);

  useEffect(() => {
    setPageState(0);
  }, [pageSize]);

  return {
    isLoading,
    pageSize,
    setPageSize,
    page: pageState,
    setPage: setPageState,
  };
}
