import { useCallback, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLazyGetTokenQuery } from '../api/commonDataApi';
import { AuthSliceState, setTokenAction } from '../store/authSlice';

type RootState = {
  auth: AuthSliceState;
};

type UseAuth = {
  token: string | null;
  roles: string[] | null;
  expiration: number | null;
  username: string | null;
  firstname: string | null;
  lastname: string | null;
  appId: string | null;
  plantId: number | undefined;
  isAdmin: boolean;
  isCommonDataAdmin: boolean;
  hasRole: (role: string) => boolean;
  isExpired: () => boolean;
  setToken: (token: string) => void;
  refreshToken: () => void;
  lastUpdate: number | null;
};

export function useAuth(): UseAuth {
  const dispatch = useDispatch();

  const { token, lastUpdate, username, firstname, lastname, appId, plantId, roles, expiration, commonDataRoles } =
    useSelector((state: RootState) => state.auth);

  const hasRole = useCallback((role: string) => roles.indexOf(role) > -1, [roles]);

  const isAdmin = useMemo(() => hasRole('admin'), [hasRole]);

  const isCommonDataAdmin = useMemo(() => commonDataRoles.indexOf('admin') > -1, [commonDataRoles]);

  const isExpired = useCallback(() => expiration !== null && Date.now() >= expiration, [expiration]);

  const [refreshToken, { data: updatedToken }] = useLazyGetTokenQuery();

  const setToken = useCallback(
    (token: string) => {
      dispatch(setTokenAction({ token }));
    },
    [dispatch]
  );

  useEffect(() => {
    if (updatedToken && updatedToken !== token) {
      setToken(updatedToken);
    }
  }, [updatedToken, token, setToken]);

  return {
    token,
    lastUpdate,
    username,
    firstname,
    lastname,
    appId,
    plantId,
    roles,
    expiration,
    isAdmin,
    isCommonDataAdmin,
    hasRole,
    isExpired,
    setToken,
    refreshToken,
  };
}
