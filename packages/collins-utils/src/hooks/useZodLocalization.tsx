import { useEffect } from 'react';
import { z } from 'zod';

function useZodLocalization(): void {
  useEffect(() => {
    const customErrorMap: z.ZodErrorMap = (issue, ctx) => {
      if (issue.code === z.ZodIssueCode.invalid_type) {
        if (issue.received === 'null' || issue.received === 'undefined') {
          return { message: 'Campo obbligatorio' };
        }

        if (issue.expected === 'number') {
          return { message: 'Numero non valido' };
        }
        if (issue.expected === 'date') {
          return { message: 'Data non valida' };
        }
        if (issue.expected === 'object') {
          return { message: 'Oggetto non valido' };
        }

        return { message: 'Tipo non valido' };
      }

      if (issue.code === z.ZodIssueCode.too_small) {
        if (issue.type === 'number') {
          return { message: `Minimo: ${(issue as z.ZodTooSmallIssue).minimum ?? 1}` };
        }
        if (issue.type === 'string') {
          return { message: `Deve contenere almeno ${(issue as z.ZodTooSmallIssue).minimum ?? 1} caratteri` };
        }
        return { message: `Minimo: ${(issue as z.ZodTooSmallIssue).minimum ?? 1}` };
      }

      if (issue.code === z.ZodIssueCode.too_big) {
        if (issue.type === 'number') {
          return { message: `Massimo: ${(issue as z.ZodTooBigIssue).maximum ?? 1}` };
        }
        if (issue.type === 'string') {
          return { message: `Deve contenere al massimo ${(issue as z.ZodTooBigIssue).maximum ?? 1} caratteri` };
        }
        return { message: `Massimo: ${(issue as z.ZodTooBigIssue).maximum ?? 1}` };
      }

      // if (issue.code === z.ZodIssueCode.custom) {
      //   return { message: `less-than-${(issue.params || {}).minimum}` };
      // }

      return { message: ctx.defaultError };
    };

    z.setErrorMap(customErrorMap);
  }, []);
}

export const ZodLocalizationProvider = (): null => {
  useZodLocalization();
  return null;
};
