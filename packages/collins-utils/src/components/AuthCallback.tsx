import React, { useEffect } from 'react';
import { useAuth } from '../hooks/useAuth';

type QueryParams = {
  token?: string;
  error?: string;
  path?: string;
};

function parse(search: string): QueryParams {
  const searchParams = new URLSearchParams(search);
  const token = searchParams.get('token') ?? undefined;
  const error = searchParams.get('error') ?? undefined;
  const path = searchParams.get('path') ?? undefined;
  return { token, error, path };
}

export interface AuthCallbackProps {
  onAuthenticationSuccess: (path: string) => void;
  ErrorComponent?: (props: { error: string }) => React.ReactElement;
  ProgressComponent?: () => React.ReactElement;
}

export function AuthCallback(props: AuthCallbackProps): React.ReactElement | null {
  const { onAuthenticationSuccess, ErrorComponent, ProgressComponent } = props;
  const { token, error, path } = parse(document.location.search);
  const auth = useAuth();

  useEffect(() => {
    if (token && token !== auth.token && !error) {
      auth.setToken(token);
    }
  }, [token, auth, error]);

  useEffect(() => {
    if (token === auth.token && !error) {
      onAuthenticationSuccess(path || '/');
    }
  }, [auth.token, error, onAuthenticationSuccess, path, token]);

  if (error) {
    if (ErrorComponent) {
      return <ErrorComponent error={error} />;
    } else {
      return <div>{error}</div>;
    }
  }

  if (ProgressComponent) {
    return <ProgressComponent />;
  }

  return null;
}
