import React, { useMemo } from 'react';
import { useCommonDataApiParams } from '../api/commonDataApi';
import { AppUserRole } from '../entities/User';
import { useAuth } from '../hooks/useAuth';

export interface AuthGuardProps {
  authorizeAppKey?: string;
  authorizeRole?: (role: AppUserRole['name']) => boolean;
  redirectUri?: string;
  unauthorizedFallback?: React.ReactNode;
  loaderComponent?: React.ReactNode;
  children: React.ReactNode;
}

export function AuthGuard(props: AuthGuardProps): React.ReactElement | null {
  const { authorizeAppKey, authorizeRole, redirectUri, unauthorizedFallback, loaderComponent, children } = props;
  const { lastUpdate, isExpired, roles } = useAuth();
  const { authApi, appId } = useCommonDataApiParams();

  const authorized = useMemo(() => {
    if (!authorizeRole) {
      return true;
    }

    if (roles && authorizeRole) {
      for (let i = 0; i < roles.length; i++) {
        if (authorizeRole(roles[i]) === true) {
          return true;
        }
      }
    }

    return false;
  }, [authorizeRole, roles]);

  const authenticated = useMemo(() => {
    if (!lastUpdate || isExpired()) {
      if (!appId) {
        throw new Error('AuthGuard: appId is not provided');
      }

      const searchParams = new URLSearchParams(window.location.search).toString();

      if (document.location.pathname === '/authorize') {
        throw new Error(`/authorize is handled by AuthGuard: redirect is blocked as it would cause a redirect loop`);
      }

      if (document.location.pathname === '/login') {
        throw new Error(`/login is handled by AuthGuard: redirect is blocked as it would cause a redirect loop`);
      }

      if (document.location.pathname === '/auth') {
        throw new Error(`/auth is handled by AuthGuard: redirect is blocked as it would cause a redirect loop`);
      }

      const locationWithQueryString = encodeURIComponent(
        `${document.location.pathname}${searchParams ? `?${searchParams}` : ''}`
      );
      const encodedRedirectUri = encodeURIComponent(
        redirectUri ?? `${document.location.origin}/auth?path=${locationWithQueryString}`
      );

      if (authorizeAppKey) {
        document.location.href = `${authApi}/authorize?appKey=${authorizeAppKey}&appID=${appId}&redirectUri=${encodedRedirectUri}`;
      } else {
        document.location.href = `${authApi}/login?appID=${appId}&redirectUri=${encodedRedirectUri}`;
      }

      return false;
    }
    return true;
  }, [appId, authApi, authorizeAppKey, isExpired, lastUpdate, redirectUri]);

  if (!lastUpdate) {
    if (loaderComponent) {
      return <>{loaderComponent}</>;
    }
    return null;
  }

  if (!authorized) {
    if (unauthorizedFallback) {
      return <>{unauthorizedFallback}</>;
    }
    return <em>{'Accesso non autorizzato'}</em>;
  }

  if (!authenticated) {
    return null;
  }

  return <>{children}</>;
}
