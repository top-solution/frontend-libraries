import React from 'react';
import { useReadCountriesQuery } from '..';
import { useCommonDataApiParams } from '../api/commonDataApi';

export interface CountryFlagProps {
  countryCode: string;
  format?: 'png' | 'svg';
}

function RawCountryFlag(props: CountryFlagProps): JSX.Element {
  const { countryCode, format } = props;
  const { data } = useReadCountriesQuery();
  const country = data?.byISO[countryCode];

  const params = useCommonDataApiParams();

  return (
    <img
      className="CountryFlag-root"
      src={`${params.authApi}/flags/${countryCode}.${format ?? 'svg'}`}
      alt={country?.name || countryCode}
      title={country?.name || countryCode}
    />
  );
}

export const CountryFlag = React.memo(RawCountryFlag);
