import { z } from 'zod';

export const ProgramSchema = z.object({
  name: z.string().min(3),
  id: z
    .string()
    .min(3)
    .max(32)
    .regex(/^[a-z0-9-]{3,32}$/),
});

export type Program = z.infer<typeof ProgramSchema>;
