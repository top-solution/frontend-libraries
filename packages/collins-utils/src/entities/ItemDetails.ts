import { z } from 'zod';

export interface ItemChild {
  pn: string;
  description: string;
  level: number;
  childList: ItemChild[];
}

const ItemChild: z.ZodSchema<ItemChild> = z.lazy(() =>
  z.object({
    pn: z.string().min(3),
    description: z.string(),
    level: z.number(),
    childList: z.array(ItemChild),
  })
);

export const ItemDetailsSchema = z.object({
  pn: z.string().min(3),
  description: z.string(),
  endItemList: z.string().array(),
  isEndItem: z.boolean(),
  childList: ItemChild.array(),
});

export type ItemDetails = z.infer<typeof ItemDetailsSchema>;
