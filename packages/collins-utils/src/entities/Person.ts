import { z } from 'zod';
import { PlantSchema } from './Plant';

export const PersonSchema = z.object({
  username: z.string().optional(),
  lastname: z.string().optional(),
  firstname: z.string().optional(),
  title: z.string().optional(),
  company: z.string().optional(),
  initials: z.string().optional(),
  country: z.string().optional(),
  department: z.string().optional(),
  departmentDescription: z.string().optional(),
  division: z.string().optional(),
  email: z.string().email().optional(),
  employeeCode: z.string().optional(),
  faxNumber: z.string().optional(),
  id: z.number(),
  location: z.string().optional(),
  mobileNumber: z.string().optional(),
  mobileNumberShort: z.string().optional(),
  phoneNumber: z.string().optional(),
  phoneNumberShort: z.string().optional(),
  plantID: z.number().optional(),
  postalCode: z.string().optional(),
  state: z.string().optional(),
  streetAddress: z.string().optional(),
});

export type Person = z.infer<typeof PersonSchema>;

export function getPersonDisplayName(person: Person): string {
  if (person.lastname && person.firstname) {
    return `${person.lastname} ${person.firstname}`;
  }
  return person.lastname || person.company || person.username || person.id.toString();
}

export const PersonFormSchema = z
  .object({
    displayname: z.string(), // Fake field used to validate the presence of at least 1 name
    lastname: z.string(),
    firstname: z.string(),
    company: z.string(),
    mobileNumber: z.string(),
    mobileNumberShort: z.string(),
    phoneNumber: z.string(),
    phoneNumberShort: z.string(),
    email: z.union([z.string().email(), z.string().max(0)]),
    plant: PlantSchema.nullable(),
  })
  .refine((data) => data.firstname || data.lastname || data.company, {
    message: 'Fill at least one of the following fields: lastname, firstname, company',
    path: ['displayname'],
  });

export type PersonForm = z.infer<typeof PersonFormSchema>;
