import { z } from 'zod';

export const PlantSchema = z.object({
  id: z.number().positive(),
  name: z.string().min(1),
  slug: z.string().min(1),
});

export type Plant = z.infer<typeof PlantSchema>;
