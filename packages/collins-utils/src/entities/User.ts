import { z } from 'zod';

export const ADMIN = 'admin' as const;
export const USER = 'user' as const;

export type UserRole = 'admin' | 'user' | string;

export const AppUserRoleSchema = z.object({
  id: z.number(),
  name: z.union([z.string(), z.literal(ADMIN), z.literal(USER)]),
});

export type AppUserRole = z.infer<typeof AppUserRoleSchema>;

export const UserSchema = z.object({
  username: z.string(),
  roles: AppUserRoleSchema.array().optional(),
  lastname: z.string().optional(),
  firstname: z.string().optional(),
});

export type User = z.infer<typeof UserSchema>;

export function getDisplayName(user: User): string {
  if (user.lastname && user.firstname) {
    return `${user.lastname} ${user.firstname}`;
  }
  return user.lastname || user.username;
}

export const AppUserSchema = z.object({
  username: z.string(),
  roles: AppUserRoleSchema.array().min(1),
});

export type AppUser = z.infer<typeof AppUserSchema>;
