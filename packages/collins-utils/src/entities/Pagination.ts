import { z } from 'zod';

export enum FilterOperator {
  equals = 'eq',
  notEquals = 'neq',
  like = 'like',
  notLike = 'notlike',
  isNull = 'isNull',
  isNotNull = 'isNotNull',
  lessThan = 'lt',
  lessOrEqualThan = 'lte',
  greaterThen = 'gt',
  greaterOrEqualThan = 'gte',
  in = 'in',
  notIn = 'notIn',
}

export const rowsPerPageOptions = [5, 10, 20, 30, 50, 100];
export const defaultPageSize = rowsPerPageOptions[3];

export const pagedRequestFilterSchema = z.object({
  field: z.string(),
  operator: z.nativeEnum(FilterOperator),
  value: z.string(),
});

export type PagedRequestFilter = z.infer<typeof pagedRequestFilterSchema>;

export const pagedRequestParamsSchema = z.object({
  offset: z.number().min(0),
  limit: z
    .number()
    .min(rowsPerPageOptions[0])
    .max(rowsPerPageOptions[rowsPerPageOptions.length - 1]),
  sort: z.string().array().optional(),
  filters: pagedRequestFilterSchema.array().optional(),
});

export type PagedRequestParams = z.infer<typeof pagedRequestParamsSchema>;

export type PagedResponse<T> = {
  offset: number;
  limit: number;
  total: number;
  data: T[];
};

export function createPagedResponseSchema<T>(itemSchema: z.ZodSchema<T>): z.ZodSchema<PagedResponse<T>> {
  const schema = z.object({
    offset: z.number(),
    limit: z.number(),
    total: z.number(),
    data: itemSchema.array(),
  });
  return schema;
}
