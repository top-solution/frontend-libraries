import { z } from 'zod';

export const ItemSchema = z.object({
  pn: z.string().min(3),
  endItemList: z.string().array(),
  isEndItem: z.boolean(),
});

export type Item = z.infer<typeof ItemSchema>;
