import { z } from 'zod';

export const CountrySchema = z.object({
  id: z.string().length(2),
  name: z.string().min(1),
  isEU: z.boolean(),
});

export type Country = z.infer<typeof CountrySchema>;
