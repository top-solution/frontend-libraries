export type ServerErrorData = {
  name: string;
  id: string;
  message: string;
  temporary: boolean;
  timeout: boolean;
  fault: boolean;
};

export type ServerError = Error & {
  status: number;
  data: ServerErrorData;
};
