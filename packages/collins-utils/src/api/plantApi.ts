import { Plant, PlantSchema } from '../entities/Plant';
import { commonDataApi } from './commonDataApi';
const url = 'v1/plants';

const plantApi = commonDataApi.injectEndpoints({
  endpoints: (builder) => ({
    readPlants: builder.query<{ list: Plant[]; map: Record<string, Plant> }, void>({
      query: () => url,
      transformResponse: (data) => {
        const list = PlantSchema.array().parse(data);
        const map: Record<string, Plant> = {};
        list.forEach((plant) => (map[plant.id] = plant));
        return { list, map };
      },
      keepUnusedDataFor: 3600,
    }),
  }),
  overrideExisting: false,
});

export const { useReadPlantsQuery } = plantApi;
