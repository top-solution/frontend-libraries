import { Item, ItemSchema } from '../entities/Item';
import { ItemDetails, ItemDetailsSchema } from '../entities/ItemDetails';
import { createPagedResponseSchema, PagedRequestParams, PagedResponse } from '../entities/Pagination';
import { commonDataApi } from './commonDataApi';
import { formatQueryParams } from './utils';

const url = 'v1/items';

const itemApi = commonDataApi.injectEndpoints({
  endpoints: (builder) => ({
    searchItem: builder.query<PagedResponse<Item>, PagedRequestParams & { isEnditem?: boolean }>({
      query: (params) => ({ url, params: formatQueryParams(params) }),
      transformResponse: (data) => createPagedResponseSchema(ItemSchema).parse(data),
    }),
    readItemDetails: builder.query<ItemDetails, string>({
      query: (pn) => `${url}/${encodeURIComponent(pn)}`,
      transformResponse: (data) => ItemDetailsSchema.parse(data),
    }),
  }),
  overrideExisting: false,
});

export const { useSearchItemQuery, useReadItemDetailsQuery, useLazySearchItemQuery } = itemApi;
