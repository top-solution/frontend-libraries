import { createPagedResponseSchema, PagedRequestParams, PagedResponse } from '../entities/Pagination';
import { AppUser, AppUserRole, AppUserRoleSchema, User, UserSchema } from '../entities/User';
import { commonDataApi, TAG_TYPES } from './commonDataApi';
import { formatQueryParams } from './utils';

const url = 'v1/apps';

const appUserApi = commonDataApi.injectEndpoints({
  endpoints: (builder) => ({
    readAppUserRoleList: builder.query<AppUserRole[], string>({
      query: (appId) => `${url}/${appId}/roles`,
      transformResponse: (data) => AppUserRoleSchema.array().parse(data),
    }),
    readAppUserList: builder.query<PagedResponse<User>, PagedRequestParams & { appId: string }>({
      query: ({ appId, ...params }) => ({ url: `${url}/${appId}/users`, params: formatQueryParams(params) }),
      transformResponse: (data) => createPagedResponseSchema(UserSchema).parse(data),
      providesTags: () => [{ type: TAG_TYPES.APP_USER, id: 'LIST' }],
    }),
    createAppUser: builder.mutation<void, { appId: string; user: AppUser }>({
      query: ({ appId, user: { username, roles } }) => ({
        url: `${url}/${appId}/users/${username}`,
        method: 'PATCH',
        body: { roles: roles.map(({ id }) => id) },
      }),
      invalidatesTags: () => [{ type: TAG_TYPES.APP_USER, id: 'LIST' }],
    }),
    updateAppUser: builder.mutation<void, { appId: string; user: AppUser }>({
      query: ({ appId, user: { username, roles } }) => ({
        url: `${url}/${appId}/users/${username}`,
        method: 'PATCH',
        body: { roles: roles.map(({ id }) => id) },
      }),
      invalidatesTags: () => [{ type: TAG_TYPES.APP_USER, id: 'LIST' }],
    }),
    removeAppUser: builder.mutation<void, { appId: string; user: AppUser }>({
      query: ({ appId, user: { username } }) => ({
        url: `${url}/${appId}/users/${username}/roles`,
        method: 'DELETE',
      }),
      invalidatesTags: () => [{ type: TAG_TYPES.APP_USER, id: 'LIST' }],
    }),
  }),
  overrideExisting: false,
});

export const {
  useReadAppUserListQuery,
  useReadAppUserRoleListQuery,
  useCreateAppUserMutation,
  useUpdateAppUserMutation,
  useRemoveAppUserMutation,
} = appUserApi;
