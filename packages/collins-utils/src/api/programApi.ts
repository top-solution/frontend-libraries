import { Program, ProgramSchema } from '../entities/Program';
import { commonDataApi, TAG_TYPES } from './commonDataApi';

const invalidatesTags = () => [{ type: TAG_TYPES.PROGRAM, id: 'LIST' }];

const url = 'v1/programs';

const programApi = commonDataApi.injectEndpoints({
  endpoints: (builder) => ({
    readPrograms: builder.query<{ list: Program[]; map: Record<string, Program> }, void>({
      query: () => url,
      transformResponse: (data) => {
        const list = ProgramSchema.array().parse(data);
        const map: Record<string, Program> = {};
        list.forEach((program) => (map[program.id] = program));
        return { list, map };
      },
      keepUnusedDataFor: 3600,
      providesTags: () => [{ type: TAG_TYPES.PROGRAM, id: 'LIST' }],
    }),
    createProgram: builder.mutation<void, Program>({
      query: (program) => ({ url, method: 'POST', body: program }),
      invalidatesTags,
    }),
    updateProgram: builder.mutation<void, Program>({
      query: (program) => ({ url: `${url}/${program.id}`, method: 'PUT', body: program }),
      invalidatesTags,
    }),
    removeProgram: builder.mutation<void, Program>({
      query: (program) => ({ url: `${url}/${program.id}`, method: 'DELETE' }),
      invalidatesTags,
    }),
  }),
  overrideExisting: false,
});

export const { useReadProgramsQuery, useCreateProgramMutation, useUpdateProgramMutation, useRemoveProgramMutation } =
  programApi;
