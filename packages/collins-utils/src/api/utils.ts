import { PagedRequestFilter } from '../entities/Pagination';

export function formatQueryParams<T>(data: T): URLSearchParams {
  const searchParams = new URLSearchParams();
  for (const [key, value] of Object.entries(data)) {
    if (key === 'filters') {
      if (Array.isArray(value)) {
        value.forEach((item: PagedRequestFilter) => searchParams.append(item.field, `${item.operator}:${item.value}`));
      }
    } else if (Array.isArray(value)) {
      value.forEach((item) => searchParams.append(key, encodeURIComponent(item)));
    } else if (typeof value !== undefined) {
      searchParams.append(key, encodeURIComponent(value));
    }
  }
  return searchParams;
}
