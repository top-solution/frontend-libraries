import { commonDataApi, TAG_TYPES } from './commonDataApi';

const url = 'v1/config';

type AppConfig = Record<string, unknown>;

const appConfigApi = commonDataApi.injectEndpoints({
  endpoints: (builder) => ({
    readAppConfig: builder.query<AppConfig, void>({
      query: () => url,
      keepUnusedDataFor: 3600,
      providesTags: [TAG_TYPES.APP_CONFIG],
    }),
    updateAppConfig: builder.mutation<void, AppConfig>({
      query: (params) => ({ url, method: 'PUT', body: params }),
      invalidatesTags: [TAG_TYPES.APP_CONFIG],
    }),
  }),
  overrideExisting: false,
});

export const { useReadAppConfigQuery, useUpdateAppConfigMutation } = appConfigApi;
