import { createPagedResponseSchema, PagedRequestParams, PagedResponse } from '../entities/Pagination';
import { Person, PersonSchema } from '../entities/Person';
import { commonDataApi, TAG_TYPES } from './commonDataApi';
import { formatQueryParams } from './utils';

const invalidatesTags = (result: unknown, error: unknown, args: Person) => {
  const tags = [
    { type: TAG_TYPES.PERSON, id: 'LIST' },
    { type: TAG_TYPES.PERSON, id: 'SEARCH' },
  ];

  if (args.id) {
    tags.push({ type: TAG_TYPES.PERSON, id: `${args.id}` });
  }

  return tags;
};

const personApi = commonDataApi.injectEndpoints({
  endpoints: (builder) => ({
    searchPeople: builder.query<Person[], { needle: string }>({
      query: (params) => ({ url: `v1/people/search`, params }),
      transformResponse: (data: unknown) => PersonSchema.array().parse(data),
      providesTags: [{ type: TAG_TYPES.PERSON, id: 'SEARCH' }],
    }),
    readPersonList: builder.query<PagedResponse<Person>, PagedRequestParams>({
      query: (params) => ({ url: 'v1/people', params: formatQueryParams(params) }),
      transformResponse: (data: PagedResponse<Person>) => createPagedResponseSchema(PersonSchema).parse(data),
      providesTags: [{ type: TAG_TYPES.PERSON, id: 'LIST' }],
    }),
    removePerson: builder.mutation<void, Person>({
      query: (person) => ({
        method: 'DELETE',
        url: `v1/people/${person.id}`,
      }),
      invalidatesTags,
    }),
    updatePerson: builder.mutation<void, Person>({
      query: ({ id, ...person }) => ({
        method: 'PUT',
        url: `v1/people/${id}`,
        body: person,
      }),
      invalidatesTags,
    }),
    createPerson: builder.mutation<void, Person>({
      query: (person) => ({
        method: 'POST',
        url: `v1/people`,
        body: person,
      }),
      invalidatesTags,
    }),
  }),
  overrideExisting: false,
});

export const {
  useSearchPeopleQuery,
  useLazySearchPeopleQuery,
  useReadPersonListQuery,
  useRemovePersonMutation,
  useUpdatePersonMutation,
  useCreatePersonMutation,
} = personApi;
