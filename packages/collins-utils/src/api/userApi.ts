import { User, UserSchema } from '../entities/User';
import { commonDataApi } from './commonDataApi';

const url = 'v1/users';

const userApi = commonDataApi.injectEndpoints({
  endpoints: (builder) => ({
    readUser: builder.query<User, { username: string }>({
      query: ({ username }) => `${url}/${username}`,
      transformResponse: (data) => UserSchema.parse(data),
      keepUnusedDataFor: 3600,
    }),
    searchUserList: builder.query<User[], { needle: string } | { usernames: string[] }>({
      query: (params) => ({ url: `${url}/search`, params }),
      transformResponse: (data) => UserSchema.array().parse(data),
    }),
  }),
  overrideExisting: false,
});

export const { useReadUserQuery, useSearchUserListQuery, useLazySearchUserListQuery } = userApi;
