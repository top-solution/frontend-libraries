import React, { useContext } from 'react';
import { BaseQueryFn, createApi, FetchArgs, fetchBaseQuery, FetchBaseQueryError } from '@reduxjs/toolkit/query/react';
import { AuthSliceState } from '../store/authSlice';

type RootState = {
  auth: AuthSliceState;
};

export const TAG_TYPES = {
  APP_CONFIG: 'AppConfig',
  PERSON: 'Person',
  APP_USER: 'AppUser',
  PROGRAM: 'Program',
};

let authApiUrl: null | string = null;

export const setAuthApiUrl = (url: string) => (authApiUrl = url);

const dynamicBaseQuery: BaseQueryFn<string | FetchArgs, unknown, FetchBaseQueryError> = async (
  args,
  api,
  extraOptions
) => {
  if (!authApiUrl) {
    throw new Error('authApiUrl not set');
  }

  return fetchBaseQuery({
    baseUrl: authApiUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = (getState() as RootState).auth.token;
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  })(args, api, extraOptions);
};

export const commonDataApi = createApi({
  reducerPath: 'commonDataApi',
  baseQuery: dynamicBaseQuery,
  tagTypes: Object.values(TAG_TYPES),
  endpoints: (builder) => ({
    getToken: builder.query<string, void>({
      query: () => 'token',
    }),
  }),
});

export const { useLazyGetTokenQuery } = commonDataApi;

const CommonDataApiContext = React.createContext({
  authApi: '',
  appId: '',
});

export const CommonDataApiProvider = CommonDataApiContext.Provider;
export const useCommonDataApiParams = () => {
  return useContext(CommonDataApiContext);
};
