import { Country, CountrySchema } from '../entities/Country';
import { commonDataApi } from './commonDataApi';
const url = 'v1/countries';

const countryApi = commonDataApi.injectEndpoints({
  endpoints: (builder) => ({
    readCountries: builder.query<{ list: Country[]; byISO: Record<string, Country> }, void>({
      query: () => url,
      transformResponse: (data) => {
        const list = CountrySchema.array().parse(data);
        const byISO: Record<string, Country> = {};
        list.forEach((country) => (byISO[country.id] = country));
        return { list, byISO };
      },
      keepUnusedDataFor: 3600,
    }),
  }),
  overrideExisting: false,
});

export const { useReadCountriesQuery } = countryApi;
