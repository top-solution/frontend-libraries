import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import decode from 'jwt-decode';

type TokenPayload = {
  roles: string[];
  username: string;
  firstname: string;
  lastname: string;
  appID: string;
  plantID: number | undefined;
  exp: number;
  extra: {
    'common-data-roles': string[];
  };
};

const initialState = {
  token: null as null | string,
  roles: [] as string[],
  commonDataRoles: [] as string[],
  expiration: 0,
  username: null as null | string,
  firstname: null as null | string,
  lastname: null as null | string,
  appId: null as null | string,
  plantId: undefined as undefined | number,
  lastUpdate: null as null | number,
};

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setTokenAction: (state, action: PayloadAction<{ token: string }>) => {
      const { roles, username, firstname, lastname, exp, appID, plantID, extra } = decode<TokenPayload>(
        action.payload.token
      );
      state.token = action.payload.token;
      state.roles = roles || [];
      state.commonDataRoles = extra?.['common-data-roles'] || [];
      state.username = username;
      state.firstname = firstname;
      state.lastname = lastname;
      state.appId = appID;
      state.plantId = plantID;
      state.lastUpdate = Date.now();
      state.expiration = exp * 1000;
    },
  },
});

export const { setTokenAction } = authSlice.actions;

export const authSliceReducer = authSlice.reducer;

export type AuthSliceState = typeof initialState;
