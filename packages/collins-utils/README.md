A collection of utiliets related to collins projects.

# Setup
All components and function are exported separately, and they are meant to be replaced on single projects if necessary.

In order to correctly make requests to `common-data`, make sure pass the `common-data` API URL to `setAuthApiUrl` (see below) as soon as possible, preferably before any component is mounted:

```ts
setAuthApiUrl(`${import.meta.env.VITE_AUTH_API}/api`);
```

Wrap the whole application with `CommonDataApiContext` to use the `AuthGuard` component (see below).

Add the `authSliceReducer` to your redux store.


# Components

## `CommonDataApiContext`
Context necessary to pass the application env varibles to all the components. It is advised to wrap the whole application with this component:

### Props

#### `value`
##### `value?: (role: { authApi: string, appId: string }) => boolean;`
The value must contain the `authApi` the `common-data` API URL, and `appId`, the `common-data` application id.

**Example:**
```tsx
import React from 'react';
import { Route, BrowserRouter, Routes } from 'react-router-dom';
import { CommonDataApiProvider } from '@top-solution/collins-utils';
import { ZodLocalizationProvider } from './hooks/useZodLocalization';

import {
  Home,
  NotFoundPage,
} from './pages';
import RoomEdit from './pages/Rooms/RoomEdit/RoomEdit';

export function Main(): React.ReactElement {
  return (
    <BrowserRouter>
      <ZodLocalizationProvider />
      <CommonDataApiProvider
        value={{ appId: `${import.meta.env.VITE_ID}`, authApi: `${import.meta.env.VITE_AUTH_API}` }}
      >
        <Routes>
          <Route path="/" element={<Home />} />
          <Route element={<NotFoundPage />} />
        </Routes>
      </CommonDataApiProvider>
    </BrowserRouter>
  );
}

```

## `AuthGuard`
Authentication and authorization component.

If the user in not already logged in, `AuthGuard` will automatically perform the authentication by redirecting the user to `common-data`.

You cat authorize the user by checking its roles with `authorizeRole`.

**Example (just authentication):**
```tsx
import React from 'react';
import Box from '@mui/material/Box';
import { AuthGuard, UserRole } from '@top-solution/collins-utils';

function Rooms(): React.ReactElement {
  return (
    <Box>
      <AuthGuard
        authorizeRole={(r) => r === UserRole.ADMIN}
        unauthorizedFallback={<Alert>{'Unauthorized'}</Alert>}
      >
        <Box>
          {'...'}
        </Box>
      </AuthGuard>
    </Box>
  );
}

export default React.memo(Rooms);
```

### Props

#### `authorizeRole`
##### `authorizeRole?: (role: string) => boolean;`
The component will call the passed function once for each user role. The user will be authorized if at least one of these calls returns true. If all calls return false, the user is not authorized and therefore `unauthorizedFallback` is rendered.

#### `unauthorizedPath`
##### `unauthorizedPath?: string`
Optional path to redirect the user in case of failed authorization.
By default its value is `/unauthorized`.


## AuthCallback
`AuthCallback` should handle all redirects from `common-data`. It parses the received JWT token and stores it in the Redux store.

To use it, add a `Route` to path `/auth` and make `<AuthCallback />` handle it.

**Example:**
```tsx
import React from 'react';
import { Route, BrowserRouter, Routes, useNavigate } from 'react-router-dom';
import { CommonDataApiProvider, setAuthApiUrl, AuthCallback } from '@top-solution/collins-utils';

export function Main(): React.ReactElement {
  const navigate = useNavigate();
  return (
    <BrowserRouter>
      <CommonDataApiProvider
        value={{ appId: `${import.meta.env.VITE_ID}`, authApi: `${import.meta.env.VITE_AUTH_API}` }}
      >
        <Routes>
          {'...'}
          <Route path="/auth" element={<AuthCallback onAuthenticationSuccess={(path) => navigate(path)} />} />
        </Routes>
      </CommonDataApiProvider>
    </BrowserRouter>
  );
}
```

# Redux stuff

## authSliceReducer
Redux store slice to manage the user authetication status.

The state branch name **must** be `auth`.

**Example:**
```ts
import { configureStore } from '@reduxjs/toolkit';
import { authSliceReducer } from '@top-solution/collins-utils';

export const store = configureStore({
  reducer: {
    auth: authSliceReducer,
  },
});
```

### Properties
All properties except `token` are `null` if the user is not authenticated.

#### token
##### token: string
The authentication token.

#### roles
##### roles: null | string[]
The logged user array of roles.

#### username
##### username: null | string
The logged user username.

#### lastUpdate
##### lastUpdate: null | number
The date of the last login.

#### expiration
##### expiration: null | number
The expiration date of the token.