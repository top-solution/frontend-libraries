import React, { useEffect } from 'react';
import { installGtag } from 'mr-gtag';

export type TrackingContextConfig = {
  trackingId?: string;
};

export const TrackingContext = React.createContext({} as TrackingContextConfig);

export type TrackingContextProviderProps = {
  children: Array<React.ReactNode> | React.ReactNode;
  config: TrackingContextConfig;
};

export function TrackingContextProvider({ config, children }: TrackingContextProviderProps): JSX.Element {
  useEffect(() => {
    if (config.trackingId) {
      installGtag(config.trackingId, { sendDefaultPageView: false });
    } else {
      // eslint-disable-next-line no-console
      console.warn('Tracking not enabled, as `trackingId` was not configured.');
    }
  }, [config.trackingId]);

  return <TrackingContext.Provider value={config}>{children}</TrackingContext.Provider>;
}
