import React, { useCallback, useContext, useRef } from 'react';
import Box from '@material-ui/core/Box';
import Grow from '@material-ui/core/Grow';
import Paper, { PaperProps } from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import TextField from '@material-ui/core/TextField';
import { BaseDatePickerProps } from '@material-ui/lab/DatePicker/shared';
import StaticDatePicker from '@material-ui/lab/StaticDatePicker';

import { MuiInputsConfigContext } from './ConfigContext';
import TextDatePicker, { TextDatePickerProps } from './TextDatePicker';

export interface DatePickerProps extends Omit<TextDatePickerProps, 'onChange' | 'labels' | 'value'> {
  value: Date | null;
  onChange: (value: Date | null) => void;
  minDate?: Date;
  maxDate?: Date;
  datePickersProps?: Partial<BaseDatePickerProps<Date>>;
  popupPaperProps?: Partial<PaperProps>;
}

function DatePicker(props: DatePickerProps, ref: React.Ref<HTMLInputElement>): JSX.Element {
  const { datePicker: datePickerConfig } = useContext(MuiInputsConfigContext);
  const containerRef = useRef<HTMLDivElement>(null);
  const [anchorEl, setAnchorEl] = React.useState<HTMLDivElement | null>(null);

  const {
    value,
    minDate,
    maxDate,
    onChange,
    // inputFormat,
    helperText,
    style,
    ...textDatePickerProps
  } = props;

  const handleChange = useCallback(
    (date: Date | null) => {
      if (onChange) {
        onChange(date);

        setAnchorEl(null);
      }
    },
    [onChange]
  );

  return (
    <Box ref={ref}>
      <TextDatePicker
        helperText={helperText ?? undefined}
        initialValue={new Date((props.value as string | null) ?? new Date().valueOf())}
        label={props.label || ''}
        sx={{
          minWidth: '18ch',
          maxWidth: '20ch',
          '& label': {
            whiteSpace: 'nowrap',
          },
          ...style,
        }}
        value={value}
        onChange={(e, value) => onChange(value)}
        onFocus={() => {
          if (!props.disabled) {
            setAnchorEl(containerRef.current);
            if (textDatePickerProps.onFocus) {
              textDatePickerProps.onFocus();
            }
          }
        }}
        ref={containerRef}
        // onFocus={(e) => onFocus && onFocus(e as any)}
        {...textDatePickerProps}
      />
      <Box
        sx={{
          position: 'absolute',
          height: 0,
          zIndex: 1301,
          '& .MuiPaper-root': {
            overflow: 'hidden',
            marginTop: 0,
          },
        }}
      >
        <Popper
          open={Boolean(anchorEl)}
          anchorEl={containerRef.current}
          role={undefined}
          placement="bottom-start"
          transition
          disablePortal
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{
                transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
              }}
            >
              <Paper
                sx={{
                  display: 'flex',
                  flexDirection: 'column',
                  width: 320,
                }}
                elevation={4}
                {...props.popupPaperProps}
              >
                <Box
                  sx={{
                    display: 'flex',
                  }}
                >
                  <Box
                    sx={{
                      display: 'flex',
                      flexDirection: 'column',
                      borderBottom: `1px solid`,
                      borderColor: 'divider',
                      '& + &': {
                        borderLeft: `1px solid`,
                        borderColor: 'divider',
                      },
                    }}
                  >
                    <StaticDatePicker
                      displayStaticWrapperAs="desktop"
                      minDate={minDate || datePickerConfig?.minDate}
                      maxDate={maxDate || datePickerConfig?.maxDate}
                      value={value}
                      onChange={handleChange}
                      renderInput={(props) => <TextField {...props} />}
                      // renderDay={renderWeekPickerDay}
                      {...props.datePickersProps}
                    />
                  </Box>
                </Box>
              </Paper>
            </Grow>
          )}
        </Popper>
      </Box>
    </Box>
  );
}

const DatePickerWithRefAndMemo = React.memo(React.forwardRef(DatePicker)) as typeof DatePicker;
export default DatePickerWithRefAndMemo;
