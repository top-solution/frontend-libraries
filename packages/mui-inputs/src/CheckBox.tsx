import React from 'react';
import Checkbox, { CheckboxProps as MUICheckboxProps } from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';

export interface CheckBoxProps extends Omit<MUICheckboxProps, 'onChange'> {
  /**
   * The checkbox label, shown next to the checkbox.
   */
  label: React.ReactNode;
  /**
   * Fired on onCHange. The second argument is the checkbox checked state.
   */
  onChange: (event: React.ChangeEvent<HTMLInputElement>, checked: boolean) => void;
}

function CheckBox(props: CheckBoxProps, ref: React.Ref<HTMLButtonElement>): JSX.Element {
  const { label, onChange, className, style, required, ...otherProps } = props;

  const handleChange = function (event: React.ChangeEvent<HTMLInputElement>): void {
    if (onChange) {
      onChange(event, event.target.checked);
    }
  };

  return (
    <FormControlLabel
      label={
        required ? (
          <>
            {label}
            <span aria-hidden="true" className="MuiFormLabel-asterisk MuiInputLabel-asterisk">
              &nbsp;*
            </span>
          </>
        ) : (
          label
        )
      }
      className={className}
      style={style}
      control={<Checkbox {...otherProps} ref={ref} onChange={handleChange} />}
    />
  );
}

const CheckBoxWithRefAndMemo = React.memo(React.forwardRef(CheckBox)) as typeof CheckBox;
export default CheckBoxWithRefAndMemo;
