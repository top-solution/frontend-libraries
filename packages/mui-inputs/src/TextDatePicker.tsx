import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { getDaysInMonth, isAfter, isBefore, isValid, format as dateFnsFormat } from 'date-fns';
import { isEqual } from 'lodash';
import Box, { BoxProps } from '@material-ui/core/Box';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputBase from '@material-ui/core/InputBase';
import { useTheme } from '@material-ui/core/styles';

const now = new Date();
const today = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0);
const defaultFormat = 'dd/MM/yyyy';

const isBetween = (date: Date, min: Date | undefined, max: Date | undefined): boolean =>
  (min ? isAfter(date, min) : true) && (max ? isBefore(date, max) : true);

export interface TextDatePickerProps extends Omit<BoxProps, 'onChange'> {
  label: string | React.ReactNode;
  value: Date;
  onChange: (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>, value: Date) => void;
  onFocus?: () => void;
  onKeyDown?: (e: React.KeyboardEvent<HTMLInputElement>) => void;
  disabled?: boolean | undefined;
  format?: string | undefined;
  error?: boolean;
  helperText?: React.ReactNode;
  minDate?: Date | undefined;
  maxDate?: Date | undefined;
  minWidth?: number | undefined;
  labels?: Partial<{
    minDateError?: (minDate: string) => string | React.ReactNode;
    maxDateError?: (maxDate: string) => string | React.ReactNode;
  }>;
}

function TextTextDatePicker(props: TextDatePickerProps, ref?: React.Ref<HTMLDivElement>): JSX.Element {
  const { onChange, ...boxProps } = props;
  const [focus, setFocus] = useState(false);
  const theme = useTheme();
  const [value, setValue] = useState({
    date: props.value || today,
    year: (props.value || today).getFullYear(),
    month: (props.value || today).getMonth(),
    day: (props.value || today).getDate(),
    hours: (props.value || today).getHours(),
    minutes: (props.value || today).getMinutes(),
    seconds: (props.value || today).getSeconds(),
    milliseconds: (props.value || today).getMilliseconds(),
    valid: true,
  });

  const format: string = useMemo(() => props.format || defaultFormat, [props.format]);

  const minError = useMemo(() => props.minDate && isBefore(value.date, props.minDate), [props.minDate, value.date]);
  const maxError = useMemo(() => props.maxDate && isAfter(value.date, props.maxDate), [props.maxDate, value.date]);

  const helperText = useMemo(() => {
    if (props.helperText) {
      return props.helperText;
    }

    if (props.minDate && minError) {
      if (props.labels?.minDateError) {
        return props.labels.minDateError(dateFnsFormat(props.minDate, format));
      }
      return `Date must be after ${dateFnsFormat(props.minDate, format)}`;
    }
    if (props.maxDate && maxError) {
      if (props.labels?.maxDateError) {
        return props.labels.maxDateError(dateFnsFormat(props.maxDate, format));
      }
      return `Date must be before ${dateFnsFormat(props.maxDate, format)}`;
    }
  }, [props.helperText, props.labels, props.minDate, props.maxDate, minError, maxError, format]);

  const error = useMemo(() => minError || maxError || props.error, [maxError, minError, props.error]);

  const setValueFromDate = useCallback(
    (date: Date) =>
      setValue((prevValue) => {
        if (isEqual(prevValue.date, date)) {
          return prevValue;
        }

        const newValue = {
          date: date,
          year: date.getFullYear(),
          month: date.getMonth(),
          day: date.getDate(),
          hours: date.getHours(),
          minutes: date.getMinutes(),
          seconds: date.getSeconds(),
          milliseconds: date.getMilliseconds(),
          valid: true,
        };

        return newValue;
      }),
    []
  );

  useEffect(() => {
    if (props.value && isValid(props.value)) {
      setValueFromDate(props.value);
    }
  }, [props.value, setValueFromDate]);

  const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key.match(/^[^0-9]$/)) {
      return e.preventDefault();
    }
    if (props.onKeyDown) {
      props.onKeyDown(e);
    }
  };

  const handleFocus = useCallback(() => {
    setFocus(true);
    if (props.onFocus) {
      props.onFocus();
    }
  }, [props]);

  const patchValue = useCallback(
    (
      patch: {
        year?: number | undefined;
        month?: number | undefined;
        day?: number | undefined;
        hours?: number | undefined;
        minutes?: number | undefined;
        seconds?: number | undefined;
        milliseconds?: number | undefined;
      },
      e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
    ) => {
      const date = new Date(value.date);

      if (typeof patch.year !== 'undefined') {
        date.setFullYear(patch.year);
      }
      if (typeof patch.month !== 'undefined') {
        date.setMonth(patch.month);
      }
      if (typeof patch.day !== 'undefined') {
        date.setDate(patch.day);
      }
      if (typeof patch.hours !== 'undefined') {
        date.setHours(patch.hours);
      }
      if (typeof patch.minutes !== 'undefined') {
        date.setMinutes(patch.minutes);
      }
      if (typeof patch.seconds !== 'undefined') {
        date.setSeconds(patch.seconds);
      }
      if (typeof patch.milliseconds !== 'undefined') {
        date.setMilliseconds(patch.milliseconds);
      }

      const valid = isValid(date) && isBetween(date, props.minDate, props.maxDate);

      const newvalue = {
        date: valid ? date : value.date,
        valid,
        year: typeof patch.year === 'undefined' ? value.year : patch.year,
        month: typeof patch.month === 'undefined' ? value.month : patch.month,
        day: typeof patch.day === 'undefined' ? value.day : patch.day,
        hours: typeof patch.hours === 'undefined' ? value.hours : patch.hours,
        minutes: typeof patch.minutes === 'undefined' ? value.minutes : patch.minutes,
        seconds: typeof patch.seconds === 'undefined' ? value.seconds : patch.seconds,
        milliseconds: typeof patch.milliseconds === 'undefined' ? value.milliseconds : patch.milliseconds,
      };

      if (newvalue.valid && onChange) {
        onChange(e, newvalue.date);
      }

      setValue(newvalue);
    },
    [
      onChange,
      props.maxDate,
      props.minDate,
      value.date,
      value.day,
      value.hours,
      value.milliseconds,
      value.minutes,
      value.month,
      value.seconds,
      value.year,
    ]
  );

  const forceValidDate = useCallback(() => {
    setValue((prevValue) => {
      // eslint-disable-next-line prefer-const
      let { year, month, day, hours, minutes, seconds, milliseconds } = prevValue;

      let date = new Date(year, month, day, hours, minutes, seconds, milliseconds);

      if (month < 0) {
        month = 0;
      } else if (month > 11) {
        month = 11;
      }

      date = new Date(year, month, day, hours, minutes, seconds, milliseconds);

      if (day < 1) {
        day = 1;
      } else if (day > getDaysInMonth(date)) {
        day = getDaysInMonth(date);
      }

      date = new Date(year, month, day, hours, minutes, seconds, milliseconds);

      return {
        date,
        valid: isBetween(date, props.minDate, props.maxDate),
        year: date.getFullYear(),
        month: date.getMonth(),
        day: date.getDate(),
        hours: date.getHours(),
        minutes: date.getMinutes(),
        seconds: date.getSeconds(),
        milliseconds: date.getMilliseconds(),
      };
    });
  }, [props.maxDate, props.minDate]);

  const handleBlur = useCallback(() => {
    setFocus(false);
    forceValidDate();
  }, [forceValidDate]);

  const yearInputElement = (
    <InputBase
      key="yearInputElement"
      sx={{
        width: '4ch',
        textAlign: 'right',
      }}
      inputProps={{ style: { width: '4ch', minWidth: '4ch', textAlign: 'right' } }}
      value={value.year}
      onChange={(e) => patchValue({ year: Number(e.target.value) }, e)}
      onKeyDown={handleKeyDown}
      onFocus={handleFocus}
      onBlur={handleBlur}
      onClick={(e) => (e.target as HTMLInputElement).select()}
      disabled={props.disabled}
    />
  );

  const monthInputElement = (
    <InputBase
      key="monthInputElement"
      sx={{
        width: '2ch',
        textAlign: 'right',
      }}
      inputProps={{ style: { width: '2ch', minWidth: '2ch', textAlign: 'right' } }}
      value={value.month < 9 ? `0${value.month + 1}` : value.month + 1}
      onChange={(e) => patchValue({ month: Number(e.target.value) - 1 }, e)}
      onKeyDown={handleKeyDown}
      onFocus={handleFocus}
      onBlur={handleBlur}
      onClick={(e) => (e.target as HTMLInputElement).select()}
      disabled={props.disabled}
    />
  );

  const dayInputElement = (
    <InputBase
      key="dayInputElement"
      sx={{
        width: '2ch',
        textAlign: 'right',
      }}
      inputProps={{ style: { width: '2ch', minWidth: '2ch', textAlign: 'right' } }}
      value={value.day < 10 ? `0${value.day}` : value.day}
      onChange={(e) => patchValue({ day: Number(e.target.value) }, e)}
      onKeyDown={handleKeyDown}
      onFocus={handleFocus}
      onBlur={handleBlur}
      onClick={(e) => (e.target as HTMLInputElement).select()}
      disabled={props.disabled}
    />
  );

  const hoursInputElement = (
    <InputBase
      key="hoursInputElement"
      sx={{
        width: '2ch',
        textAlign: 'right',
      }}
      inputProps={{ style: { width: '2ch', minWidth: '2ch', textAlign: 'right' } }}
      value={value.hours < 10 ? `0${value.hours}` : value.hours}
      onChange={(e) => patchValue({ hours: Number(e.target.value) }, e)}
      onKeyDown={handleKeyDown}
      onFocus={handleFocus}
      onBlur={handleBlur}
      onClick={(e) => (e.target as HTMLInputElement).select()}
      disabled={props.disabled}
    />
  );

  const minutesInputElement = (
    <InputBase
      key="minutesInputElement"
      sx={{
        width: '2ch',
        textAlign: 'right',
      }}
      inputProps={{ style: { width: '2ch', minWidth: '2ch', textAlign: 'right' } }}
      value={value.minutes < 10 ? `0${value.minutes}` : value.minutes}
      onChange={(e) => patchValue({ minutes: Number(e.target.value) }, e)}
      onKeyDown={handleKeyDown}
      onFocus={handleFocus}
      onBlur={handleBlur}
      onClick={(e) => (e.target as HTMLInputElement).select()}
      disabled={props.disabled}
    />
  );

  const secondsInputElement = (
    <InputBase
      key="secondsInputElement"
      sx={{
        width: '2ch',
        textAlign: 'right',
      }}
      inputProps={{ style: { width: '2ch', minWidth: '2ch', textAlign: 'right' } }}
      value={value.seconds < 10 ? `0${value.seconds}` : value.seconds}
      onChange={(e) => patchValue({ seconds: Number(e.target.value) }, e)}
      onKeyDown={handleKeyDown}
      onFocus={handleFocus}
      onBlur={handleBlur}
      onClick={(e) => (e.target as HTMLInputElement).select()}
      disabled={props.disabled}
    />
  );

  const millisecondsInputElement = (
    <InputBase
      key="millisecondsInputElement"
      sx={{
        width: '3ch',
        textAlign: 'right',
      }}
      inputProps={{ style: { width: '3ch', minWidth: '3ch', textAlign: 'right' } }}
      value={
        value.milliseconds < 100
          ? `00${value.milliseconds}`
          : value.milliseconds < 10
          ? `0${value.milliseconds}`
          : value.milliseconds
      }
      onChange={(e) => patchValue({ milliseconds: Number(e.target.value) }, e)}
      onKeyDown={handleKeyDown}
      onFocus={handleFocus}
      onBlur={handleBlur}
      onClick={(e) => (e.target as HTMLInputElement).select()}
      disabled={props.disabled}
    />
  );

  const formatTokens = useMemo(() => {
    const formatTokenizerRegExp = /(SSS|ss|mm|HH|dd|MM|yyyy|.+?)/g;
    let match = formatTokenizerRegExp.exec(format);
    const tokens = [];
    do {
      if (match) {
        tokens.push(match[0]);
      }
    } while ((match = formatTokenizerRegExp.exec(format)) !== null);
    return tokens;
  }, [format]);

  const elements = [];

  for (let i = 0; i < formatTokens.length; i++) {
    const formatToken = formatTokens[i];
    if (formatToken === 'yyyy') {
      elements.push(yearInputElement);
    } else if (formatToken === 'MM') {
      elements.push(monthInputElement);
    } else if (formatToken === 'dd') {
      elements.push(dayInputElement);
    } else if (formatToken === 'HH') {
      elements.push(hoursInputElement);
    } else if (formatToken === 'mm') {
      elements.push(minutesInputElement);
    } else if (formatToken === 'ss') {
      elements.push(secondsInputElement);
    } else if (formatToken === 'SSS') {
      elements.push(millisecondsInputElement);
    } else {
      elements.push(
        <Box
          key={i}
          sx={{
            typography: 'body1',
            paddingX: 0.5,
            paddingY: 0,
          }}
        >
          {formatToken}
        </Box>
      );
    }
  }

  return (
    <Box
      className="TslTextTextDatePicker-root"
      ref={ref}
      {...boxProps}
      sx={{
        ...props.sx,
        display: 'flex',
        flexDirection: 'column',
        typography: 'body1',
        '& .MuiInputBase-root': {
          height: 23,
        },
        '& .MuiInputBase-input': {
          fontFamily: 'monospace',
          typography: 'body1',
        },
        ...boxProps.sx,
      }}
    >
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          padding: 1.5,
          borderRadius: 1,
          position: 'relative',
          fontFamily: 'inherit',
          color: props.disabled ? theme.palette.text.disabled : undefined,
          pointerEvents: props.disabled ? 'none' : undefined,
          minWidth: props.minWidth,
          paddingX: 1.75,
          paddingY: 2.0625,
          '&:hover fieldset': {
            borderColor: 'rgba(0, 0, 0, 0.87)',
          },
        }}
      >
        {elements}
        <Box
          component="fieldset"
          sx={{
            textAlign: 'left',
            position: 'absolute',
            bottom: 0,
            right: 0,
            top: -5,
            left: 0,
            margin: 0,
            paddingX: 1,
            paddingY: 0,
            pointerEvents: 'none',
            borderRadius: 'inherit',
            borderStyle: 'solid',
            borderWidth: 1,
            overflow: 'hidden',
            minWidth: '0%',
            color: 'rgba(0, 0, 0, 0.6)',
            borderColor: 'rgba(0, 0, 0, 0.23)',
            fontFamily: 'inherit',
            '& legend': {
              display: 'block',
              width: 'auto',
              padding: '0 4px',
              height: 11,
              fontSize: '0.75em',
              visibility: 'visible',
              color: 'inherit',
              maxWidth: '100%',
              transition: 'color 200ms cubic-bezier(0.0, 0, 0.2, 1)',
              fontWeight: 400,
              lineHeight: '10px',
              letterSpacing: '0.00938em',
            },
            ...(focus
              ? {
                  color: theme.palette.primary.main,
                  borderWidth: 2,
                  borderColor: error ? theme.palette.error.main : theme.palette.primary.main,
                }
              : {
                  borderColor: error ? theme.palette.error.main : undefined,
                }),
          }}
          disabled={props.disabled}
        >
          <legend
            style={
              focus
                ? {
                    paddingLeft: 3,
                    fontFamily: 'inherit',
                    color: error ? theme.palette.error.main : undefined,
                  }
                : {
                    color: error ? theme.palette.error.main : undefined,
                  }
            }
          >
            {props.label}
          </legend>
        </Box>
      </Box>
      {helperText ? (
        <FormHelperText style={{ color: error ? theme.palette.error.main : undefined }}>{helperText}</FormHelperText>
      ) : null}
    </Box>
  );
}

export default React.memo(React.forwardRef(TextTextDatePicker)) as typeof TextTextDatePicker;
