import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import MUITextField, { StandardTextFieldProps } from '@material-ui/core/TextField';
import ClearIcon from 'mdi-material-ui/Close';

export interface TextFieldProps extends Omit<StandardTextFieldProps, 'onChange' | 'onBlur'> {
  value?: string;
  onChange?: (event: React.ChangeEvent<HTMLInputElement> | React.MouseEvent<HTMLButtonElement>, value: string) => void;
  onBlur?: (
    event: React.FocusEvent<HTMLInputElement | HTMLTextAreaElement> | React.MouseEvent<HTMLButtonElement>
  ) => void;
  /**
   * If true, hides the clear field button.
   */
  disableClearable?: boolean;
}

function TextField(props: TextFieldProps, ref: React.Ref<HTMLDivElement>): JSX.Element {
  const { value, onChange, InputProps, disabled, disableClearable, helperText, ...otherProps } = props;

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (onChange) {
      onChange(event, event.target.value);
    }
  };

  const handleClear =
    disableClearable || disabled
      ? undefined
      : (event: React.MouseEvent<HTMLButtonElement>) => {
          if (props.onBlur) {
            props.onBlur(event);
          }
          if (onChange) {
            onChange(event, '');
          }
        };

  const endAdornment =
    InputProps?.endAdornment ||
    (handleClear && value && (
      <InputAdornment position="end">
        <IconButton size="small" onClick={handleClear}>
          <ClearIcon fontSize="inherit" />
        </IconButton>
      </InputAdornment>
    ));

  return (
    <MUITextField
      {...otherProps}
      ref={ref}
      value={value}
      disabled={disabled}
      onChange={handleChange}
      helperText={helperText || ' '}
      InputProps={{ ...InputProps, endAdornment }}
    />
  );
}

const TextFieldWithRefAndMemo = React.memo(React.forwardRef(TextField)) as typeof TextField;
export default TextFieldWithRefAndMemo;
