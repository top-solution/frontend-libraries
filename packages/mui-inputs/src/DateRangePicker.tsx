import React, { useCallback, useContext, useEffect, useRef, useState } from 'react';
import clsx from 'clsx';
import { add, endOfDay, format, isBefore, isSameDay, isWithinInterval, max as dateFnsMax, startOfDay } from 'date-fns';
import Box, { BoxProps } from '@material-ui/core/Box';
import Button, { ButtonProps } from '@material-ui/core/Button';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper, { PaperProps } from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import TextField, { TextFieldProps } from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { BaseDatePickerProps } from '@material-ui/lab/DatePicker/shared';
import PickersDay, { PickersDayProps } from '@material-ui/lab/PickersDay';
import StaticDatePicker from '@material-ui/lab/StaticDatePicker';
import { SxProps } from '@material-ui/system/';
import { MuiInputsConfigContext } from './ConfigContext';
import TextDatePicker from './TextDatePicker';

const defaultDateFormat = 'dd/MM/yyyy';

const max = (dates: Array<Date | undefined | null>) =>
  dateFnsMax(dates.filter((date) => date !== null && typeof date !== 'undefined') as Date[]);

export type DateRange = [Date | null, Date | null];

type PartialDatePickerProps = Partial<BaseDatePickerProps<Date>>;

export interface DateRangePickerProps extends Omit<BoxProps, 'onChange' | 'onBlur'> {
  value: DateRange;
  onChange?: (value: DateRange) => void;
  onBlur?: (
    event: React.FocusEvent<HTMLDivElement> | React.MouseEvent<HTMLButtonElement> | MouseEvent | TouchEvent
  ) => void;
  minDate?: Date;
  maxDate?: Date;
  inputFormat?: string;
  inputsWidth?: number;
  disabled?: boolean;
  labels?: Partial<{
    start?: string | React.ReactNode;
    middle?: string | React.ReactNode;
    end?: string | React.ReactNode;
    cancel?: string | React.ReactNode;
    apply?: string | React.ReactNode;
    minDateError?: (date: string) => string | React.ReactNode;
    maxDateError?: (date: string) => string | React.ReactNode;
  }>;
  startError?: boolean;
  endError?: boolean;
  startHelperText?: React.ReactNode;
  endHelperText?: React.ReactNode;
  datePickersProps?: PartialDatePickerProps;
  startDatePickerProps?: PartialDatePickerProps;
  endDatePickerProps?: PartialDatePickerProps;
  startInputProps?: Partial<TextFieldProps>;
  endInputProps?: Partial<TextFieldProps>;
  startPopupDatePickerProps?: PartialDatePickerProps;
  endoPopupDatePickerProps?: PartialDatePickerProps;
  actionButtonsProps?: Partial<ButtonProps>;
  cancelButtonProps?: Partial<ButtonProps>;
  applyButtonProps?: Partial<ButtonProps>;
  popupPaperProps?: Partial<PaperProps>;
}

const defaultLabels = {
  start: '',
  middle: 'to',
  end: '',
  cancel: 'Cancel',
  apply: 'Apply',
};

/**
 * Squash all context configuration in a single typed object
 */
function useDatePickerContext() {
  const { datePicker, dateRangePicker } = useContext(MuiInputsConfigContext);
  return {
    ...(datePicker as DateRangePickerProps),
    ...(dateRangePicker as DateRangePickerProps),
  };
}

function DateRangePicker(props: DateRangePickerProps, ref: React.Ref<HTMLDivElement>): JSX.Element {
  const containerRef = useRef(null);
  const datePickerConfig = useDatePickerContext();

  // Get values of some variables from (in that order) defaults, config context and component props
  const minDate = props.minDate || datePickerConfig.minDate;
  const maxDate = props.maxDate || datePickerConfig.maxDate;
  const inputFormat = props.inputFormat || datePickerConfig.inputFormat || defaultDateFormat;
  const labels = {
    ...defaultLabels,
    ...datePickerConfig?.labels,
    ...props.labels,
  };

  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);

  const [value, setValue] = useState<{
    start: Date | null;
    end: Date | null;
  }>({
    start: props.value[0] || null,
    end: props.value[1] || null,
  });

  useEffect(() => {
    const value = props.value || [null, null];

    setValue({
      start: value[0] || null,
      end: value[1] || null,
    });
  }, [props.value]);

  const handleCancel = useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) => {
      setAnchorEl(null);
      if (props.onBlur) {
        props.onBlur(event);
      }
    },
    [props]
  );

  const renderWeekPickerDay = useCallback(
    (day: Date, selectedDates: (Date | null)[], DayComponentProps: PickersDayProps<Date>) => {
      const { start, end } = value;
      const dayIsBetween =
        start && end && isBefore(start, end)
          ? isWithinInterval(day, { start: startOfDay(start), end: endOfDay(end) })
          : false;

      const isFirstDay = start ? isSameDay(day, start) : false;
      const isLastDay = end ? isSameDay(day, end) : false;

      let sx: SxProps = {};

      if (dayIsBetween) {
        sx = {
          '.MuiPickersDay-root': {
            borderTopRightRadius: 0,
            borderBottomRightRadius: 0,
            borderTopLeftRadius: 0,
            borderBottomLeftRadius: 0,
            backgroundColor: 'primary.light',
            color: 'primary.contrastText',
            '&:hover': {
              backgroundColor: 'primary.main',
            },
          },
        };
      }
      if (isFirstDay) {
        sx = {
          '.MuiPickersDay-root': {
            borderTopRightRadius: 0,
            borderBottomRightRadius: 0,
            backgroundColor: 'primary.main',
            color: 'primary.contrastText',
            '&:hover': {
              backgroundColor: 'primary.dark',
            },
          },
        };
      } else if (isLastDay) {
        sx = {
          '.MuiPickersDay-root': {
            borderTopLeftRadius: '0',
            borderBottomLeftRadius: '0',
            backgroundColor: 'primary.main',
            color: 'primary.contrastText',
            '&:hover': {
              backgroundColor: 'primary.dark',
            },
          },
        };
      }

      return (
        <Box key={day.valueOf()} sx={sx}>
          <PickersDay {...DayComponentProps} disableMargin />
        </Box>
      );
    },
    [value]
  );

  const handleStartChange = useCallback((newValue: Date | null) => {
    if (newValue) {
      setValue(() => {
        return {
          start: newValue,
          end: add(newValue, { days: 1 }),
          // end: !prevValue.end || isAfter(newValue, prevValue.end) ? newValue : prevValue.end,
        };
      });
    }
  }, []);

  const handleEndChange = useCallback((newValue: Date | null) => {
    if (newValue) {
      setValue((prevValue) => ({
        ...prevValue,
        end: newValue,
      }));
    }
  }, []);

  const handleInputChange = useCallback(
    (e: React.KeyboardEvent<HTMLInputElement>) => {
      if (e.key === 'Enter') {
        setAnchorEl(null);

        if (props.onChange && value.start && value.end) {
          props.onChange([value.start, value.end]);
        }
      }
    },
    [props, value.end, value.start]
  );

  return (
    <ClickAwayListener
      onClickAway={(event) => {
        setAnchorEl(null);
        if (props.onBlur) {
          props.onBlur(event);
        }
      }}
    >
      <Box
        sx={{
          position: 'relative',
          '& .MuiPickersCalendar-root': {
            minHeight: 232,
          },
          ...props.sx,
        }}
        ref={containerRef}
      >
        <Box
          sx={{
            display: 'flex',
            alignItems: 'baseline',
          }}
          ref={ref}
        >
          <TextDatePicker
            value={value.start || new Date()}
            onChange={(e, value) => handleStartChange(value)}
            label={props.startInputProps?.label || labels.start || ''}
            onFocus={() => {
              if (!props.disabled) {
                setAnchorEl(containerRef.current);
              }
            }}
            onKeyDown={handleInputChange}
            disabled={props.disabled}
            format={inputFormat}
            error={props.startError || props.startInputProps?.error}
            helperText={props.startHelperText || ''}
            minDate={minDate}
            maxDate={maxDate}
            minWidth={props.inputsWidth}
            labels={{
              minDateError: labels?.minDateError,
              maxDateError: labels?.maxDateError,
            }}
          />
          {typeof labels.middle === 'string' ? (
            <Box sx={{ marginX: 2, color: props.disabled ? 'text.disabled' : undefined }}>
              <Typography variant="body1">{labels.middle}</Typography>
            </Box>
          ) : (
            labels.middle
          )}
          <TextDatePicker
            value={value.end || new Date()}
            onChange={(e, value) => handleEndChange(value)}
            label={props.endInputProps?.label || labels.end || ''}
            onFocus={() => {
              if (!props.disabled) {
                setAnchorEl(containerRef.current);
              }
            }}
            onKeyDown={handleInputChange}
            disabled={props.disabled}
            format={inputFormat}
            error={props.endError || props.endInputProps?.error}
            helperText={props.endHelperText || ''}
            minDate={minDate}
            maxDate={maxDate}
            minWidth={props.inputsWidth}
            labels={{
              minDateError: labels?.minDateError,
              maxDateError: labels?.maxDateError,
            }}
          />
        </Box>
        <Box
          sx={{
            position: 'absolute',
            height: 0,
            zIndex: 1301,
            '& .MuiPaper-root': {
              overflow: 'hidden',
              marginTop: 0,
            },
          }}
        >
          <Popper
            open={Boolean(anchorEl)}
            anchorEl={containerRef.current}
            role={undefined}
            placement="bottom-start"
            transition
            disablePortal
          >
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                style={{
                  transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
                }}
              >
                <Paper
                  sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    width: 640,
                  }}
                  elevation={4}
                  {...props.popupPaperProps}
                >
                  <Box
                    sx={{
                      display: 'flex',
                    }}
                  >
                    <Box
                      sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        borderBottom: `1px solid`,
                        borderColor: 'divider',
                        '& + &': {
                          borderLeft: `1px solid`,
                          borderColor: 'divider',
                        },
                      }}
                    >
                      <Box sx={{ paddingTop: 2, paddingX: 3 }}>
                        <Typography variant="body1">{`${labels?.start}:`}</Typography>
                      </Box>
                      <StaticDatePicker
                        displayStaticWrapperAs="desktop"
                        minDate={minDate}
                        maxDate={maxDate}
                        value={value.start}
                        onChange={handleStartChange}
                        renderInput={(props) => <TextField {...props} />}
                        renderDay={renderWeekPickerDay}
                        {...props.datePickersProps}
                        {...props.startPopupDatePickerProps}
                      />
                    </Box>
                    <Box
                      sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        borderBottom: `1px solid`,
                        borderColor: 'divider',
                        '& + &': {
                          borderLeft: `1px solid`,
                          borderColor: 'divider',
                        },
                      }}
                    >
                      <Box sx={{ paddingTop: 2, paddingX: 3 }}>
                        <Typography variant="body1">{`${labels?.end}:`}</Typography>
                      </Box>
                      <StaticDatePicker
                        displayStaticWrapperAs="desktop"
                        minDate={max([value.start, minDate].filter((d) => d))}
                        maxDate={maxDate}
                        value={value.end}
                        onChange={handleEndChange}
                        renderInput={(props) => <TextField {...props} />}
                        renderDay={renderWeekPickerDay}
                        {...props.datePickersProps}
                        {...props.endoPopupDatePickerProps}
                      />
                    </Box>
                  </Box>
                  <Box
                    sx={{
                      display: 'flex',
                      alignItems: 'center',
                      paddingX: 2,
                      paddingY: 1.5,
                    }}
                  >
                    <Box
                      sx={{
                        flex: '1 1 auto',
                      }}
                    >
                      <Typography variant="body2">
                        {value.start &&
                          value.end &&
                          `${format(value.start, inputFormat)} - ${format(value.end, inputFormat)}`}
                      </Typography>
                    </Box>
                    <Box
                      sx={{ marginLeft: 1 }}
                      className={clsx(props.actionButtonsProps?.className, props.cancelButtonProps?.className)}
                    >
                      <Button
                        size="small"
                        {...props.actionButtonsProps}
                        {...props.cancelButtonProps}
                        onClick={handleCancel}
                      >
                        {labels.cancel}
                      </Button>
                    </Box>
                    <Box
                      sx={{ marginLeft: 1 }}
                      className={clsx(props.actionButtonsProps?.className, props.applyButtonProps?.className)}
                    >
                      <Button
                        variant="contained"
                        size="small"
                        {...props.actionButtonsProps}
                        {...props.applyButtonProps}
                        onClick={(event: React.MouseEvent<HTMLButtonElement>) => {
                          event.preventDefault();
                          event.stopPropagation();

                          if (props.onChange) {
                            if (value.start && value.end) {
                              props.onChange([value.start, value.end]);
                            }
                            setAnchorEl(null);
                            if (props.onBlur) {
                              props.onBlur(event);
                            }
                          }
                        }}
                      >
                        {labels.apply}
                      </Button>
                    </Box>
                  </Box>
                </Paper>
              </Grow>
            )}
          </Popper>
        </Box>
      </Box>
    </ClickAwayListener>
  );
}

const DateRangePickerWithRefAndMemo = React.memo(React.forwardRef(DateRangePicker)) as typeof DateRangePicker;
export default DateRangePickerWithRefAndMemo;
