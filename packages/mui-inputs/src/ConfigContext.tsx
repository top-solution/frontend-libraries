import React from 'react';
import { DateRangePickerProps } from './DateRangePicker';
import { TransferListCustomizationProps } from './TransferList';

export interface MuiInputsConfig {
  datePicker?: {
    inputFormat?: string;
    minDate?: Date;
    maxDate?: Date;
  };
  dateRangePicker?: {
    labels?: DateRangePickerProps['labels'];
    inputsWidth?: DateRangePickerProps['inputsWidth'];
  };
  transferList?: Partial<TransferListCustomizationProps>;
}

const defaultConfig = {} as MuiInputsConfig;

export const MuiInputsConfigContext = React.createContext(defaultConfig);

export interface MuiInputsConfigProviderProps {
  children: Array<React.ReactNode> | React.ReactNode;
  config: MuiInputsConfig;
}

export function MuiInputsConfigProvider({ config, children }: MuiInputsConfigProviderProps): JSX.Element {
  return <MuiInputsConfigContext.Provider value={config}>{children}</MuiInputsConfigContext.Provider>;
}
