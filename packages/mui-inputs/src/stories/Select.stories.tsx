/* eslint-disable react/display-name */
import React, { useState, useEffect } from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import { RequestState } from '@top-solution/utils';
import ProgressButton from '../ProgressButton';
import Select, { SelectProps } from '../Select';
import useFakeRequest from './useFakeRequest';

export default {
  title: 'MUI Inputs/Select',
  component: Select,
  argTypes: {
    onChange: { action: 'onChange' },
    onBlur: { action: 'onBlur' },
    reqError: { control: null },
    name: { control: { disable: true } },
  },
} as Meta;

const FIELD_NAME = 'field_name';

type StoryOption = { id: string; color: string };

const options = ['Red', 'Green', 'Blue', 'Yellow', 'Purple', 'Grey'].map((c) => ({
  id: c.toLowerCase(),
  color: c,
}));

const request: RequestState = {
  inProgress: false,
  lastUpdate: null,
  error: null,
};

const Template: Story<SelectProps<typeof FIELD_NAME, StoryOption> & { reqError: boolean }> = (props) => {
  const { reqError, ...componentProps } = props;
  const [value, setValue] = useState<null | StoryOption>(props.value);
  const { requestState, data, start, reset } = useFakeRequest(options);

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', width: '300px', justifyContent: 'stretch' }}>
      <Box>
        <Select
          {...componentProps}
          request={requestState}
          options={data || []}
          onChange={(...args) => {
            setValue(args[0]);
          }}
          value={value}
        />
      </Box>
      <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <ProgressButton variant="outlined" onClick={() => start(reqError)} inProgress={requestState.inProgress}>
          {'Start request'}
        </ProgressButton>
        <Button
          variant="outlined"
          onClick={() => {
            setValue(null);
            reset();
          }}
        >
          {'Reset'}
        </Button>
      </Box>
      <pre>
        <code>{`Request state: ${JSON.stringify(requestState, null, 2)}`}</code>
      </pre>
      <pre>
        <code>{`Data: ${JSON.stringify(data, null, 2)}`}</code>
      </pre>
    </Box>
  );
};

export const Basic = Template.bind({});
Basic.args = {
  name: FIELD_NAME,
  label: 'Text field label',
  options,
  request,
  renderOption: (option) => option.color,
};

export const CustomOptions = Template.bind({});
CustomOptions.args = {
  ...Basic.args,
  renderOption: (option) => (
    <Box sx={{ display: 'flex', alignItems: 'center' }}>
      <Box sx={{ width: 18, height: 18, marginRight: 1 }} style={{ backgroundColor: option.color }} />
      {option.color}
    </Box>
  ),
};

export const WithReqError = Template.bind({});
WithReqError.storyName = 'Request error';
WithReqError.args = {
  ...Basic.args,
  reqError: true,
};

export const DisableClearable = Template.bind({});
DisableClearable.args = {
  name: FIELD_NAME,
  label: 'Text field label',
  disableClearable: true,
  options,
  request,
  renderOption: (option) => option.color,
};

export const DisabledWithoutOnChange = Template.bind({});
DisabledWithoutOnChange.storyName = 'Disabled without onChange';
DisabledWithoutOnChange.args = {
  ...Basic.args,
  onChange: undefined,
  disabled: true,
};

const VariantsTemplate: Story<SelectProps<typeof FIELD_NAME, StoryOption> & { reqError: boolean }> = (props) => {
  const { reqError, ...componentProps } = props;
  const [value, setValue] = useState<null | StoryOption>(props.value);
  const { requestState, data, start, reset } = useFakeRequest(options);

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', width: '300px', justifyContent: 'stretch' }}>
      <Box>
        <Select
          {...componentProps}
          request={requestState}
          options={data || []}
          onChange={(...args) => {
            setValue(args[0]);
          }}
          value={value}
          label="Outlined"
          variant="outlined"
        />
        <Select
          {...componentProps}
          request={requestState}
          options={data || []}
          onChange={(...args) => {
            setValue(args[0]);
          }}
          value={value}
          label="Filled"
          variant="filled"
        />
        <Select
          {...componentProps}
          request={requestState}
          options={data || []}
          onChange={(...args) => {
            setValue(args[0]);
          }}
          value={value}
          label="Standard"
          variant="standard"
        />
      </Box>
      <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <ProgressButton variant="outlined" onClick={() => start(reqError)} inProgress={requestState.inProgress}>
          {'Start request'}
        </ProgressButton>
        <Button
          variant="outlined"
          onClick={() => {
            setValue(null);
            reset();
          }}
        >
          {'Reset'}
        </Button>
      </Box>
      <pre>
        <code>{`Request state: ${JSON.stringify(requestState, null, 2)}`}</code>
      </pre>
      <pre>
        <code>{`Data: ${JSON.stringify(data, null, 2)}`}</code>
      </pre>
    </Box>
  );
};

export const Variants = VariantsTemplate.bind({});
Variants.storyName = 'Variants';
Variants.args = {
  ...Basic.args,
};
