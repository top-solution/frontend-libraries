import React, { useState, useEffect } from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import { Story, Meta } from '@storybook/react/types-6-0';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import { green, red } from '@material-ui/core/colors';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ChevronLeft from 'mdi-material-ui/ChevronLeft';
import ChevronRight from 'mdi-material-ui/ChevronRight';
import { ProgressButton } from '..';
import { MuiInputsConfig, MuiInputsConfigProvider } from '../ConfigContext';
import TransferList, { TransferListCustomizationProps, TransferListLabels, TransferListProps } from '../TransferList';
import cities from './cities.json';
import useFakeRequest from './useFakeRequest';

export default {
  title: 'MUI Inputs/TransferList',
  component: TransferList,
  argTypes: {
    onChange: { action: 'onChange' },
    onBlur: { action: 'onBlur' },
    name: { control: { disable: true } },
    searchOnlyAvailable: { control: 'boolean' },
  },
  parameters: {
    viewport: {
      viewports: INITIAL_VIEWPORTS,
    },
  },
} as Meta;

type StoryOption = { id: string; name: string };

const Template: Story<TransferListProps<StoryOption>> = (props) => {
  const [value, setValue] = useState<StoryOption[]>(props.value || []);

  useEffect(() => {
    if (props.value) {
      setValue(props.value);
    }
  }, [props.value]);

  const renderOption = props.renderOption || ((item) => item.name);

  return (
    <div>
      <button onClick={() => setValue([])}>{'Reset value'}</button>
      <TransferList
        {...props}
        renderOption={renderOption}
        filterOptions={(search) => (item) => item.name.toLowerCase().indexOf(search.toLowerCase()) >= 0}
        value={value || []}
        onChange={(...args) => {
          setValue(args[0]);
          if (props.onChange) {
            props.onChange(...args);
          }
        }}
      />
    </div>
  );
};

export const Basic = Template.bind({});
Basic.args = {
  options: cities.slice(0, 10),
  sortOptions: (list) => list.sort((a: StoryOption, b: StoryOption) => a.name.localeCompare(b.name)),
};

export const LongList = Template.bind({});
LongList.args = {
  ...Basic.args,
  options: cities.slice(0, 5000) as StoryOption[],
  maxHeight: 400,
};

const cats = {
  felix: { id: 'felix', name: 'Felix' },
  marshmallow: { id: 'marshmallow', name: 'Marshmallow' },
  miss_awesomewumps: { id: 'miss_awesomewumps', name: 'Miss Awesomewumps' },
  mittens: { id: 'mittens', name: 'Mittens' },
  mrs_clearclaws: { id: 'mrs_clearclaws', name: 'Mrs Clearclaws' },
  mrs_smartfoot: { id: 'mrs_smartfoot', name: 'Mrs Smartfoot' },
  oreo: { id: 'oreo', name: 'Oreo' },
  pebbles: { id: 'pebbles', name: 'Pebbles' },
  puffins: { id: 'puffins', name: 'Puffins' },
  tigger: { id: 'tigger', name: 'Tigger' },
};

export const ExclusionRules = Template.bind({});
ExclusionRules.args = {
  ...Basic.args,
  options: Object.values(cats),
  rules: {
    exclusion: (item) => {
      if (item === cats['pebbles']) {
        return [cats['miss_awesomewumps'], cats['puffins'], cats['marshmallow']];
      }
      if (item === cats['marshmallow']) {
        return [cats['pebbles']];
      }
      if (item === cats['oreo']) {
        return [cats['tigger']];
      }
      if (item === cats['tigger']) {
        return [cats['oreo'], cats['mrs_smartfoot']];
      }
      if (item === cats['mittens']) {
        return [cats['felix']];
      }
      if (item === cats['mrs_clearclaws']) {
        return [cats['miss_awesomewumps']];
      }
      return [];
    },
    addAllOptions: () => [cats['felix'], cats['puffins'], cats['marshmallow'], cats['oreo'], cats['mrs_smartfoot']],
  },
};

export const MutualExclusion = Template.bind({});
MutualExclusion.args = {
  ...Basic.args,
  options: [cats['pebbles'], cats['marshmallow'], cats['mrs_clearclaws']],
  rules: {
    exclusion: (item) => {
      if (item === cats['pebbles']) {
        return [cats['marshmallow'], cats['mrs_clearclaws']];
      }
      if (item === cats['marshmallow']) {
        return [cats['pebbles'], cats['mrs_clearclaws']];
      }
      if (item === cats['mrs_clearclaws']) {
        return [cats['marshmallow'], cats['pebbles']];
      }
      return [];
    },
    addAllOptions: () => [cats['pebbles']],
  },
};

export const Dense = Template.bind({});
Dense.args = {
  ...MutualExclusion.args,
  dense: true,
};

export const MaxElements = Template.bind({});
MaxElements.args = {
  ...Basic.args,
  options: Object.values(cats),
  hideAddAllOptions: true,
  rules: {
    disableAddOptions: ({ selected }) => {
      return selected.length <= 4;
    },
  },
};

export const MinElements = Template.bind({});
MinElements.args = {
  ...Basic.args,
  options: Object.values(cats),
  value: [cats['felix'], cats['puffins'], cats['marshmallow'], cats['oreo']],
  hideRemoveAllOptions: true,
  rules: {
    disableRemoveOptions: ({ available }) => {
      return available.length <= 5;
    },
  },
};

const customIcons = {
  addOptionIcon: <ChevronLeft />,
  removeOptionIcon: <ChevronRight />,
};

export const CustomIcons = Template.bind({});
CustomIcons.args = {
  ...Basic.args,
  ...customIcons,
};

const customLabels: TransferListLabels = {
  addAllOptions: 'Aggiungi tutti',
  removeAllOptions: 'Rimuovi tutti',
  blockedBy: 'Blocca: ',
  removes: 'Rimuove: ',
  search: 'Cerca...',
  totalSelected: (n) => `Elementi selezionati: ${n}`,
  totalAvailable: (n) => `Elementi disponibili: ${n}`,
};

export const CustomLabels = Template.bind({});
CustomLabels.args = {
  ...ExclusionRules.args,
  labels: customLabels,
};

const customProps = {
  listProps: {
    dense: false,
  },
  availableListProps: {
    style: {
      backgroundColor: red[100],
    },
  },
  selectedListProps: {
    style: {
      backgroundColor: green[100],
    },
  },
  appBarProps: {
    color: 'secondary',
  },
  toolbarProps: {
    variant: 'regular',
  },
} as Partial<TransferListCustomizationProps>;

export const Customizations = Template.bind({});
Customizations.args = {
  ...Basic.args,
  ...customProps,
};

const BoxedTemplate: Story<TransferListProps<StoryOption>> = (props) => {
  return (
    <Paper elevation={2} style={{ overflow: 'hidden' }}>
      <Template {...props} />
    </Paper>
  );
};

export const Boxed = BoxedTemplate.bind({});
Boxed.args = {
  ...ExclusionRules.args,
  minHeight: 500,
  maxHeight: 500,
};

const ConfigContextTemplate: Story<TransferListProps<StoryOption> & { configContextProps: MuiInputsConfig }> = (
  props
) => {
  const { configContextProps, ...templateProps } = props;
  return (
    <MuiInputsConfigProvider config={configContextProps}>
      <Template {...templateProps} />
    </MuiInputsConfigProvider>
  );
};

export const ConfigContext = ConfigContextTemplate.bind({});
ConfigContext.args = {
  ...ExclusionRules.args,
  configContextProps: {
    transferList: {
      labels: customLabels,
      ...customProps,
      ...customIcons,
    },
  },
};

export const Mobile = Template.bind({});

Mobile.parameters = {
  viewport: {
    defaultViewport: 'iphone6',
  },
};

Mobile.args = {
  ...ExclusionRules.args,
  maxHeight: 460,
};

const RequestTemplate: Story<TransferListProps<StoryOption>> = (props) => {
  const initialValue = [cities[5], cities[14]];
  const [value, setValue] = useState<StoryOption[]>(initialValue);
  const { requestState, data, start, reset } = useFakeRequest(cities.slice(0, 20));

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'stretch' }}>
      <Box>
        <TransferList
          {...props}
          options={data || []}
          renderOption={(item) => item.name}
          filterOptions={(search) => (item) => item.name.toLowerCase().indexOf(search.toLowerCase()) >= 0}
          value={value || []}
          request={requestState}
          onChange={(...args) => {
            setValue(args[0]);
            if (props.onChange) {
              props.onChange(...args);
            }
          }}
        />
      </Box>
      <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <ProgressButton variant="outlined" onClick={() => start(false)} inProgress={requestState.inProgress}>
          {'Start request'}
        </ProgressButton>
        <Button
          variant="outlined"
          onClick={() => {
            setValue(initialValue);
            reset();
          }}
        >
          {'Reset'}
        </Button>
      </Box>
      <pre>
        <code>{`Request state: ${JSON.stringify(requestState, null, 2)}`}</code>
      </pre>
      <pre>
        <code>{`Data: ${JSON.stringify(data, null, 2)}`}</code>
      </pre>
    </Box>
  );
};

export const Request = RequestTemplate.bind({});
Request.args = {
  ...Basic.args,
  minHeight: 400,
  maxHeight: 400,
};

export const DisabledWithoutOnChange = Template.bind({});
DisabledWithoutOnChange.storyName = 'Disabled without onChange';
DisabledWithoutOnChange.args = {
  ...Basic.args,
  onChange: undefined,
  disabled: true,
};

function customRenderOption(option: StoryOption, asText?: boolean) {
  if (asText) {
    return `${option.name}`;
  }

  return (
    <div>
      <Typography variant="h6">{`${option.name}`}</Typography>
      <Typography variant="caption" gutterBottom>
        {`Sister city: ${(cities[Math.floor(Number(option.id) % 250) + 1000] as StoryOption).name}`}
      </Typography>
    </div>
  );
}

export const OptionAsComponent = Template.bind({});
OptionAsComponent.args = {
  ...Basic.args,
  renderOption: customRenderOption,
};
