import React, { useState, useEffect } from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';
import { add, sub } from 'date-fns';
import { LocalizationProvider } from '@material-ui/lab';
import DateFnsAdapter from '@material-ui/lab/AdapterDateFns';

import DatePicker, { DatePickerProps } from '../DatePicker';

export default {
  title: 'MUI Inputs/DatePicker',
  component: DatePicker,
  argTypes: {
    onChange: { action: 'onChange' },
    onBlur: { action: 'onBlur' },
    name: { control: { disable: true } },
  },
} as Meta;

const Template: Story<DatePickerProps> = (props) => {
  const [value, setValue] = useState(props.value);

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  return (
    <LocalizationProvider dateAdapter={DateFnsAdapter}>
      <DatePicker
        {...props}
        onChange={(...args) => {
          setValue(args[0]);
          if (props.onChange) {
            props.onChange(...args);
          }
        }}
        value={value}
      />
    </LocalizationProvider>
  );
};

export const Basic = Template.bind({});
Basic.args = {
  label: 'Date field label',
  // inputFormat: 'dd/MM/yyyy',
  value: new Date(),
  maxDate: add(new Date(), { years: 1 }),
  minDate: sub(new Date(), { years: 1 }),
};

export const NullDate = Template.bind({});
NullDate.args = {
  ...Basic.args,
  value: null,
};

export const WithError = Template.bind({});
WithError.args = {
  ...Basic.args,
  error: true,
  helperText: 'This is an error',
};

export const DisabledWithoutOnChange = Template.bind({});
DisabledWithoutOnChange.storyName = 'Disabled without onChange';
DisabledWithoutOnChange.args = {
  ...Basic.args,
  onChange: undefined,
  disabled: true,
};
