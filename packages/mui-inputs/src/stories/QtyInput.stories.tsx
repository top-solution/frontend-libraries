import React, { useState, useEffect } from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import Box from '@material-ui/core/Box';
import QtyInput, { QtyInputProps } from '../QtyInput';

export default {
  title: 'MUI Inputs/QtyInput',
  component: QtyInput,
  argTypes: {
    onChange: { action: 'onChange' },
    onBlur: { action: 'onBlur' },
    name: { control: { disable: true } },
  },
} as Meta;

const Template: Story<QtyInputProps> = (props) => {
  const [value, setValue] = useState(props.value);

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  return (
    <QtyInput
      {...props}
      onChange={(...args) => {
        setValue(args[1]);
        if (props.onChange) {
          props.onChange(...args);
        }
      }}
      value={value}
    />
  );
};

const FullWidthTemplate: Story<QtyInputProps> = (props) => {
  return (
    <Box sx={{ background: 'rgba(255,0,0,.2)', width: 300 }}>
      <Template {...props} />
    </Box>
  );
};

export const Basic = Template.bind({});
Basic.args = {
  label: 'Quantity field label',
  value: 42,
  min: 0,
  max: 50,
  float: false,
  step: 2,
};

export const DisabledWithoutOnChange = Template.bind({});
DisabledWithoutOnChange.storyName = 'Disabled without onChange';
DisabledWithoutOnChange.args = {
  ...Basic.args,
  onChange: undefined,
  disabled: true,
};

export const NullBelowMin = Template.bind({});
NullBelowMin.storyName = 'Null value below min';
NullBelowMin.args = {
  ...Basic.args,
  nullBelowMin: true,
  value: 2,
};

export const NullInitialValue = Template.bind({});
NullInitialValue.storyName = 'Null value below min';
NullInitialValue.args = {
  ...Basic.args,
  nullBelowMin: false,
  value: null,
};

export const AllowFloat = Template.bind({});
AllowFloat.storyName = 'Allow float values';
AllowFloat.args = {
  ...Basic.args,
  step: 0.5,
  value: 2.5,
  float: true,
};

export const FullWidth = FullWidthTemplate.bind({});
FullWidth.storyName = 'Full width';
FullWidth.args = {
  ...Basic.args,
  fullWidth: true,
};
