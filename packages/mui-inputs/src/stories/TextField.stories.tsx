import React, { useState, useEffect } from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import TextField, { TextFieldProps } from '../TextField';

export default {
  title: 'MUI Inputs/TextField',
  component: TextField,
  argTypes: {
    onChange: { action: 'onChange' },
    onBlur: { action: 'onBlur' },
    name: { control: { disable: true } },
  },
} as Meta;

const Template: Story<TextFieldProps> = (props) => {
  const [value, setValue] = useState(props.value);

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  return (
    <TextField
      {...props}
      onChange={(...args) => {
        setValue(args[1]);
        if (props.onChange) {
          props.onChange(...args);
        }
      }}
      value={value}
    />
  );
};

export const Basic = Template.bind({});
Basic.args = {
  label: 'Text field label',
  value: 'Quisque diam nisl augue',
};

export const NotClearable = Template.bind({});
NotClearable.args = {
  label: 'Text field label',
  disableClearable: true,
  value: 'Quisque diam nisl augue',
};

export const DisabledWithoutOnChange = Template.bind({});
DisabledWithoutOnChange.storyName = 'Disabled without onChange';
DisabledWithoutOnChange.args = {
  ...Basic.args,
  onChange: undefined,
  disabled: true,
};
