/* eslint-disable react/display-name */
import React, { useState, useEffect } from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';
import Autocomplete, { AutocompleteProps } from '../Autocomplete';
import ProgressButton from '../ProgressButton';
import useFakeRequest from './useFakeRequest';

export default {
  title: 'MUI Inputs/Autocomplete',
  component: Autocomplete,
  argTypes: {
    onChange: { action: 'onChange' },
    onInputChange: { action: 'onInputChange' },
    onBlur: { action: 'onBlur' },
    reqError: { control: null },
  },
} as Meta;

type StoryOption = { id: string; color: string };

const options = ['Red', 'Green', 'Blue', 'Yellow', 'Purple', 'Grey'].map((c) => ({
  id: c.toLowerCase(),
  color: c,
}));

const Template: Story<AutocompleteProps<StoryOption, true | false, true | false, false> & { reqError: boolean }> = (
  props
) => {
  const { reqError, ...componentProps } = props;
  const [value, setValue] = useState(props.value);
  const { requestState, data, start, reset } = useFakeRequest(options);

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', width: '300px', justifyContent: 'stretch' }}>
      <Box>
        <Autocomplete
          {...componentProps}
          request={requestState}
          options={data || []}
          getOptionLabel={(option) => option.color}
          onChange={(...args) => {
            setValue(args[1]);
            if (props.onChange) {
              props.onChange(...args);
            }
          }}
          value={value}
        />
      </Box>
      <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <ProgressButton variant="outlined" onClick={() => start(reqError)} inProgress={requestState.inProgress}>
          {'Start request'}
        </ProgressButton>
        <Button
          variant="outlined"
          onClick={() => {
            setValue([]);
            reset();
          }}
        >
          {'Reset'}
        </Button>
      </Box>
      <pre>
        <code>{`Request state: ${JSON.stringify(requestState, null, 2)}`}</code>
      </pre>
      <pre>
        <code>{`Data: ${JSON.stringify(data, null, 2)}`}</code>
      </pre>
    </Box>
  );
};

export const Basic = Template.bind({});
Basic.args = {
  label: 'Text field label',
  value: [],
  multiple: true,
};

export const SingleValue = Template.bind({});
SingleValue.args = {
  ...Basic.args,
  label: 'Text field label',
  value: null,
  multiple: false,
};

export const CustomOptions = Template.bind({});
CustomOptions.args = {
  ...Basic.args,
  getOptionLabel: undefined,
  renderOption: (props, option: StoryOption) => (
    <Box component="li" {...props} sx={{ display: 'flex', alignItems: 'center' }}>
      <Box sx={{ width: 18, height: 18, marginRight: 1 }} style={{ backgroundColor: option.color }} />
      {option.color}
    </Box>
  ),
  renderTags: (value: StoryOption[], getTagProps) =>
    value.map((option, i) => (
      // eslint-disable-next-line react/jsx-key
      <Chip
        variant="outlined"
        label={
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Box sx={{ width: 18, height: 18, marginRight: 1 }} style={{ backgroundColor: option.color }} />
            {option.color}
          </Box>
        }
        size="small"
        {...getTagProps({ index: i })}
      />
    )),
};

export const WithReqError = Template.bind({});
WithReqError.storyName = 'Request error';
WithReqError.args = {
  ...Basic.args,
  reqError: true,
};

export const DisabledWithoutOnChange = Template.bind({});
DisabledWithoutOnChange.storyName = 'Disabled without onChange';
DisabledWithoutOnChange.args = {
  ...Basic.args,
  onChange: undefined,
  disabled: true,
};

const FreeSoloTemplate: Story<
  AutocompleteProps<StoryOption['color'], true | false, true | false, true> & { reqError: boolean }
> = (props) => {
  const { reqError, ...componentProps } = props;
  const [value, setValue] = useState(props.value);
  const { requestState, data, start, reset } = useFakeRequest(options);

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', width: '300px', justifyContent: 'stretch' }}>
      <Box>
        {value}
        <Autocomplete
          {...componentProps}
          request={requestState}
          options={(data || []).map(({ color }) => color)}
          onChange={(...args) => {
            setValue(args[1]);
            if (props.onChange) {
              props.onChange(...args);
            }
          }}
          freeSolo
          value={value}
        />
      </Box>
      <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <ProgressButton variant="outlined" onClick={() => start(reqError)} inProgress={requestState.inProgress}>
          {'Start request'}
        </ProgressButton>
        <Button
          variant="outlined"
          onClick={() => {
            setValue([]);
            reset();
          }}
        >
          {'Reset'}
        </Button>
      </Box>
      <pre>
        <code>{`Request state: ${JSON.stringify(requestState, null, 2)}`}</code>
      </pre>
      <pre>
        <code>{`Data: ${JSON.stringify(data, null, 2)}`}</code>
      </pre>
    </Box>
  );
};

export const FreeSolo = FreeSoloTemplate.bind({});
FreeSolo.storyName = 'FreeSolo';
FreeSolo.args = {
  freeSolo: true,
  value: '',
};

const VariantsTemplate: Story<
  AutocompleteProps<StoryOption, true | false, true | false, true | false> & { reqError: boolean }
> = (props) => {
  const { reqError, ...componentProps } = props;
  const [value, setValue] = useState(props.value);
  const { requestState, data, start, reset } = useFakeRequest(options);

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', width: '300px', justifyContent: 'stretch' }}>
      <Box>
        <Autocomplete
          {...componentProps}
          request={requestState}
          options={data || []}
          getOptionLabel={(option) => option.color}
          onChange={(...args) => {
            setValue(args[1]);
            if (props.onChange) {
              props.onChange(...args);
            }
          }}
          value={value}
          label="Outlined"
          variant="outlined"
        />
        <Autocomplete
          {...componentProps}
          request={requestState}
          options={data || []}
          getOptionLabel={(option) => option.color}
          onChange={(...args) => {
            setValue(args[1]);
            if (props.onChange) {
              props.onChange(...args);
            }
          }}
          value={value}
          label="Filled"
          variant="filled"
        />
        <Autocomplete
          {...componentProps}
          request={requestState}
          options={data || []}
          getOptionLabel={(option) => option.color}
          onChange={(...args) => {
            setValue(args[1]);
            if (props.onChange) {
              props.onChange(...args);
            }
          }}
          value={value}
          label="Standard"
          variant="standard"
        />
      </Box>
      <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <ProgressButton variant="outlined" onClick={() => start(reqError)} inProgress={requestState.inProgress}>
          {'Start request'}
        </ProgressButton>
        <Button
          variant="outlined"
          onClick={() => {
            setValue([]);
            reset();
          }}
        >
          {'Reset'}
        </Button>
      </Box>
      <pre>
        <code>{`Request state: ${JSON.stringify(requestState, null, 2)}`}</code>
      </pre>
      <pre>
        <code>{`Data: ${JSON.stringify(data, null, 2)}`}</code>
      </pre>
    </Box>
  );
};

export const Variants = VariantsTemplate.bind({});
Variants.storyName = 'Variants';
Variants.args = {
  value: '',
};
