import { useCallback, useState, useMemo } from 'react';
import { RequestState, RequestError } from '@top-solution/utils';

export default function useFakeRequest<T>(data: T) {
  const [state, setState] = useState<'RESET' | 'PROGRESS' | 'DONE'>('RESET');
  const [lastUpdate, setLastUpdate] = useState<null | Date>(null);
  const [error, setError] = useState<null | RequestError>(null);

  const start = useCallback(
    (withError: booleans) => {
      setState('PROGRESS');
      setTimeout(() => {
        if (withError) {
          setError({
            code: '500',
            method: 'POST',
            url: 'https://example.com',
            detail: 'Internal server error',
            axiosError: null,
          } as RequestError);
          setState('DONE');
        } else {
          setLastUpdate(new Date());
          setState('DONE');
        }
      }, 2000);
    },
    [setState]
  );

  const reset = useCallback(() => {
    setState('RESET');
    setError(null);
  }, [setState]);

  const requestState: RequestState = useMemo(
    () => ({
      inProgress: state === 'PROGRESS',
      lastUpdate,
      error,
    }),
    [lastUpdate, error, state]
  );

  return {
    start,
    reset,
    requestState,
    data: state === 'DONE' && error === null ? data : null,
  };
}
