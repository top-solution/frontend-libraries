import React, { useState, useEffect } from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';
import { add, sub, endOfToday, isFriday } from 'date-fns';
import { amber, pink } from '@material-ui/core/colors';
import { createTheme, ThemeProvider } from '@material-ui/core/styles';
import { LocalizationProvider } from '@material-ui/lab';
import DateFnsAdapter from '@material-ui/lab/AdapterDateFns';

import { MuiInputsConfig, MuiInputsConfigProvider } from '../ConfigContext';
import DateRangePicker, { DateRangePickerProps } from '../DateRangePicker';

export default {
  title: 'MUI Inputs/DateRangePicker',
  component: DateRangePicker,
  argTypes: {
    onChange: { action: 'onChange' },
    onBlur: { action: 'onBlur' },
    name: { control: { disable: true } },
  },
} as Meta;

const Template: Story<DateRangePickerProps> = (props) => {
  const [value, setValue] = useState(props.value);

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  return (
    <LocalizationProvider dateAdapter={DateFnsAdapter}>
      <DateRangePicker
        {...props}
        onChange={(...args) => {
          setValue(args[0]);
          if (props.onChange) {
            props.onChange(...args);
          }
        }}
        value={value}
      />
    </LocalizationProvider>
  );
};
const today = endOfToday();
export const Basic = Template.bind({});
Basic.args = {
  labels: {
    start: 'Check-in',
    middle: 'to',
    end: 'Check-out',
  },
  inputFormat: 'dd/MM/yyyy',
  value: [sub(today, { days: 10 }), today],
  maxDate: add(today, { months: 25 }),
  minDate: sub(today, { months: 1 }),
  inputsWidth: 250,
};

export const NullDate = Template.bind({});
NullDate.args = {
  ...Basic.args,
  value: [null, null],
};

export const Disabled = Template.bind({});
Disabled.args = {
  ...Basic.args,
  disabled: true,
};

export const DifferentInputFormat = Template.bind({});
DifferentInputFormat.args = {
  ...Basic.args,
  inputFormat: 'MM/dd/yyyy',
};

export const WithError = Template.bind({});
WithError.args = {
  ...Basic.args,
  startError: true,
  endError: true,
  startHelperText: 'This is an error',
  endHelperText: 'This is another error',
};

export const DisabledWithoutOnChange = Template.bind({});
DisabledWithoutOnChange.storyName = 'Disabled without onChange';
DisabledWithoutOnChange.args = {
  ...Basic.args,
  onChange: undefined,
  disabled: true,
};

export const MinMax = Template.bind({});
MinMax.storyName = 'With minDate and maxDate';
MinMax.args = {
  ...Basic.args,
  value: [today, add(today, { days: 10 })],
  minDate: sub(today, { days: 10 }),
  maxDate: add(today, { days: 20 }),
};

export const WithoutMiddleText = Template.bind({});
WithoutMiddleText.args = {
  ...Basic.args,
  labels: {
    middle: null,
  },
};

const ThemeTemplate: Story<DateRangePickerProps> = (props) => {
  return (
    <ThemeProvider
      theme={createTheme({
        palette: {
          primary: pink,
          secondary: amber,
        },
      })}
    >
      <Template {...props} />
    </ThemeProvider>
  );
};

export const ThemingAndPropsOverriding = ThemeTemplate.bind({});
ThemingAndPropsOverriding.args = {
  ...Basic.args,
  inputsWidth: 150,
  labels: {
    ...Basic.args.labels,
    cancel: 'Nope',
    apply: 'Good enough',
  },
  startInputProps: {
    variant: 'outlined',
    label: 'Check-in',
    size: 'small',
  },
  endInputProps: {
    variant: 'outlined',
    label: 'Check-out',
    size: 'small',
  },
  datePickersProps: {
    shouldDisableDate: (day) => {
      if (isFriday(day as Date)) {
        return true;
      }
      return false;
    },
  },
  actionButtonsProps: {
    variant: 'outlined',
  },
  cancelButtonProps: {
    color: 'primary',
  },
  applyButtonProps: {
    color: 'secondary',
  },
};

const ContextTemplate: Story<DateRangePickerProps & { configContextProps: MuiInputsConfig }> = (props) => {
  const { configContextProps, ...templateProps } = props;

  return (
    <MuiInputsConfigProvider config={configContextProps}>
      <Template {...templateProps} />
    </MuiInputsConfigProvider>
  );
};

export const WithContextConfig = ContextTemplate.bind({});
WithContextConfig.args = {
  ...Basic.args,
  labels: undefined,
  inputFormat: undefined,
  configContextProps: {
    datePicker: {
      inputFormat: 'yyyy/MM/dd',
      minDate: sub(new Date(), { weeks: 3 }),
      maxDate: add(new Date(), { weeks: 2 }),
    },
    dateRangePicker: {
      labels: {
        apply: "That's OK for me",
        cancel: 'It was better before',
        end: 'Check-out',
        start: 'Check-it',
        middle: 'until',
        minDateError: (date: string) => `Too soon, try after ${date}`,
        maxDateError: (date: string) => `Too late, try before ${date}`,
      },
      inputsWidth: 200,
    },
  },
};
