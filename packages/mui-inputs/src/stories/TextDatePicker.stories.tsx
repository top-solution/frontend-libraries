import React, { useCallback, useState } from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';
import { add, sub } from 'date-fns';
import { LocalizationProvider } from '@material-ui/lab';
import DateFnsAdapter from '@material-ui/lab/AdapterDateFns';

import TextDatePicker, { TextDatePickerProps } from '../TextDatePicker';

export default {
  title: 'MUI Inputs/TextDatePicker',
  component: TextDatePicker,
  argTypes: {},
} as Meta;

const Template: Story<TextDatePickerProps> = (props) => {
  const [value, setValue] = useState(props.value);
  const onChange = useCallback(
    (e, value: Date) => {
      setValue(value);
      if (props.onChange) {
        props.onChange(e, value);
      }
    },
    [props]
  );

  return (
    <LocalizationProvider dateAdapter={DateFnsAdapter}>
      <TextDatePicker {...props} onChange={onChange} value={value} />
    </LocalizationProvider>
  );
};
const today = new Date();

export const Basic = Template.bind({});
Basic.args = {
  format: 'dd/MM/yyyy',
  value: today,
  maxDate: add(today, { months: 25 }),
  minDate: sub(today, { months: 1 }),
  label: 'Text date selector',
};

export const DifferentFormat = Template.bind({});
DifferentFormat.args = {
  ...Basic.args,
  format: 'yyyy/dd/MM',
};

export const WithTime = Template.bind({});
WithTime.args = {
  ...Basic.args,
  format: 'dd/MM/yyyy HH:mm:ss',
};

export const WithMilliseconds = Template.bind({});
WithMilliseconds.args = {
  ...Basic.args,
  format: 'dd/MM/yyyy HH:mm:ss:SSS',
};
