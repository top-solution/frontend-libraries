import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import { RequestError } from '@top-solution/utils';
import ErrorMessage, { ErrorMessageProps } from '../ErrorMessage';

export default {
  title: 'MUI Inputs/ErrorMessage',
  component: ErrorMessage,
  argTypes: {},
} as Meta;

const Template: Story<ErrorMessageProps> = (props) => <ErrorMessage {...props} />;

export const Basic = Template.bind({});
Basic.storyName = 'RequestError';
Basic.args = {
  error: {
    code: '500',
    method: 'POST',
    url: 'https://example.com',
    detail: 'Internal server error',
    axiosError: null,
    name: 'Error',
    message: 'Error message',
  } as RequestError,
};

export const JSError = Template.bind({});
JSError.args = {
  error: new Error('Segmentation fault (core dumped)'),
};

export const NullError = Template.bind({});
NullError.args = {
  error: null,
};
