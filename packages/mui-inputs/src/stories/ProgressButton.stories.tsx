import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import Alert from '@material-ui/core/Alert';
import Box from '@material-ui/core/Box';
import ProgressButton, { ProgressButtonProps } from '../ProgressButton';

export default {
  title: 'MUI Inputs/ProgressButton',
  component: ProgressButton,
  argTypes: {
    onClick: { action: 'onClick' },
    inProgress: { control: 'boolean' },
  },
} as Meta;

const Template: Story<ProgressButtonProps> = (props) => {
  return (
    <Box>
      <Alert severity="warning">
        {'ProgressButton component is deprecated, use @material-ui/lab/LoadingButton instad'}
      </Alert>
      <Box sx={{ display: 'flex', alignItems: 'center', mb: 2 }}>
        <Box sx={{ minWidth: 100 }}>Standard:</Box>
        <ProgressButton {...props} />
      </Box>
      <Box sx={{ display: 'flex', alignItems: 'center', mb: 2 }}>
        <Box sx={{ minWidth: 100 }}>Outlined:</Box>
        <ProgressButton {...props} variant="outlined" />
      </Box>
      <Box sx={{ display: 'flex', alignItems: 'center', mb: 2 }}>
        <Box sx={{ minWidth: 100 }}>Contained:</Box>
        <ProgressButton {...props} variant="contained" />
      </Box>
    </Box>
  );
};

export const Basic = Template.bind({});
Basic.args = {
  name: 'field_name',
  children: 'Button text',
  inProgress: true,
};
