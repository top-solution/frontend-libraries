import React, { useState, useEffect } from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import CheckBox, { CheckBoxProps } from '../CheckBox';

export default {
  title: 'MUI Inputs/CheckBox',
  component: CheckBox,
  argTypes: {
    onChange: { action: 'onChange' },
    onBlur: { action: 'onBlur' },
    value: { control: 'boolean' },
    name: { control: { disable: true } },
    required: { control: 'boolean' },
  },
} as Meta;

const FIELD_NAME = 'field_name';

const Template: Story<CheckBoxProps<typeof FIELD_NAME>> = (props) => {
  const [value, setValue] = useState(props.value || false);

  useEffect(() => {
    setValue(props.value || false);
  }, [props.value]);

  return (
    <CheckBox
      {...props}
      onChange={(...args) => {
        setValue(args[0] || false);
        if (props.onChange) {
          props.onChange(...args);
        }
      }}
      value={value}
    />
  );
};

export const Basic = Template.bind({});
Basic.args = {
  name: FIELD_NAME,
  label: 'Check this box',
  value: false,
  required: true,
};

export const LabelComponent = Template.bind({});
LabelComponent.args = {
  name: FIELD_NAME,
  label: (
    <span>
      Accept <a href="">our terms</a>
    </span>
  ),
  value: false,
  required: true,
};

export const DisabledWithoutOnChange = Template.bind({});
DisabledWithoutOnChange.storyName = 'Disabled without onChange';
DisabledWithoutOnChange.args = {
  ...Basic.args,
  onChange: undefined,
  disabled: true,
};
