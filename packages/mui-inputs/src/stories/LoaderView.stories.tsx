import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import { Story, Meta } from '@storybook/react/types-6-0';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import LoaderView, { LoaderViewProps } from '../LoaderView';

export default {
  title: 'MUI Inputs/LoaderView',
  component: LoaderView,
  parameters: {
    viewport: {
      viewports: INITIAL_VIEWPORTS,
    },
  },
} as Meta;

const Template: Story<LoaderViewProps> = (props) => {
  return (
    <Box sx={{ maxWidth: 400 }}>
      <Paper elevation={1}>
        <Box sx={{ padding: 2 }}>
          <LoaderView {...props}>
            <Card variant="outlined">
              <CardContent>
                <Typography color="textSecondary" gutterBottom>
                  Word of the Day
                </Typography>
                <Typography variant="h5" component="h2">
                  {`be•nev•o•lent`}
                </Typography>
                <Typography color="textSecondary">adjective</Typography>
                <Typography variant="body2" component="p">
                  well meaning and kindly.
                  <br />
                  {'"a benevolent smile"'}
                </Typography>
              </CardContent>
              <CardActions>
                <Button size="small">Learn More</Button>
              </CardActions>
            </Card>
          </LoaderView>
        </Box>
      </Paper>
    </Box>
  );
};

export const Basic = Template.bind({});
Basic.args = {
  condition: false,
  minHeight: 205,
};
