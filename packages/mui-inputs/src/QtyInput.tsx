import React, { useEffect, useState } from 'react';
import IconButton from '@material-ui/core/IconButton';
import TextField, { StandardTextFieldProps } from '@material-ui/core/TextField';
import MinusIcon from 'mdi-material-ui/Minus';
import PlusIcon from 'mdi-material-ui/Plus';

export interface QtyInputProps extends Omit<StandardTextFieldProps, 'onChange'> {
  value: number | null;
  onChange: (
    event: React.ChangeEvent<HTMLInputElement> | React.MouseEvent<HTMLButtonElement>,
    value: number | null
  ) => void;
  /**
   * The minimum allowed value.
   */
  min?: number;
  /**
   * The maximum allowed value.
   */
  max?: number;
  /**
   * Set the value to undefined as value when it is below the minimum allowed value.
   */
  nullBelowMin?: boolean;
  /**
   * The increase/decrease step
   */
  step?: number;
  /**
   * Allow float values
   */
  float?: boolean;
}

const floatStringValueRegexp = /^(-)?[0-9]*([,.][0-9]*)?$/g;
const stringValueRegexp = /^(-)?[0-9]*$/g;

const NULL_VALUE_STRING = '--';

function QtyInput(props: QtyInputProps, ref: React.Ref<HTMLDivElement>): JSX.Element {
  const {
    value,
    min,
    max,
    step,
    float,
    nullBelowMin,
    onChange,
    onBlur,
    disabled,
    helperText,
    InputProps,
    ...otherProps
  } = props;

  const safeStep = (float ? step : Math.floor(step || 1)) || 1;

  // Store the string value in a separate field because of edge cases that happens while user is typing
  // e.g. to type 3.1 stringValue is going to be "3" -> "3." (this one is tricky as it is not a number) -> "3.1"
  const [stringValue, setStringValue] = useState(value === null ? NULL_VALUE_STRING : `${value}`);

  useEffect(() => {
    setStringValue(value === null ? NULL_VALUE_STRING : `${value}`);
  }, [value]);

  const handleChange = function (event: React.ChangeEvent<HTMLInputElement>) {
    if (onChange) {
      if (
        (typeof event.target.value === 'undefined' || event.target.value === '' || event.target.value === null) &&
        nullBelowMin
      ) {
        return onChange(event, null);
      }

      if (!event.target.value) {
        onChange(event, 0);
      }

      let qty = Number(event.target.value as string);

      if (!isNaN(qty)) {
        if (typeof min !== 'undefined') {
          qty = Math.max(min, qty);
        }
        if (typeof max !== 'undefined') {
          qty = Math.min(max, qty);
        }
        onChange(event, qty);
      }

      if (event.target.value.match(float ? floatStringValueRegexp : stringValueRegexp)) {
        setStringValue(event.target.value);
      }
    }
  };

  const increase = function (event: React.MouseEvent<HTMLButtonElement>) {
    if (onChange) {
      const qty = (value ?? (min ?? safeStep) - safeStep) + safeStep;
      if (typeof max === 'undefined' || qty <= max) {
        if (qty !== null) {
          onChange(event, qty);
        }
      }
    }
  };

  const decrease = function (event: React.MouseEvent<HTMLButtonElement>) {
    if (onChange) {
      const qty = (value || 0) - safeStep;
      if (typeof min === 'undefined' || qty >= min) {
        if (qty !== null) {
          onChange(event, qty);
        }
      } else if (nullBelowMin) {
        onChange(event, null);
      }
    }
  };

  return (
    <TextField
      {...otherProps}
      ref={ref}
      sx={{
        minWidth: '14ch',
        maxWidth: props.fullWidth ? undefined : '16ch',
        width: props.fullWidth ? '100%' : undefined,
        '& .MuiInputBase-input': {
          textAlign: 'center',
        },
        '& label': {
          whiteSpace: 'nowrap',
        },
        ...props.sx,
      }}
      value={stringValue}
      disabled={disabled}
      onChange={handleChange}
      onBlur={(e) => {
        if (onBlur) {
          onBlur(e);

          setStringValue(value === null ? NULL_VALUE_STRING : `${value}`);
        }
      }}
      helperText={helperText || ' '}
      InputProps={{
        ...InputProps,
        startAdornment: disabled ? undefined : (
          <IconButton
            sx={{ margin: -3 / 2 }}
            onClick={decrease}
            disabled={(!nullBelowMin && value === min) || value === null || !onChange}
          >
            <MinusIcon />
          </IconButton>
        ),
        endAdornment: disabled ? undefined : (
          <IconButton
            sx={{ margin: -3 / 2 }}
            onClick={increase}
            disabled={(!nullBelowMin && value === max) || !onChange}
          >
            <PlusIcon />
          </IconButton>
        ),
        onMouseDown: (e) =>
          (e.target as HTMLInputElement).value === NULL_VALUE_STRING ? (e.target as HTMLInputElement).select() : null,
      }}
    />
  );
}

const QtyInputWithRefAndMemo = React.memo(React.forwardRef(QtyInput)) as typeof QtyInput;
export default QtyInputWithRefAndMemo;
