import React, { useEffect } from 'react';
import Box from '@material-ui/core/Box';
import { ButtonProps } from '@material-ui/core/Button';
import LoadingButton from '@material-ui/lab/LoadingButton';

export interface ProgressButtonProps extends ButtonProps {
  /**
   * If `true', shows an infinite progress bar and disables the button.
   */
  inProgress: boolean;
}

function ProgressButton(props: ProgressButtonProps, ref: React.Ref<HTMLDivElement>): JSX.Element {
  const { inProgress, ...otherProps } = props;

  useEffect(() => {
    // eslint-disable-next-line no-console
    console.warn('ProgressButton component is deprecated, use @material-ui/lab/LoadingButton instad');
    // eslint-disable-next-line no-console
    console.warn(new Error().stack);
  }, []);

  return (
    <Box ref={ref}>
      <LoadingButton {...otherProps} loading={inProgress} />
    </Box>
  );
}

const ProgressButtonWithRefAndMemo = React.memo(React.forwardRef(ProgressButton)) as typeof ProgressButton;
export default ProgressButtonWithRefAndMemo;
