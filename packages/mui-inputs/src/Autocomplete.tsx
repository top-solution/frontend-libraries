import React from 'react';
import MUIAutocomplete, { AutocompleteProps as MUIAutocompleteProps } from '@material-ui/core/Autocomplete';
import Box from '@material-ui/core/Box';
import LinearProgress from '@material-ui/core/LinearProgress';
import TextField, { TextFieldProps } from '@material-ui/core/TextField';

import { ObjectWithId, RequestState } from '@top-solution/utils';
import ErrorMessage from './ErrorMessage';

export interface AutocompleteProps<
  T extends ObjectWithId | string,
  Multiple extends boolean | undefined,
  DisableClearable extends boolean | undefined,
  FreeSolo extends boolean | undefined
> extends Omit<MUIAutocompleteProps<T, Multiple, DisableClearable, FreeSolo>, 'renderInput'>,
    Pick<TextFieldProps, 'label' | 'required' | 'error' | 'helperText' | 'inputRef' | 'variant'> {
  request?: RequestState;
  keepEnabledOnRequest?: boolean;
}

function Autocomplete<
  T extends ObjectWithId | string,
  Multiple extends boolean | undefined,
  DisableClearable extends boolean | undefined,
  FreeSolo extends boolean | undefined
>(props: AutocompleteProps<T, Multiple, DisableClearable, FreeSolo>, ref: React.Ref<unknown>): JSX.Element {
  const {
    value,
    label,
    options,
    onChange,
    onBlur,
    className,
    request,
    required,
    disabled,
    error,
    helperText,
    disableClearable,
    keepEnabledOnRequest,
    variant,
    ...otherProps
  } = props;

  return (
    <Box
      className={className}
      sx={{
        display: 'inline-flex',
        position: 'relative',
        width: '100%',
        ...props.style,
      }}
    >
      <MUIAutocomplete
        {...otherProps}
        ref={ref}
        disableClearable={disableClearable}
        value={value}
        options={options}
        onChange={onChange}
        onBlur={onBlur}
        isOptionEqualToValue={(option: T, item: T) =>
          typeof option !== 'string' && typeof item !== 'string' ? option.id === item.id : option === item
        }
        disabled={disabled || (keepEnabledOnRequest !== true && (options.length === 0 || request?.inProgress))}
        fullWidth
        renderInput={(params: TextFieldProps) => (
          <TextField
            {...params}
            label={label}
            helperText={request?.error ? <ErrorMessage error={request.error} /> : helperText || ' '}
            error={error || Boolean(request?.error)}
            required={required}
            variant={variant}
          />
        )}
      />
      {request?.inProgress && (
        <LinearProgress
          color="secondary"
          className={variant}
          sx={{
            position: 'absolute',
            top: 52,
            left: 0,
            right: 0,
            borderRadius: 1,
            borderTopLeftRadius: 0,
            borderTopRightRadius: 0,
            '&.standard': {
              top: 48,
              borderRadius: 0,
            },

            '&.filled': {
              borderRadius: 0,
            },
          }}
        />
      )}
    </Box>
  );
}

const AutocompleteWithRefAndMemo = React.memo(React.forwardRef(Autocomplete)) as typeof Autocomplete;
export default AutocompleteWithRefAndMemo;
