import React from 'react';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton/IconButton';
import LinearProgress from '@material-ui/core/LinearProgress';
import MenuItem from '@material-ui/core/MenuItem';
import TextField, { StandardTextFieldProps } from '@material-ui/core/TextField';
import ClearIcon from 'mdi-material-ui/Close';

import { ObjectWithId, RequestState } from '@top-solution/utils';
import ErrorMessage from './ErrorMessage';

export interface SelectProps<T extends ObjectWithId> extends Omit<StandardTextFieldProps, 'value' | 'onChange'> {
  value?: T | null;
  onChange?: (
    event: React.ChangeEvent<HTMLInputElement> | React.MouseEvent<HTMLButtonElement>,
    value: T | null
  ) => void;
  /**
   * The options shown in the select component.
   */
  options: T[];
  /**
   * A RequestState object representing the network request to fetch this component options.
   */
  request?: RequestState;
  /**
   * Render function of the select options.
   */
  renderOption: (value: T) => React.ReactNode;
  disableClearable?: boolean;
}

function Select<T extends ObjectWithId>(props: SelectProps<T>, ref: React.Ref<HTMLDivElement>): JSX.Element {
  const {
    value,
    onChange,
    className,
    style,
    options,
    request,
    renderOption,
    disabled,
    helperText,
    error,
    disableClearable,
    variant,
    ...otherProps
  } = props;

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (onChange) {
      const option = options.find((option) => option.id === event.target.value);
      onChange(event, option ?? null);
    }
  };

  return (
    <Box
      className={className}
      sx={{
        display: 'inline-flex',
        position: 'relative',
        width: '100%',

        '& .Mui-error': {
          whiteSpace: 'nowrap',
        },
        ...style,
      }}
    >
      <TextField
        select
        {...otherProps}
        ref={ref}
        value={value && options.length > 0 ? value.id : ''}
        onChange={handleChange}
        error={Boolean(error || request?.error)}
        helperText={(request?.error && <ErrorMessage error={request?.error} />) || helperText || ' '}
        disabled={disabled || request?.inProgress || options.length === 0}
        InputProps={{
          endAdornment:
            (!disableClearable && Boolean(value) && onChange && (
              <IconButton
                size="small"
                onClick={(e) => onChange(e, null)}
                sx={{
                  position: 'absolute',
                  right: 24,
                  top: 'calc(50% - 13px)',
                }}
              >
                <ClearIcon fontSize="small" />
              </IconButton>
            )) ||
            undefined,
        }}
        fullWidth
        variant={variant}
      >
        {options.map((value) => (
          <MenuItem value={value.id} key={value.id}>
            {renderOption(value)}
          </MenuItem>
        ))}
      </TextField>
      {request?.inProgress && (
        <LinearProgress
          color="secondary"
          className={variant}
          sx={{
            position: 'absolute',
            top: 52,
            left: 0,
            right: 0,
            borderRadius: 1,
            borderTopLeftRadius: 0,
            borderTopRightRadius: 0,
            '&.standard': {
              top: 48,
              borderRadius: 0,
            },

            '&.filled': {
              borderRadius: 0,
            },
          }}
        />
      )}
    </Box>
  );
}

const SelectWithRefAndMemo = React.memo(React.forwardRef(Select)) as typeof Select;
export default SelectWithRefAndMemo;
