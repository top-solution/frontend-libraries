import React, { useCallback, useMemo } from 'react';
import { BoxProps } from '@material-ui/core/Box';
import { Theme } from '@material-ui/core/styles';
import CloseCircle from 'mdi-material-ui/CloseCircle';
import { ObjectWithId } from '@top-solution/utils';
import TransferListItem from './TransferListItem';

type SelectedListItemProps<T extends ObjectWithId> = {
  disabled: boolean;
  option: T;
  renderOption: (option: T, asText?: boolean) => string | JSX.Element;
  secondaryLabel: (option: T) => string | undefined;
  onRemove: (option: T) => void;
  iconDisabled: boolean;
  removeOptionIcon: undefined | React.ReactNode;
  dense?: boolean | undefined;
  sx?: BoxProps['sx'];
};

function SelectedListItem<T extends ObjectWithId>(props: SelectedListItemProps<T>, ref: React.Ref<HTMLDivElement>) {
  const { disabled, option, renderOption, secondaryLabel, onRemove, iconDisabled, removeOptionIcon } = props;
  const handleClick = useCallback(() => onRemove(option), [onRemove, option]);

  const primary = useMemo(() => renderOption(option), [option, renderOption]);
  const secondary = useMemo(() => (secondaryLabel ? secondaryLabel(option) : undefined), [option, secondaryLabel]);

  const removeIcon = useMemo(() => removeOptionIcon || <CloseCircle />, [removeOptionIcon]);

  return (
    <TransferListItem
      ref={ref}
      key={option.id}
      disabled={disabled}
      primary={primary}
      secondary={secondary}
      icon={removeIcon}
      iconDisabled={iconDisabled}
      iconAriaLabel="remove"
      iconOnRight={true}
      onClick={handleClick}
      dense={props.dense}
      sx={{
        border: `2px solid transparent`,
        borderRadius: 1,
        transition: (theme: Theme) =>
          theme.transitions.create('border-color', { duration: theme.transitions.duration.shortest }),
        boxSizing: 'border-box',
        paddingLeft: 2,
        ...props.sx,
      }}
    />
  );
}

const SelectedListItemWithRefAndMemo = React.memo(React.forwardRef(SelectedListItem)) as typeof SelectedListItem;
export default SelectedListItemWithRefAndMemo;
