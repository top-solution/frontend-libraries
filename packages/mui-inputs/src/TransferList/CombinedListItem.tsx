import React, { useCallback, useMemo } from 'react';
import { BoxProps } from '@material-ui/core/Box';
import { ObjectWithId } from '@top-solution/utils';
import TransferListItem from './TransferListItem';

type CombinedListItemProps<T extends ObjectWithId> = {
  disabled: boolean;
  option: T;
  checked: boolean;
  renderOption: (option: T, asText?: boolean) => string | JSX.Element;
  secondaryLabel: (option: T, disabledBy: T[]) => string | undefined;
  disabledBy: T[];
  disableRemoveOptions: boolean;
  onAdd: (option: T) => void;
  onRemove: (option: T) => void;
  dense?: boolean | undefined;
  sx?: BoxProps['sx'];
};

function CombinedListItem<T extends ObjectWithId>(
  props: CombinedListItemProps<T>,
  ref: React.Ref<HTMLDivElement>
): JSX.Element {
  const { option, checked, renderOption, secondaryLabel, disabledBy, disableRemoveOptions, onAdd, onRemove } = props;

  const handleClick = useCallback(() => {
    if (checked) {
      onRemove(option);
    } else {
      onAdd(option);
    }
  }, [checked, onAdd, onRemove, option]);

  const primary = useMemo(() => renderOption(option), [option, renderOption]);
  const secondary = useMemo(() => secondaryLabel(option, disabledBy), [disabledBy, option, secondaryLabel]);

  const disabled = checked ? disableRemoveOptions : props.disabled;

  return (
    <TransferListItem
      ref={ref}
      key={option.id}
      disabled={disabled}
      checkbox={true}
      checked={props.checked}
      primary={primary}
      secondary={secondary}
      onClick={handleClick}
      dense={props.dense}
      sx={{
        minHeight: 60,
        paddingRight: 2,
        paddingLeft: 2,
        ...props.sx,
      }}
    />
  );
}

const CombinedListItemWithRefAndMemo = React.memo(React.forwardRef(CombinedListItem)) as typeof CombinedListItem;
export default CombinedListItemWithRefAndMemo;
