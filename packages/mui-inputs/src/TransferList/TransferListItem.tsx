import React from 'react';
import Box, { BoxProps } from '@material-ui/core/Box';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';

type TransferListItemProps = {
  disabled: boolean;
  checkbox?: boolean;
  checked?: boolean;
  primary: string | JSX.Element;
  secondary: string | undefined;
  icon?: React.ReactNode;
  iconDisabled?: boolean;
  iconAriaLabel?: string;
  iconOnRight?: boolean;
  onClick: () => void;
  onMouseEnter?: () => void;
  onMouseLeave?: () => void;
  onButtonMouseEnter?: () => void;
  onButtonMouseLeave?: () => void;
  ref?: React.Ref<unknown>;
  dense?: boolean | undefined;
  sx: BoxProps['sx'];
};

/**
 * Generic ListItem for the TransferList, as a memoized pure function
 * (MUI ListItem re-renders even if memoized).
 * Sick performance gains.
 */
function TransferListItem(props: TransferListItemProps, ref: React.Ref<HTMLDivElement>): JSX.Element {
  const {
    disabled,
    checkbox,
    checked,
    primary,
    secondary,
    icon,
    iconDisabled,
    iconAriaLabel,
    iconOnRight,
    onMouseEnter,
    onMouseLeave,
    onButtonMouseEnter,
    onButtonMouseLeave,
    onClick,
  } = props;

  return (
    <Box
      ref={ref}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      sx={{
        ...props.sx,
        display: 'flex',
        alignItems: 'center',
        padding: 0.5,
        flexDirection: iconOnRight ? 'row-reverse' : undefined,
      }}
    >
      {icon ? (
        <IconButton
          sx={{
            p: 1,
            marginRight: iconOnRight ? 0.5 : 1,
          }}
          onClick={onClick}
          aria-label={iconAriaLabel}
          disabled={iconDisabled}
          onMouseEnter={onButtonMouseEnter}
          onMouseLeave={onButtonMouseLeave}
        >
          {icon}
        </IconButton>
      ) : null}
      {checkbox ? (
        <Checkbox edge="start" onClick={onClick} disabled={disabled} checked={checked} tabIndex={-1} disableRipple />
      ) : null}
      <Box sx={{ display: 'flex', flexDirection: 'column', flex: '1 1 auto' }}>
        <Typography variant="body2" color={disabled ? 'textSecondary' : undefined} component="div">
          {primary}
        </Typography>
        {props.dense !== true ? (
          <Typography variant="body2" color="textSecondary">
            {secondary}
          </Typography>
        ) : null}
      </Box>
    </Box>
  );
}

const TransferListItemWithRefAndMemo = React.memo(React.forwardRef(TransferListItem)) as typeof TransferListItem;
export default TransferListItemWithRefAndMemo;
