import React, { useCallback, useContext, useMemo, useState } from 'react';
import AutoSizer from 'react-virtualized-auto-sizer';
import { FixedSizeList as RVList, ListChildComponentProps } from 'react-window';
import { differenceBy } from 'lodash';
import AppBar, { AppBarProps } from '@material-ui/core/AppBar';
import Box, { BoxProps } from '@material-ui/core/Box';
import Button, { ButtonProps } from '@material-ui/core/Button';
import { grey, red } from '@material-ui/core/colors';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import InputBase from '@material-ui/core/InputBase';
import List, { ListProps } from '@material-ui/core/List';
import { alpha, useTheme } from '@material-ui/core/styles';
import Toolbar, { ToolbarProps } from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CloseIcon from 'mdi-material-ui/Close';
import MagnifyIcon from 'mdi-material-ui/Magnify';
import { ObjectWithId, RequestState } from '@top-solution/utils';

import { MuiInputsConfigContext } from '../ConfigContext';
import LoaderView from '../LoaderView';
import AvailableListItem from './AvailableListItem';
import CombinedListItem from './CombinedListItem';
import SelectedListItem from './SelectedListItem';
import useWindowSize from './useWindowSize';

const defaultLabels = {
  search: 'Search...',
  addAllOptions: 'Add all',
  removeAllOptions: 'Remove all',
  removes: 'Removes: ',
  blockedBy: 'Blocked by: ',
  totalAvailable: (n: number) => `Available elements: ${n}`,
  totalSelected: (n: number) => `Selected elements: ${n}`,
};

function defaultSort<T extends ObjectWithId>(list: T[]) {
  return list.sort((a, b) => `${a.id}`.localeCompare(`${b.id}`));
}

/**
 * @returns — Returns the new array of filtered values.
 */
function difference<T extends ObjectWithId>(a: T[], b: T[]) {
  return differenceBy(a, b, 'id') || [];
}

export interface TransferListLabels {
  search?: string;
  addAllOptions?: string;
  removeAllOptions?: string;
  removes?: string;
  blockedBy?: string;
  totalAvailable?: (n: number) => string;
  totalSelected?: (n: number) => string;
}

export interface TransferListCustomizationProps {
  labels?: TransferListLabels;
  minHeight?: number;
  maxHeight?: number;
  hideAddAllOptions?: boolean;
  hideRemoveAllOptions?: boolean;
  hideTotals?: boolean;
  addOptionIcon?: React.ReactNode;
  removeOptionIcon?: React.ReactNode;
  listProps?: Partial<ListProps>;
  availableListProps?: Partial<ListProps>;
  searchOnlyAvailable?: boolean | undefined;
  selectedListProps?: Partial<ListProps>;
  toolbarProps?: Partial<ToolbarProps>;
  appBarProps?: Partial<Omit<AppBarProps, 'position'>>;
  disabled?: boolean;
  dense?: boolean | undefined;
}

function HeaderButton(props: Partial<ButtonProps>) {
  return (
    <Button
      {...props}
      color="inherit"
      sx={{
        '&:last-child': {
          marginBottom: 0,
          marginLeft: {
            sm: 1,
          },
        },
        flex: {
          xs: '1 1 auto',
          sm: '0 0 auto',
        },
        marginBottom: { xs: 0.5 },
        width: {
          xs: '100%',
          sm: 'auto',
        },
        ...props.sx,
      }}
    />
  );
}

export interface TransferListProps<T extends ObjectWithId>
  extends TransferListCustomizationProps,
    Omit<BoxProps, 'onChange' | 'minHeight' | 'maxHeight'> {
  value: T[];
  onChange: (value: T[]) => void;
  options: T[];
  renderOption: (option: T, asText?: boolean) => string | JSX.Element;
  filterOptions: (searchQuery: string) => (option: T) => boolean;
  sortOptions?: (list: T[]) => T[];
  /**
   * A RequestState object representing the network request to fetch this component options.
   */
  request?: RequestState;
  rules?: {
    exclusion?: (option: T) => T[];
    addAllOptions?: () => T[];
    disableAddOptions?: (state: { selected: T[]; available: T[] }) => boolean;
    disableRemoveOptions?: (state: { selected: T[]; available: T[] }) => boolean;
  };
}

function TransferList<T extends ObjectWithId>(
  props: TransferListProps<T>,
  ref: React.Ref<HTMLDivElement>
): JSX.Element {
  const { value, onChange, disabled, renderOption } = props;
  const mainDisabled = disabled;
  const [search, setSearch] = useState('');
  const sort = props.sortOptions || defaultSort;
  const [highlightForDisabling, setHighlightForDisabling] = useState<(string | number)[]>([] as (string | number)[]);
  const [highlightForDisabled, setHighlightForDisabled] = useState<(string | number)[]>([] as (string | number)[]);
  const theme = useTheme();

  const available = useMemo(() => {
    return difference(props.options, props.value);
  }, [props.options, props.value]);

  const { width } = useWindowSize();
  const isMobile = useMemo(() => {
    if (width && width < theme.breakpoints.values.sm) {
      return true;
    }

    return false;
  }, [theme.breakpoints.values.sm, width]);

  const configContext = useContext(MuiInputsConfigContext);
  const configContextProps = configContext?.transferList;
  const minHeight = useMemo(() => {
    return props.minHeight ?? configContextProps?.minHeight ?? 400;
  }, [configContextProps, props.minHeight]);
  const maxHeight = useMemo(() => {
    return props.maxHeight ?? configContextProps?.maxHeight;
  }, [configContextProps, props.maxHeight]);
  const hideAddAllOptions = Boolean(props.hideAddAllOptions) || Boolean(configContextProps?.hideAddAllOptions);
  const hideRemoveAllOptions = Boolean(props.hideRemoveAllOptions) || Boolean(configContextProps?.hideRemoveAllOptions);
  const hideTotals = Boolean(props.hideTotals) || Boolean(configContextProps?.hideTotals);
  const addOptionIcon = props.addOptionIcon || configContextProps?.addOptionIcon;
  const removeOptionIcon = props.removeOptionIcon || configContextProps?.removeOptionIcon;

  const listProps = props.listProps || configContextProps?.listProps || {};
  const availableListProps = props.availableListProps || configContextProps?.availableListProps || {};
  const selectedListProps = props.selectedListProps || configContextProps?.selectedListProps || {};
  const toolbarProps = props.toolbarProps || configContextProps?.toolbarProps || {};
  const appBarProps = props.appBarProps || configContextProps?.appBarProps || {};
  const dense = props.dense || configContextProps?.dense || false;

  const disableAddOptions =
    (props.rules?.disableAddOptions && props.rules.disableAddOptions({ selected: value, available }) === false) ||
    false;

  const disableRemoveOptions =
    (props.rules?.disableRemoveOptions && props.rules.disableRemoveOptions({ selected: value, available }) === false) ||
    false;

  const loaderViewCondition = !props.request || props.request.inProgress === false;

  const reverseExclusionRules = useMemo(() => {
    const reverseRules = {} as Record<string | number, (string | number)[]>;

    if (props.rules?.exclusion) {
      for (let i = 0; i < value.length; i++) {
        const option = value[i];

        const excludedByOption = props.rules.exclusion(option);

        for (let j = 0; j < excludedByOption.length; j++) {
          const excludedOption = excludedByOption[j];
          reverseRules[excludedOption.id]
            ? reverseRules[excludedOption.id].push(option.id)
            : (reverseRules[excludedOption.id] = [option.id]);
        }
      }
    }

    return reverseRules;
  }, [props.rules, value]);

  const labels = useMemo(
    () => ({
      ...defaultLabels,
      ...configContextProps?.labels,
      ...props.labels,
    }),
    [configContextProps, props.labels]
  );

  const listStyle: React.CSSProperties = useMemo(
    () => ({
      minHeight,
      maxHeight: maxHeight ? maxHeight - 125 : 'unset',
      height: maxHeight ? undefined : '100%',
      overflowY: maxHeight ? 'auto' : 'visible',
    }),
    [minHeight, maxHeight]
  );

  const handleChange = useCallback(
    (value) => {
      if (onChange) {
        onChange(value);
      }
    },
    [onChange]
  );

  const handleAddOption = useCallback(
    (option: T) => {
      if (disableAddOptions) {
        return;
      }

      const newValues = [...value, option];
      setHighlightForDisabling([]);
      handleChange(newValues);
    },
    [value, handleChange, disableAddOptions]
  );

  const handleRemoveOption = useCallback(
    (option: T) => {
      if (disableRemoveOptions) {
        return;
      }

      const newValue = [...value];
      newValue.splice(value.indexOf(option), 1);
      handleChange(newValue);
    },
    [handleChange, value, disableRemoveOptions]
  );

  const addAllItems = useCallback(() => {
    if (props.rules?.addAllOptions) {
      const added = props.rules.addAllOptions();
      handleChange([...added]);
    } else {
      handleChange([...props.options]);
    }
  }, [handleChange, props.rules, props.options]);

  const removeAllItems = useCallback(() => {
    handleChange([]);
  }, [handleChange]);

  // Renders the secondary label: if the option i disabled, prints the options that caused that,
  // if this options disables other options, prints those
  const secondaryLabel = useCallback(
    (option: T, disabledBy?: T[]) => {
      if (disabledBy) {
        return `${labels.blockedBy}${disabledBy.map((option) => renderOption(option, true)).join(', ')}`;
      }
      if (props.rules?.exclusion) {
        const exclusion = props.rules.exclusion(option).map((c) => renderOption(c, true));

        if (exclusion.length) {
          return `${labels.removes}${exclusion.join(', ')}`;
        }
      }
      return undefined;
    },
    [props.rules, labels.blockedBy, labels.removes, renderOption]
  );

  // Creates a key-value map where the key is the id of the disabled elements,
  // and the value the items that caused it
  const disabledItems: Record<string | number, T[]> = useMemo(() => {
    if (!props.rules?.exclusion) {
      return {};
    }

    const disabled: Record<string | number, T[]> = {};

    for (let i = 0; i < value.length; i++) {
      const option = value[i];

      const optionExclusionRules = props.rules.exclusion(option);

      for (let j = 0; j < optionExclusionRules.length; j++) {
        const disabledOption = optionExclusionRules[j];

        disabled[disabledOption.id] = [...(disabled[disabledOption.id] || []), option];
      }
    }

    return disabled;
  }, [props.rules, value]);

  // Highlights the selected options that caused the argument option to be disabled
  const handleAvailableMouseEnter = useCallback(
    (option: T) => {
      if (reverseExclusionRules) {
        const excluded = reverseExclusionRules[option.id];

        if (excluded) {
          setHighlightForDisabled(excluded);
        }
      }
    },
    [reverseExclusionRules]
  );

  const handleAvailableMouseLeave = useCallback(() => {
    setHighlightForDisabled([]);
  }, []);

  // Highlights the selected options that are going to be disabled by the argument option
  const handleAvailableButtonMouseEnter = useCallback(
    (option: T) => {
      if (props.rules?.exclusion) {
        const excluded = props.rules?.exclusion(option);

        if (excluded) {
          setHighlightForDisabling(excluded.map((option) => option.id));
        }
      }
    },
    [props.rules]
  );

  const handleAvailableButtonMouseLeave = useCallback(() => {
    setHighlightForDisabling([]);
  }, []);

  const checkedOptions = useMemo(() => {
    const checked: Record<string | number, boolean> = {};

    for (let i = 0; i < value.length; i++) {
      const option = value[i];
      checked[option.id] = true;
    }

    return checked;
  }, [value]);

  const filteredSelected = useMemo(() => {
    if (isMobile) {
      return [];
    }

    if (props.searchOnlyAvailable === true) {
      return sort(value);
    }

    return sort(value.filter(props.filterOptions(search)));
  }, [isMobile, props, search, sort, value]);

  const filteredAvailable = useMemo(() => {
    if (isMobile) {
      return [];
    }
    return sort(available.filter(props.filterOptions(search)));
  }, [available, isMobile, props, search, sort]);

  const filteredAll = useMemo(() => {
    if (!isMobile) {
      return [];
    }
    return sort(value.concat(available).filter(props.filterOptions(search)));
  }, [available, isMobile, props, search, sort, value]);

  const CombinedRow = useCallback(
    function CombinedRow(props: ListChildComponentProps) {
      const option = filteredAll[props.index];
      const checked = checkedOptions[option.id];

      return (
        <CombinedListItem
          sx={props.style}
          disabled={Boolean(disabledItems[option.id]) || disableAddOptions}
          option={option}
          checked={checked}
          renderOption={renderOption}
          secondaryLabel={secondaryLabel}
          disabledBy={disabledItems[option.id]}
          disableRemoveOptions={disableRemoveOptions}
          onAdd={handleAddOption}
          onRemove={handleRemoveOption}
          dense={dense}
        />
      );
    },
    [
      filteredAll,
      checkedOptions,
      disabledItems,
      disableAddOptions,
      renderOption,
      dense,
      secondaryLabel,
      disableRemoveOptions,
      handleAddOption,
      handleRemoveOption,
    ]
  );

  const SelectedRow = useCallback(
    function SelectedRow(props: ListChildComponentProps) {
      const option = filteredSelected[props.index];

      return (
        <SelectedListItem
          sx={{
            borderColor: highlightForDisabled.indexOf(option.id) >= 0 ? alpha(grey[700], 0.6) : undefined,
            ...props.style,
          }}
          disabled={disableRemoveOptions}
          option={option}
          renderOption={renderOption}
          secondaryLabel={secondaryLabel}
          onRemove={handleRemoveOption}
          iconDisabled={mainDisabled || disableRemoveOptions}
          removeOptionIcon={removeOptionIcon}
          dense={dense}
        />
      );
    },
    [
      filteredSelected,
      highlightForDisabled,
      disableRemoveOptions,
      renderOption,
      dense,
      secondaryLabel,
      handleRemoveOption,
      mainDisabled,
      removeOptionIcon,
    ]
  );

  const AvailableRow = useCallback(
    function AvailableRow(props: ListChildComponentProps) {
      const option = filteredAvailable[props.index];
      const disabled = Boolean(disabledItems[option.id]) || disableAddOptions;

      return (
        <AvailableListItem
          sx={{
            borderColor: highlightForDisabling.indexOf(option.id) >= 0 ? alpha(red[700], 0.6) : undefined,
            ...props.style,
          }}
          disabled={disabled}
          option={option as T}
          renderOption={renderOption}
          secondaryLabel={secondaryLabel}
          disabledBy={disabledItems[option.id]}
          onAdd={handleAddOption}
          iconDisabled={mainDisabled || disabled}
          addOptionIcon={addOptionIcon}
          onMouseEnter={handleAvailableMouseEnter}
          onMouseLeave={handleAvailableMouseLeave}
          onButtonMouseEnter={handleAvailableButtonMouseEnter}
          onButtonMouseLeave={handleAvailableButtonMouseLeave}
          dense={dense}
        />
      );
    },
    [
      filteredAvailable,
      disabledItems,
      disableAddOptions,
      highlightForDisabling,
      renderOption,
      secondaryLabel,
      handleAddOption,
      mainDisabled,
      addOptionIcon,
      handleAvailableMouseEnter,
      handleAvailableMouseLeave,
      handleAvailableButtonMouseEnter,
      handleAvailableButtonMouseLeave,
      dense,
    ]
  );

  return (
    <div ref={ref}>
      <AppBar
        position="static"
        {...appBarProps}
        sx={{
          boxShadow: 'none',
        }}
      >
        <Toolbar
          variant="dense"
          {...toolbarProps}
          sx={{
            paddingX: 2,
            paddingY: 1,
            display: 'flex',
            flexWrap: {
              xs: 'wrap',
              sm: 'nowrap',
            },
          }}
        >
          <Box
            sx={{
              position: 'relative',
              borderRadius: 1,
              backgroundColor: alpha('#fff', 0.15),
              '&:hover': {
                backgroundColor: alpha('#fff', 0.25),
              },
              height: 32,
              marginLeft: 0,
              marginRight: 'auto',
              width: {
                xs: '100%',
                sm: 'auto',
              },
              maxWidth: {
                sx: '100%',
                sm: 'calc(20ch + 104px)',
              },
              flex: {
                sm: '1 1 auto',
              },
              marginTop: { sm: 1 },
              marginBottom: { sm: 1 },
              '& input': {
                height: 16,
              },
            }}
          >
            <Box
              sx={{
                paddingX: 2,
                height: '100%',
                position: 'absolute',
                pointerEvents: 'none',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <MagnifyIcon />
            </Box>
            <InputBase
              placeholder={labels.search}
              sx={{
                color: '#fff',
                '.MuiInputBase-input': {
                  color: 'inherit',
                  paddingY: 1,
                  // vertical padding + font size from searchIcon
                  paddingLeft: `calc(1em + 32px)`,
                  transition: (theme) => theme.transitions.create('width'),
                  width: {
                    sm: '20ch',
                    xs: '100%',
                  },
                },
              }}
              inputProps={{ 'aria-label': labels.search }}
              value={search}
              onChange={(e) => setSearch(e.target.value)}
              endAdornment={
                search && (
                  <InputAdornment position="end" sx={{ width: 56, color: '#fff' }}>
                    <IconButton aria-label="clear" onClick={() => setSearch('')} color="inherit" size="small">
                      <CloseIcon />
                    </IconButton>
                  </InputAdornment>
                )
              }
            />
          </Box>
          {loaderViewCondition === true && hideAddAllOptions !== true && (
            <HeaderButton
              onClick={(event) => {
                event.preventDefault();
                addAllItems();
              }}
              disabled={mainDisabled}
            >
              {labels.addAllOptions}
            </HeaderButton>
          )}
          {loaderViewCondition === true && hideRemoveAllOptions !== true && (
            <HeaderButton
              onClick={(event) => {
                event.preventDefault();
                removeAllItems();
              }}
              disabled={mainDisabled}
            >
              {labels.removeAllOptions}
            </HeaderButton>
          )}
        </Toolbar>
      </AppBar>
      <Box>
        <LoaderView condition={loaderViewCondition} minHeight={minHeight + 46}>
          <Box
            sx={{
              display: {
                sm: 'flex',
                xs: 'none',
              },
            }}
          >
            <Box
              sx={{
                flex: '0 0 50%',
                '&:first-child': {
                  '& ul': {
                    overflowY: 'auto',
                    overflowX: 'hidden',
                    padding: 0,
                  },
                },
                '&:last-child': {
                  borderLeft: `1px solid`,
                  borderColor: 'divider',
                  '& ul': {
                    overflowY: 'auto',
                    overflowX: 'hidden',
                    padding: 0,
                  },
                },
              }}
            >
              {hideTotals !== true && (
                <Box
                  sx={{
                    paddingX: 1,
                    paddingY: 2,
                    borderBottom: `1px solid`,
                    borderColor: 'divider',
                    '& > p': {
                      marginBottom: 0,
                    },
                  }}
                >
                  <Typography gutterBottom={true}>{labels.totalSelected(value.length)}</Typography>
                </Box>
              )}
              <List
                dense
                {...listProps}
                {...selectedListProps}
                sx={{
                  overflowX: 'hidden',
                  ...listStyle,
                  ...listProps?.style,
                  ...selectedListProps?.style,
                }}
              >
                <AutoSizer>
                  {({ width, height }) => (
                    <RVList
                      height={height}
                      itemCount={filteredSelected.length}
                      itemSize={dense ? 40 : 64}
                      width={width}
                    >
                      {SelectedRow}
                    </RVList>
                  )}
                </AutoSizer>
              </List>
            </Box>
            <Box
              sx={{
                flex: '0 0 50%',
                '&:first-child': {
                  '& ul': {
                    overflowY: 'auto',
                    overflowX: 'hidden',
                    padding: 0,
                  },
                },
                '&:last-child': {
                  borderLeft: `1px solid`,
                  borderColor: 'divider',
                  '& ul': {
                    overflowY: 'auto',
                    overflowX: 'hidden',
                    padding: 0,
                  },
                },
              }}
            >
              {hideTotals !== true && (
                <Box
                  sx={{
                    paddingX: 1,
                    paddingY: 2,
                    borderBottom: `1px solid`,
                    borderColor: 'divider',
                    '& > p': {
                      marginBottom: 0,
                    },
                  }}
                >
                  <Typography gutterBottom={true}>{labels.totalAvailable(available.length)}</Typography>
                </Box>
              )}
              <List
                dense
                {...listProps}
                {...availableListProps}
                sx={{
                  overflowX: 'hidden',
                  ...listStyle,
                  ...listProps?.style,
                  ...availableListProps?.style,
                }}
              >
                <AutoSizer>
                  {({ width, height }) => (
                    <RVList
                      height={height}
                      width={width}
                      itemCount={filteredAvailable.length}
                      itemSize={dense ? 40 : 64}
                    >
                      {AvailableRow}
                    </RVList>
                  )}
                </AutoSizer>
              </List>
            </Box>
          </Box>
          <Box
            sx={{
              display: {
                sm: 'none',
              },
            }}
          >
            {hideTotals !== true && (
              <Box
                sx={{
                  paddingX: 1,
                  paddingY: 2,
                  borderBottom: `1px solid`,
                  borderColor: 'divider',
                  '& > p': {
                    marginBottom: 0,
                  },
                }}
              >
                <Typography gutterBottom={true}>
                  {`${labels.totalSelected(value.length)} /
                  ${labels.totalAvailable(available.length)}`}
                </Typography>
              </Box>
            )}
            <List
              dense
              {...listProps}
              sx={{
                overflowX: 'hidden',
                ...listStyle,
                ...listProps?.style,
              }}
            >
              <AutoSizer>
                {({ width, height }) => (
                  <RVList height={height} itemCount={filteredAll.length} itemSize={dense ? 40 : 64} width={width}>
                    {CombinedRow}
                  </RVList>
                )}
              </AutoSizer>
            </List>
          </Box>
        </LoaderView>
      </Box>
    </div>
  );
}

const TransferListWithRefAndMemo = React.memo(React.forwardRef(TransferList)) as typeof TransferList;
export default TransferListWithRefAndMemo;
