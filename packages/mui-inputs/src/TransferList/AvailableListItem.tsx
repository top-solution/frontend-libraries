import React, { useCallback, useMemo } from 'react';
import { BoxProps } from '@material-ui/core/Box';
import { Theme } from '@material-ui/core/styles';
import PlusCircle from 'mdi-material-ui/PlusCircle';
import { ObjectWithId } from '@top-solution/utils';
import TransferListItem from './TransferListItem';

type AvailableListItemProps<T extends ObjectWithId> = {
  disabled: boolean;
  option: T;
  renderOption: (option: T, asText?: boolean) => string | JSX.Element;
  secondaryLabel: (option: T, disabledBy: T[]) => string | undefined;
  disabledBy: T[];
  onAdd: (option: T) => void;
  onMouseEnter: (option: T) => void;
  onMouseLeave: () => void;
  onButtonMouseEnter: (option: T) => void;
  onButtonMouseLeave: () => void;
  iconDisabled: boolean;
  addOptionIcon: undefined | React.ReactNode;
  dense?: boolean | undefined;
  sx?: BoxProps['sx'];
};

function AvailableListItem<T extends ObjectWithId>(
  props: AvailableListItemProps<T>,
  ref: React.Ref<HTMLDivElement>
): JSX.Element {
  const {
    option,
    renderOption,
    secondaryLabel,
    disabledBy,
    onAdd,
    addOptionIcon,
    onMouseEnter,
    onMouseLeave,
    onButtonMouseEnter,
    onButtonMouseLeave,
  } = props;

  const handleClick = useCallback(() => onAdd(option), [onAdd, option]);

  const handleMouseEnter = useCallback(() => onMouseEnter(option), [onMouseEnter, option]);
  const handleMouseLeave = useCallback(() => onMouseLeave(), [onMouseLeave]);
  const handleButtonMouseEnter = useCallback(() => onButtonMouseEnter(option), [onButtonMouseEnter, option]);
  const handleButtonMouseLeave = useCallback(() => onButtonMouseLeave(), [onButtonMouseLeave]);

  const primary = useMemo(() => {
    return renderOption(option, false);
  }, [option, renderOption]);
  const secondary = useMemo(() => secondaryLabel(option, disabledBy), [disabledBy, option, secondaryLabel]);

  const addIcon = useMemo(() => addOptionIcon || <PlusCircle />, [addOptionIcon]);

  return (
    <TransferListItem
      ref={ref}
      key={option.id}
      disabled={props.disabled}
      primary={primary}
      secondary={secondary}
      icon={addIcon}
      iconDisabled={props.iconDisabled}
      iconAriaLabel="add"
      iconOnRight={false}
      onClick={handleClick}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      onButtonMouseEnter={handleButtonMouseEnter}
      onButtonMouseLeave={handleButtonMouseLeave}
      dense={props.dense}
      sx={{
        border: `2px solid transparent`,
        borderRadius: 1,
        transition: (theme: Theme) =>
          theme.transitions.create('border-color', { duration: theme.transitions.duration.shortest }),
        boxSizing: 'border-box',
        paddingLeft: 2,
        ...props.sx,
      }}
    />
  );
}

const AvailableListItemWithRefAndMemo = React.memo(React.forwardRef(AvailableListItem)) as typeof AvailableListItem;
export default AvailableListItemWithRefAndMemo;
