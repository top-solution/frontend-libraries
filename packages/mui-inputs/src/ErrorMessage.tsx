import React from 'react';
import Box, { BoxProps } from '@material-ui/core/Box';
import { RequestError } from '@top-solution/utils';

export interface ErrorMessageProps extends BoxProps {
  error: RequestError | Error | null | undefined;
}

export default function ErrorMessage(props: ErrorMessageProps): JSX.Element | null {
  const { error, ...boxProps } = props;
  if (!error) {
    return null;
  }

  return (
    <Box sx={{ color: 'error.main', ...props.sx }} {...boxProps}>
      {error.message}
    </Box>
  );
}
